﻿//zmienna przechowująca interwał dla podania odpowiedzi
var interval = 7000;   //7s

//początek gry
function StartGame() {
    //komunikat dla użytkownika
    alert("Do odgadnięcia 14 miejsc ze świata. Masz 7s zanim pojawi się odpowiedź. Klikaj obrazek aby przejść do następnego.");
    //obrazek pytajnika z nową funkją kliknięcia
    QuestionPic();
}

//obrazek pytajnika
function QuestionPic() {
    //pobiera obrazek przez id i ustawia tam od nowa
    document.getElementById("game").src = "Images/question.jpg";
    //przy kliknięciu na obrazek wywołuje pokazanie kolejnego obrazka
    document.getElementById("game").onclick = function () { ShowParis(); };
}

//pokazuje obrazek 1 paryz
function ShowParis() {
    //wyświetla pytanie
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    //pobiera obrazek przez id i ustawia tam nowy
    document.getElementById("game").src = "Images/gameParis.jpg";

    //ustawia interwal przed wywołaniem funkcji odpowiedzi dla paryza
    setTimeout(AnswerParis, interval);
}

//wyświetla odpowiedz dla obrazka 1 paryz
function AnswerParis() {
    //wywietla odpowiedz (napis w naglowku)
    document.getElementById("gameHeader").innerHTML = "Eiffel Tower, Paris (France)";
    //przy kliknięciu wywoluje kolejne pokazanie obrazka
    document.getElementById("game").onclick = function () { ShowDubai(); };
}

//pokazuje obrazek 2 dubai, analogicznie reset podpisu, nowy obrazek, oczekiwanie na odpowiedz
function ShowDubai() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameDubai.jpg";

    setTimeout(AnswerDubai, interval);
}

//wyświetla odpowiedz dla obrazka 2 dubai, analogicznie daje odpowiedz, umozliwia przejscie do nastepnego obrazka przy kliknieciu
function AnswerDubai() {
    document.getElementById("gameHeader").innerHTML = "Dubai (United Arab Emirates)";
    document.getElementById("game").onclick = function () { ShowRome(); };
}

//pokazuje obrazek 3 rome, (reset, nowy, oczekiwanie)
function ShowRome() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameRome.jpg";

    setTimeout(AnswerRome, interval);
}

//wyświetla odpowiedz dla obrazka 3 rome, (odpowiedz, przejscie do nastepnego)
function AnswerRome() {
    document.getElementById("gameHeader").innerHTML = "Colosseum, Rome (Italy)";
    document.getElementById("game").onclick = function () { ShowSahara(); };
}

//pokazuje obrazek 4 sahara, (reset, nowy, oczekiwanie)
function ShowSahara() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameSahara.jpg";

    setTimeout(AnswerSahara, interval);
}

//wyświetla odpowiedz dla obrazka 4 sahara, (odpowiedz, przejscie do nastepnego)
function AnswerSahara() {
    document.getElementById("gameHeader").innerHTML = "Sahara (Marocco)";
    document.getElementById("game").onclick = function () { ShowPetersburg(); };
}

//pokazuje obrazek 5 petersburg, (reset, nowy, oczekiwanie)
function ShowPetersburg() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gamePetersburg.jpg";

    setTimeout(AnswerPetersburg, interval);
}

//wyświetla odpowiedz dla obrazka 5 petersburg, (odpowiedz, przejscie do nastepnego)
function AnswerPetersburg() {
    document.getElementById("gameHeader").innerHTML = "St.Petersburg (Russia)";
    document.getElementById("game").onclick = function () { ShowIstanbul(); };
}

//pokazuje obrazek 6 istanbul, (reset, nowy, oczekiwanie)
function ShowIstanbul() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameIstanbul.jpg";

    setTimeout(AnswerIstanbul, interval);
}

//wyświetla odpowiedz dla obrazka 6 Istanbul, (odpowiedz, przejscie do nastepnego)
function AnswerIstanbul() {
    document.getElementById("gameHeader").innerHTML = "Istanbul (Turkey)";
    document.getElementById("game").onclick = function () { ShowCambodia(); };
}

//pokazuje obrazek 7 Cambodia, (reset, nowy, oczekiwanie)
function ShowCambodia() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameCambodia.jpg";

    setTimeout(AnswerCambodia, interval);
}

//wyświetla odpowiedz dla obrazka 7 cambodia, (odpowiedz, przejscie do nastepnego)
function AnswerCambodia() {
    document.getElementById("gameHeader").innerHTML = "Siem Reap (Cambodia)";
    document.getElementById("game").onclick = function () { ShowNY(); };
}

//pokazuje obrazek 8 NY, (reset, nowy, oczekiwanie)
function ShowNY() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameNY.jpg";

    setTimeout(AnswerNY, interval);
}

//wyświetla odpowiedz dla obrazka 8 NY, (odpowiedz, przejscie do nastepnego)
function AnswerNY() {
    document.getElementById("gameHeader").innerHTML = "New York (USA)";
    document.getElementById("game").onclick = function () { ShowOsaka(); };
}

//pokazuje obrazek 9 Osaka, (reset, nowy, oczekiwanie)
function ShowOsaka() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameOsaka.jpg";

    setTimeout(AnswerOsaka, interval);
}

//wyświetla odpowiedz dla obrazka 9 Osaka, (odpowiedz, przejscie do nastepnego)
function AnswerOsaka() {
    document.getElementById("gameHeader").innerHTML = "Osaka (Japan)";
    document.getElementById("game").onclick = function () { ShowBarcelona(); };
}

//pokazuje obrazek 10 Barcelona, (reset, nowy, oczekiwanie)
function ShowBarcelona() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameBarcelona.jpg";

    setTimeout(AnswerBarcelona, interval);
}

//wyświetla odpowiedz dla obrazka 10 Barcelona, (odpowiedz, przejscie do nastepnego)
function AnswerBarcelona() {
    document.getElementById("gameHeader").innerHTML = "La Sagrada Familia, Barcelona (Spain)";
    document.getElementById("game").onclick = function () { ShowPeru(); };
}

//pokazuje obrazek 11 Peru, (reset, nowy, oczekiwanie)
function ShowPeru() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gamePeru.jpg";

    setTimeout(AnswerPeru, interval);
}

//wyświetla odpowiedz dla obrazka 11 Peru, (odpowiedz, przejscie do nastepnego)
function AnswerPeru() {
    document.getElementById("gameHeader").innerHTML = "Rainbow Mountains, Cusco (Peru)";
    document.getElementById("game").onclick = function () { ShowLondon(); };
}

//pokazuje obrazek 12 London, (reset, nowy, oczekiwanie)
function ShowLondon() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameLondon.jpg";

    setTimeout(AnswerLondon, interval);
}

//wyświetla odpowiedz dla obrazka 12 London, (odpowiedz, przejscie do nastepnego)
function AnswerLondon() {
    document.getElementById("gameHeader").innerHTML = "London (England)";
    document.getElementById("game").onclick = function () { ShowRio(); };
}

//pokazuje obrazek 13 Rio, (reset, nowy, oczekiwanie)
function ShowRio() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameRio.jpg";

    setTimeout(AnswerRio, interval);
}

//wyświetla odpowiedz dla obrazka 13 Rio, (odpowiedz, przejscie do nastepnego)
function AnswerRio() {
    document.getElementById("gameHeader").innerHTML = "Rio de Janeiro (Brasil)";
    document.getElementById("game").onclick = function () { ShowMalaysia(); };
}

//pokazuje obrazek 14 Malaysia, (reset, nowy, oczekiwanie)
function ShowMalaysia() {
    document.getElementById("gameHeader").innerHTML = "Where is it?";
    document.getElementById("game").src = "Images/gameMalaysia.jpg";

    setTimeout(AnswerMalaysia, interval);
}

//wyświetla odpowiedz dla obrazka 14 Malaysia, (odpowiedz, przejscie do nastepnego)
function AnswerMalaysia() {
    document.getElementById("gameHeader").innerHTML = "Kuala Lumpur (Malaysia)";
    document.getElementById("game").onclick = function () {
        alert("KONIEC. Mozesz sprobowac od nowa");
        QuestionPic();
    };
}