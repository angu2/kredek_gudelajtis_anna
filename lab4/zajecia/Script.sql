USE [master]
GO
/****** Object:  Database [RegistrationSystemApprentice]    Script Date: 03.12.2018 21:27:42 ******/
CREATE DATABASE [RegistrationSystemApprentice]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RegistrationSystemApprentice', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.ANIAGUDSQL\MSSQL\DATA\RegistrationSystemApprentice.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'RegistrationSystemApprentice_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.ANIAGUDSQL\MSSQL\DATA\RegistrationSystemApprentice_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [RegistrationSystemApprentice] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RegistrationSystemApprentice].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RegistrationSystemApprentice] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET ARITHABORT OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET  ENABLE_BROKER 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET RECOVERY FULL 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET  MULTI_USER 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RegistrationSystemApprentice] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [RegistrationSystemApprentice] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'RegistrationSystemApprentice', N'ON'
GO
ALTER DATABASE [RegistrationSystemApprentice] SET QUERY_STORE = OFF
GO
USE [RegistrationSystemApprentice]
GO
/****** Object:  Table [dbo].[InformationAboutStudent]    Script Date: 03.12.2018 21:27:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InformationAboutStudent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Surnname] [nvarchar](20) NOT NULL,
	[StudentIndex] [nvarchar](20) NOT NULL,
	[Email] [nvarchar](30) NOT NULL,
	[UserNameId] [int] NOT NULL,
 CONSTRAINT [PK_InformationAboutStudent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentLogInSystem]    Script Date: 03.12.2018 21:27:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentLogInSystem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](20) NOT NULL,
	[Password] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_StudentLogInSystem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[InformationAboutStudent] ON 

INSERT [dbo].[InformationAboutStudent] ([Id], [Name], [Surnname], [StudentIndex], [Email], [UserNameId]) VALUES (1, N'Jules', N'Verne', N'182828', N'verne@gmail.com', 1)
INSERT [dbo].[InformationAboutStudent] ([Id], [Name], [Surnname], [StudentIndex], [Email], [UserNameId]) VALUES (2, N'Wisława', N'Szymborska', N'192372', N'wisława@gmail.com', 2)
INSERT [dbo].[InformationAboutStudent] ([Id], [Name], [Surnname], [StudentIndex], [Email], [UserNameId]) VALUES (3, N'Ania', N'A', N'1', N'aa@op.pl', 3)
INSERT [dbo].[InformationAboutStudent] ([Id], [Name], [Surnname], [StudentIndex], [Email], [UserNameId]) VALUES (4, N'', N'', N'', N'', 4)
INSERT [dbo].[InformationAboutStudent] ([Id], [Name], [Surnname], [StudentIndex], [Email], [UserNameId]) VALUES (5, N'a', N'a', N'1', N'a', 5)
INSERT [dbo].[InformationAboutStudent] ([Id], [Name], [Surnname], [StudentIndex], [Email], [UserNameId]) VALUES (6, N'b', N'b', N'2', N'b', 6)
INSERT [dbo].[InformationAboutStudent] ([Id], [Name], [Surnname], [StudentIndex], [Email], [UserNameId]) VALUES (7, N'Ania', N'Gud', N'111111', N'ag@op.pl', 7)
SET IDENTITY_INSERT [dbo].[InformationAboutStudent] OFF
SET IDENTITY_INSERT [dbo].[StudentLogInSystem] ON 

INSERT [dbo].[StudentLogInSystem] ([Id], [UserName], [Password]) VALUES (1, N'jules', N'jules')
INSERT [dbo].[StudentLogInSystem] ([Id], [UserName], [Password]) VALUES (2, N'wisława', N'wisława')
INSERT [dbo].[StudentLogInSystem] ([Id], [UserName], [Password]) VALUES (3, N'Ania', N'a')
INSERT [dbo].[StudentLogInSystem] ([Id], [UserName], [Password]) VALUES (4, N'', N'')
INSERT [dbo].[StudentLogInSystem] ([Id], [UserName], [Password]) VALUES (5, N'a', N'jules')
INSERT [dbo].[StudentLogInSystem] ([Id], [UserName], [Password]) VALUES (6, N'b', N'b')
INSERT [dbo].[StudentLogInSystem] ([Id], [UserName], [Password]) VALUES (7, N'anna', N'anna')
SET IDENTITY_INSERT [dbo].[StudentLogInSystem] OFF
ALTER TABLE [dbo].[InformationAboutStudent]  WITH CHECK ADD  CONSTRAINT [FK_InformationAboutStudent_StudentLogInSystem1] FOREIGN KEY([UserNameId])
REFERENCES [dbo].[StudentLogInSystem] ([Id])
GO
ALTER TABLE [dbo].[InformationAboutStudent] CHECK CONSTRAINT [FK_InformationAboutStudent_StudentLogInSystem1]
GO
USE [master]
GO
ALTER DATABASE [RegistrationSystemApprentice] SET  READ_WRITE 
GO
