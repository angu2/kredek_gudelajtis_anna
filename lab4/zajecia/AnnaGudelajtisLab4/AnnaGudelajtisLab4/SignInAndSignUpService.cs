﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnaGudelajtisLab4
{
    class SignInAndSignUpService : ISignInAndSignUpService
    {
        //wlasciwosc - zwraca wartosc bool
        public bool IsTrue { get; set; }

        /// <summary>
        /// Zwraca wartość boolowską. Przypisanan zmiennej  zostanie wartość true, wtedy gdy użytkownik o danej nazwie konta nie istnieje w systemie.
        /// </summary>
        public bool CheckUserIsTrue { get; set; }
       
        //konstruktor domyślny
        public SignInAndSignUpService()
        {
            IsTrue = false;
            CheckUserIsTrue = false;
        }

        /// <summary>
        /// metoda rejestrująca użytkowników do systemu
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <param name="surnname"></param>
        /// <param name="studentIndex"></param>
        /// <param name="email"></param>
        public void SignUpToSystem(string username, string password, string name, string surnname, string studentIndex, string email)
        {
            using (RegistrationSystemApprenticeEntities context = new RegistrationSystemApprenticeEntities())
            {
                //zmienna pozwalająca na dodanie do bazy danych informacji o loginie i haśle
                var registerLoginData = context.StudentLogInSystem.FirstOrDefault();
                registerLoginData.UserName = username;
                registerLoginData.Password = password;

                //zmienna pozwalajaca na dodanie do bazy danych informacji o użytkowniku
                var registerPersonalData = context.InformationAboutStudent.FirstOrDefault();
                registerPersonalData.Name = name;
                registerPersonalData.Surnname = surnname;
                registerPersonalData.StudentIndex = studentIndex;
                registerPersonalData.Email = email;

                //sprawdzamy czy dany uzzytkownik juz istnieje w bazie, uniemozliwia zarejestrowanie dwukrotnie tego samego
                var checkUserExist = context.StudentLogInSystem.Where(x => x.UserName == username).Count();         //count() - liczy ile jest takich uzytkownikow w bazie
                //jesli liczba uzytkownikow (checkUserExist) == 0 uzytkownika nie ma w bazie i rejestrujemy
                if (checkUserExist < 1 )
                {
                    //dodaje do bazy dane logowania i o uzytkowniku
                    context.StudentLogInSystem.Add(registerLoginData);
                    context.InformationAboutStudent.Add(registerPersonalData);

                    //zapisuje zmiany 
                    context.SaveChanges();

                    //informacja
                    MessageBox.Show("Rejestracja pomyślna");
                }
                else
                {
                    MessageBox.Show("Uzytkownik o tym loginie juz istnieje");
                }
            }
                
        }

        /// <summary>
        /// Logowanie sie do systemu
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool SingInToSystem(string username, string password)
        {
            //throw new NotImplementedException();
            //tworzy context
            using (RegistrationSystemApprenticeEntities context = new RegistrationSystemApprenticeEntities() )
            {
                //sprawdzamy czy uzytkownik z takim haslem istnieje 
                var signIn = context.StudentLogInSystem.Where(x => x.UserName == username && x.Password == password).Count();
                if (signIn >= 1)
                {
                    IsTrue = true;
                }
                //zwraca true jak dane uzytkownika sa poprawne
                return IsTrue;
            }
        }

        /// <summary>
        /// Sprawdzenie, czy użytkownik istnieje w systemie. Pozwala zdecydować czy formularz rejestracji powienien zostać zamknięty.
        /// </summary>
        /// <param name="username"> nazwa użytkownika </param>
        /// <returns> Zwraca wartość boolowską, stwierdzająca istnienie użytkownika w bazie  RegistrationSystemApprentice. </returns>
        public bool UserExist(string username)
        {
            using (RegistrationSystemApprenticeEntities context = new RegistrationSystemApprenticeEntities())
            {
                /// Zmienna sprawdzająca ilość użytkowników z daną nazwą konta
                var userExist = context.StudentLogInSystem.Where(x => x.UserName == username).Count();

                if (userExist < 1)
                {
                    CheckUserIsTrue = true;
                }

                return CheckUserIsTrue;     //zwraca true jak uzytkownik nie istnieje

            }
        }
    }
}
