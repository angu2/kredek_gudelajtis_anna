﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnaGudelajtisLab4
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonLogIn_Click(object sender, EventArgs e)
        {
            SignInAndSignUpService login = new SignInAndSignUpService();

            if (login.SingInToSystem(textBoxUsername.Text, textBoxPassword.Text) == true)       //jesli zalogujemy sie poprawnie to w tej klasie zwroci sie true
            {
                MessageBox.Show("Zostałeś zalogowany");
            }
            else
            {
                MessageBox.Show("Error");
            }
        }

        /// <summary>
        /// Otwiera formularz rejestracji
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegister_Click(object sender, EventArgs e)
        {
            FormRegister signUp = new FormRegister();
            this.Hide();
            signUp.Show();
        }
    }
}

///zadanie domowe
///sklep, produkty, cena
///instr na maila
