﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnaGudelajtisLab4
{
    public partial class FormRegister : Form
    {
        /// <summary>
        /// konstruktor domyślny
        /// </summary>
        public FormRegister()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Rejestracja do systemu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegister_Click(object sender, EventArgs e)
        {
            FormMain main = new FormMain();
            SignInAndSignUpService singUp = new SignInAndSignUpService();

            /// Zmienna typu bool, sprawdzająca czy użytkownik o podanej nazwie konta istnieje.
            //var isTrue = singUp.UserExist(textBoxName.Text);    //true jak nie istnial
            
            singUp.SignUpToSystem(textBoxUsername.Text, textBoxPassword.Text, textBoxName.Text, textBoxSurname.Text, textBoxStudentIndex.Text, textBoxEmail.Text);

            /*
            ///Jeżeli użytkownik nie istnieje to zamknij formularz rejestracji.
            if (isTrue == true)
            {
                this.Close();
                main.Show();
            }
            */
            this.Close();
            main.Show();
        }
    }
}
