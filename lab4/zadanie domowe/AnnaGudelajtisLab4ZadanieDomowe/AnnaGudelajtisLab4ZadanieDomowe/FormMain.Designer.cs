﻿namespace AnnaGudelajtisLab4ZadanieDomowe
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlMagazine = new System.Windows.Forms.TabControl();
            this.tabPageProducts = new System.Windows.Forms.TabPage();
            this.dataGridViewProducts = new System.Windows.Forms.DataGridView();
            this.tabPageWarehouse = new System.Windows.Forms.TabPage();
            this.dataGridViewWarehouse = new System.Windows.Forms.DataGridView();
            this.tabPageToOrder = new System.Windows.Forms.TabPage();
            this.dataGridViewToOrder = new System.Windows.Forms.DataGridView();
            this.buttonAddFurniture = new System.Windows.Forms.Button();
            this.textBoxWarFurnitureId = new System.Windows.Forms.TextBox();
            this.textBoxWarAmount = new System.Windows.Forms.TextBox();
            this.labelWarFurnitureId = new System.Windows.Forms.Label();
            this.labelWarAmount = new System.Windows.Forms.Label();
            this.labelWarCome = new System.Windows.Forms.Label();
            this.buttonAddToWarehouse = new System.Windows.Forms.Button();
            this.labelOrdAddOrder = new System.Windows.Forms.Label();
            this.labelOrdAmount = new System.Windows.Forms.Label();
            this.labelOrdFurnitureId = new System.Windows.Forms.Label();
            this.textBoxOrdFurnitureId = new System.Windows.Forms.TextBox();
            this.textBoxOrdAmount = new System.Windows.Forms.TextBox();
            this.buttonAddOrder = new System.Windows.Forms.Button();
            this.labelOrdDelInfo = new System.Windows.Forms.Label();
            this.tabControlMagazine.SuspendLayout();
            this.tabPageProducts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).BeginInit();
            this.tabPageWarehouse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWarehouse)).BeginInit();
            this.tabPageToOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewToOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlMagazine
            // 
            this.tabControlMagazine.Controls.Add(this.tabPageProducts);
            this.tabControlMagazine.Controls.Add(this.tabPageWarehouse);
            this.tabControlMagazine.Controls.Add(this.tabPageToOrder);
            this.tabControlMagazine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabControlMagazine.ItemSize = new System.Drawing.Size(200, 50);
            this.tabControlMagazine.Location = new System.Drawing.Point(12, 12);
            this.tabControlMagazine.Name = "tabControlMagazine";
            this.tabControlMagazine.SelectedIndex = 0;
            this.tabControlMagazine.Size = new System.Drawing.Size(943, 538);
            this.tabControlMagazine.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlMagazine.TabIndex = 0;
            // 
            // tabPageProducts
            // 
            this.tabPageProducts.Controls.Add(this.buttonAddFurniture);
            this.tabPageProducts.Controls.Add(this.dataGridViewProducts);
            this.tabPageProducts.Location = new System.Drawing.Point(4, 54);
            this.tabPageProducts.Name = "tabPageProducts";
            this.tabPageProducts.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageProducts.Size = new System.Drawing.Size(935, 480);
            this.tabPageProducts.TabIndex = 0;
            this.tabPageProducts.Text = "Katalog";
            this.tabPageProducts.UseVisualStyleBackColor = true;
            // 
            // dataGridViewProducts
            // 
            this.dataGridViewProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProducts.Location = new System.Drawing.Point(6, 18);
            this.dataGridViewProducts.Name = "dataGridViewProducts";
            this.dataGridViewProducts.RowTemplate.Height = 24;
            this.dataGridViewProducts.Size = new System.Drawing.Size(588, 415);
            this.dataGridViewProducts.TabIndex = 0;
            // 
            // tabPageWarehouse
            // 
            this.tabPageWarehouse.Controls.Add(this.buttonAddToWarehouse);
            this.tabPageWarehouse.Controls.Add(this.labelWarCome);
            this.tabPageWarehouse.Controls.Add(this.labelWarAmount);
            this.tabPageWarehouse.Controls.Add(this.labelWarFurnitureId);
            this.tabPageWarehouse.Controls.Add(this.textBoxWarAmount);
            this.tabPageWarehouse.Controls.Add(this.textBoxWarFurnitureId);
            this.tabPageWarehouse.Controls.Add(this.dataGridViewWarehouse);
            this.tabPageWarehouse.Location = new System.Drawing.Point(4, 54);
            this.tabPageWarehouse.Name = "tabPageWarehouse";
            this.tabPageWarehouse.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWarehouse.Size = new System.Drawing.Size(935, 480);
            this.tabPageWarehouse.TabIndex = 1;
            this.tabPageWarehouse.Text = "Magazyn";
            this.tabPageWarehouse.UseVisualStyleBackColor = true;
            // 
            // dataGridViewWarehouse
            // 
            this.dataGridViewWarehouse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWarehouse.Location = new System.Drawing.Point(3, 18);
            this.dataGridViewWarehouse.Name = "dataGridViewWarehouse";
            this.dataGridViewWarehouse.RowTemplate.Height = 24;
            this.dataGridViewWarehouse.Size = new System.Drawing.Size(589, 444);
            this.dataGridViewWarehouse.TabIndex = 1;
            // 
            // tabPageToOrder
            // 
            this.tabPageToOrder.Controls.Add(this.labelOrdDelInfo);
            this.tabPageToOrder.Controls.Add(this.buttonAddOrder);
            this.tabPageToOrder.Controls.Add(this.textBoxOrdAmount);
            this.tabPageToOrder.Controls.Add(this.textBoxOrdFurnitureId);
            this.tabPageToOrder.Controls.Add(this.labelOrdFurnitureId);
            this.tabPageToOrder.Controls.Add(this.labelOrdAmount);
            this.tabPageToOrder.Controls.Add(this.labelOrdAddOrder);
            this.tabPageToOrder.Controls.Add(this.dataGridViewToOrder);
            this.tabPageToOrder.Location = new System.Drawing.Point(4, 54);
            this.tabPageToOrder.Name = "tabPageToOrder";
            this.tabPageToOrder.Size = new System.Drawing.Size(935, 480);
            this.tabPageToOrder.TabIndex = 2;
            this.tabPageToOrder.Text = "Zapotrzebowanie";
            this.tabPageToOrder.UseVisualStyleBackColor = true;
            // 
            // dataGridViewToOrder
            // 
            this.dataGridViewToOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewToOrder.Location = new System.Drawing.Point(3, 21);
            this.dataGridViewToOrder.Name = "dataGridViewToOrder";
            this.dataGridViewToOrder.RowTemplate.Height = 24;
            this.dataGridViewToOrder.Size = new System.Drawing.Size(594, 438);
            this.dataGridViewToOrder.TabIndex = 2;
            // 
            // buttonAddFurniture
            // 
            this.buttonAddFurniture.Location = new System.Drawing.Point(685, 227);
            this.buttonAddFurniture.Name = "buttonAddFurniture";
            this.buttonAddFurniture.Size = new System.Drawing.Size(190, 51);
            this.buttonAddFurniture.TabIndex = 1;
            this.buttonAddFurniture.Text = "Dodaj Mebel";
            this.buttonAddFurniture.UseVisualStyleBackColor = true;
            this.buttonAddFurniture.Click += new System.EventHandler(this.buttonAddFurniture_Click);
            // 
            // textBoxWarFurnitureId
            // 
            this.textBoxWarFurnitureId.Location = new System.Drawing.Point(787, 126);
            this.textBoxWarFurnitureId.Name = "textBoxWarFurnitureId";
            this.textBoxWarFurnitureId.Size = new System.Drawing.Size(100, 27);
            this.textBoxWarFurnitureId.TabIndex = 2;
            // 
            // textBoxWarAmount
            // 
            this.textBoxWarAmount.Location = new System.Drawing.Point(787, 174);
            this.textBoxWarAmount.Name = "textBoxWarAmount";
            this.textBoxWarAmount.Size = new System.Drawing.Size(100, 27);
            this.textBoxWarAmount.TabIndex = 3;
            // 
            // labelWarFurnitureId
            // 
            this.labelWarFurnitureId.AutoSize = true;
            this.labelWarFurnitureId.Location = new System.Drawing.Point(667, 126);
            this.labelWarFurnitureId.Name = "labelWarFurnitureId";
            this.labelWarFurnitureId.Size = new System.Drawing.Size(90, 20);
            this.labelWarFurnitureId.TabIndex = 4;
            this.labelWarFurnitureId.Text = "ID mebla:";
            // 
            // labelWarAmount
            // 
            this.labelWarAmount.AutoSize = true;
            this.labelWarAmount.Location = new System.Drawing.Point(651, 174);
            this.labelWarAmount.Name = "labelWarAmount";
            this.labelWarAmount.Size = new System.Drawing.Size(106, 20);
            this.labelWarAmount.TabIndex = 5;
            this.labelWarAmount.Text = "Ilość sztuk:";
            // 
            // labelWarCome
            // 
            this.labelWarCome.AutoSize = true;
            this.labelWarCome.Location = new System.Drawing.Point(651, 80);
            this.labelWarCome.Name = "labelWarCome";
            this.labelWarCome.Size = new System.Drawing.Size(248, 20);
            this.labelWarCome.TabIndex = 6;
            this.labelWarCome.Text = "DOSTAWA / SPRZEDARZ(-)";
            // 
            // buttonAddToWarehouse
            // 
            this.buttonAddToWarehouse.Location = new System.Drawing.Point(712, 227);
            this.buttonAddToWarehouse.Name = "buttonAddToWarehouse";
            this.buttonAddToWarehouse.Size = new System.Drawing.Size(174, 55);
            this.buttonAddToWarehouse.TabIndex = 7;
            this.buttonAddToWarehouse.Text = "Aktualizuj stan magazynu";
            this.buttonAddToWarehouse.UseVisualStyleBackColor = true;
            this.buttonAddToWarehouse.Click += new System.EventHandler(this.buttonAddToWarehouse_Click);
            // 
            // labelOrdAddOrder
            // 
            this.labelOrdAddOrder.AutoSize = true;
            this.labelOrdAddOrder.Location = new System.Drawing.Point(690, 181);
            this.labelOrdAddOrder.Name = "labelOrdAddOrder";
            this.labelOrdAddOrder.Size = new System.Drawing.Size(205, 20);
            this.labelOrdAddOrder.TabIndex = 4;
            this.labelOrdAddOrder.Text = "Dodaj Zapotrzebowanie";
            // 
            // labelOrdAmount
            // 
            this.labelOrdAmount.AutoSize = true;
            this.labelOrdAmount.Location = new System.Drawing.Point(652, 269);
            this.labelOrdAmount.Name = "labelOrdAmount";
            this.labelOrdAmount.Size = new System.Drawing.Size(106, 20);
            this.labelOrdAmount.TabIndex = 5;
            this.labelOrdAmount.Text = "Ilość sztuk:";
            // 
            // labelOrdFurnitureId
            // 
            this.labelOrdFurnitureId.AutoSize = true;
            this.labelOrdFurnitureId.Location = new System.Drawing.Point(672, 222);
            this.labelOrdFurnitureId.Name = "labelOrdFurnitureId";
            this.labelOrdFurnitureId.Size = new System.Drawing.Size(86, 20);
            this.labelOrdFurnitureId.TabIndex = 6;
            this.labelOrdFurnitureId.Text = "Id mebla:";
            // 
            // textBoxOrdFurnitureId
            // 
            this.textBoxOrdFurnitureId.Location = new System.Drawing.Point(795, 219);
            this.textBoxOrdFurnitureId.Name = "textBoxOrdFurnitureId";
            this.textBoxOrdFurnitureId.Size = new System.Drawing.Size(100, 27);
            this.textBoxOrdFurnitureId.TabIndex = 7;
            // 
            // textBoxOrdAmount
            // 
            this.textBoxOrdAmount.Location = new System.Drawing.Point(795, 266);
            this.textBoxOrdAmount.Name = "textBoxOrdAmount";
            this.textBoxOrdAmount.Size = new System.Drawing.Size(100, 27);
            this.textBoxOrdAmount.TabIndex = 8;
            // 
            // buttonAddOrder
            // 
            this.buttonAddOrder.Location = new System.Drawing.Point(707, 326);
            this.buttonAddOrder.Name = "buttonAddOrder";
            this.buttonAddOrder.Size = new System.Drawing.Size(188, 59);
            this.buttonAddOrder.TabIndex = 9;
            this.buttonAddOrder.Text = "Dodaj jako zapotrzebowanie";
            this.buttonAddOrder.UseVisualStyleBackColor = true;
            this.buttonAddOrder.Click += new System.EventHandler(this.buttonAddOrder_Click);
            // 
            // labelOrdDelInfo
            // 
            this.labelOrdDelInfo.AutoSize = true;
            this.labelOrdDelInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOrdDelInfo.Location = new System.Drawing.Point(632, 55);
            this.labelOrdDelInfo.Name = "labelOrdDelInfo";
            this.labelOrdDelInfo.Size = new System.Drawing.Size(263, 54);
            this.labelOrdDelInfo.TabIndex = 10;
            this.labelOrdDelInfo.Text = "Produkty z listy zapotrzebowania \r\nusuwają się automatycznie, \r\ngdy do magazynu d" +
    "odajemy towar";
            this.labelOrdDelInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 552);
            this.Controls.Add(this.tabControlMagazine);
            this.Name = "FormMain";
            this.Text = "FormMain";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.tabControlMagazine.ResumeLayout(false);
            this.tabPageProducts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).EndInit();
            this.tabPageWarehouse.ResumeLayout(false);
            this.tabPageWarehouse.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWarehouse)).EndInit();
            this.tabPageToOrder.ResumeLayout(false);
            this.tabPageToOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewToOrder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlMagazine;
        private System.Windows.Forms.TabPage tabPageProducts;
        private System.Windows.Forms.TabPage tabPageWarehouse;
        private System.Windows.Forms.TabPage tabPageToOrder;
        private System.Windows.Forms.DataGridView dataGridViewProducts;
        private System.Windows.Forms.DataGridView dataGridViewWarehouse;
        private System.Windows.Forms.DataGridView dataGridViewToOrder;
        private System.Windows.Forms.Button buttonAddFurniture;
        private System.Windows.Forms.Button buttonAddToWarehouse;
        private System.Windows.Forms.Label labelWarCome;
        private System.Windows.Forms.Label labelWarAmount;
        private System.Windows.Forms.Label labelWarFurnitureId;
        private System.Windows.Forms.TextBox textBoxWarAmount;
        private System.Windows.Forms.TextBox textBoxWarFurnitureId;
        private System.Windows.Forms.Label labelOrdDelInfo;
        private System.Windows.Forms.Button buttonAddOrder;
        private System.Windows.Forms.TextBox textBoxOrdAmount;
        private System.Windows.Forms.TextBox textBoxOrdFurnitureId;
        private System.Windows.Forms.Label labelOrdFurnitureId;
        private System.Windows.Forms.Label labelOrdAmount;
        private System.Windows.Forms.Label labelOrdAddOrder;
    }
}

