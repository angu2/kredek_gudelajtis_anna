﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnaGudelajtisLab4ZadanieDomowe
{
    /// <summary>
    /// Form dla dodania nowego mebla
    /// </summary>
    public partial class FormAddFurniture : Form
    {
        //ctor
        public FormAddFurniture()
        {
            InitializeComponent();
        }

        /// <summary>
        /// dodaje nowy mebel do bazy (tabela produkty)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddFurniture_Click(object sender, EventArgs e)
        {
            //obiekt klasy service do wywołania metody dodaj mebel z parametrami odczytanymi z textBoxów 
            Service furniture = new Service();
            furniture.AddFurniture(textBoxFurName.Text, textBoxFurColor.Text, textBoxFurMaterial.Text, textBoxFurHeight.Text, textBoxFurWidth.Text, textBoxFurWeight.Text, textBoxFurPrice.Text);
                        
            //zamyka okno dodawania i otwiera główne
            FormMain main = new FormMain();
            this.Close();
            main.Show();
        }
    }
}
