﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnaGudelajtisLab4ZadanieDomowe
{
    public partial class FormMain : Form
    {
        //globalny context używany do przekazywania i odświezania danych w dataGridViews
        AnnaGudelajtisLab4ZadanieDomoweEntities context = new AnnaGudelajtisLab4ZadanieDomoweEntities();

        //ctor
        public FormMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// dodanie mebla, otwiera okno dodawania
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddFurniture_Click(object sender, EventArgs e)
        {
            //otwiera form dla dodania danych mebla, chowa obecny
            FormAddFurniture furniture = new FormAddFurniture();
            this.Hide();
            furniture.Show();
        }

        /// <summary>
        /// ładuje FormMain, dane do dataGridViews
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_Load(object sender, EventArgs e)
        {
            //dodaje tabele jako źródła danych dla dataGridViews
            dataGridViewProducts.DataSource = context.Furniture.ToList<Furniture>();
            dataGridViewWarehouse.DataSource = context.WarehouseView.ToList<WarehouseView>();
            dataGridViewToOrder.DataSource = context.ToOrderView.ToList<ToOrderView>();
        }

        /// <summary>
        /// aktualizuje stan magazynu przy nowej dostawie towarów (zwieksza ilosc, dodaje towary)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddToWarehouse_Click(object sender, EventArgs e)
        {
            //obiekt klasy service, wywołanie funkcji updatowania stanu magazynu z paramterami z textBoxów
            Service furniture = new Service();
            furniture.AddToWarehouse(textBoxWarFurnitureId.Text, textBoxWarAmount.Text);

            #region odświeza podglądy danych
            using (AnnaGudelajtisLab4ZadanieDomoweEntities context = new AnnaGudelajtisLab4ZadanieDomoweEntities())
            {
                //odświeza dataGridViewWarehouse
                dataGridViewWarehouse.DataSource = context.WarehouseView.ToList();
                dataGridViewWarehouse.Refresh();

                //odświeza dataGridViewToOrder
                dataGridViewToOrder.DataSource = context.ToOrderView.ToList();
                dataGridViewToOrder.Refresh();
            }
            #endregion
        }

        /// <summary>
        /// dodaje produkty do listy z zapotrzebowaniem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddOrder_Click(object sender, EventArgs e)
        {
            //obiekt klasy service, dodawanie do zapotrzebowania na podstawnie textBoxów
            Service order = new Service();
            order.AddToOrderList(textBoxOrdFurnitureId.Text, textBoxOrdAmount.Text);

            //odświeza podgląd danych
            using (AnnaGudelajtisLab4ZadanieDomoweEntities context = new AnnaGudelajtisLab4ZadanieDomoweEntities())
            {
                dataGridViewToOrder.DataSource = context.ToOrderView.ToList();
                dataGridViewToOrder.Refresh();
            }
        }
    }


    

    /*
    context.Dispose();
    BindingSource bi = new BindingSource();
    bi.DataSource = context.Furniture.Local;
    dataGridViewProducts.DataSource = bi;            
    dataGridViewProducts.Refresh();                       
    */


}
