﻿namespace AnnaGudelajtisLab4ZadanieDomowe
{
    partial class FormAddFurniture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddFurniture = new System.Windows.Forms.Button();
            this.textBoxFurName = new System.Windows.Forms.TextBox();
            this.textBoxFurPrice = new System.Windows.Forms.TextBox();
            this.textBoxFurWeight = new System.Windows.Forms.TextBox();
            this.textBoxFurWidth = new System.Windows.Forms.TextBox();
            this.textBoxFurHeight = new System.Windows.Forms.TextBox();
            this.textBoxFurMaterial = new System.Windows.Forms.TextBox();
            this.textBoxFurColor = new System.Windows.Forms.TextBox();
            this.labelFurName = new System.Windows.Forms.Label();
            this.labelFurWidth = new System.Windows.Forms.Label();
            this.labelFurWeight = new System.Windows.Forms.Label();
            this.labelFurPrice = new System.Windows.Forms.Label();
            this.labelFurHeight = new System.Windows.Forms.Label();
            this.labelFurMaterial = new System.Windows.Forms.Label();
            this.labelFurColor = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonAddFurniture
            // 
            this.buttonAddFurniture.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddFurniture.Location = new System.Drawing.Point(257, 403);
            this.buttonAddFurniture.Name = "buttonAddFurniture";
            this.buttonAddFurniture.Size = new System.Drawing.Size(181, 62);
            this.buttonAddFurniture.TabIndex = 0;
            this.buttonAddFurniture.Text = "Dodaj do katalogu";
            this.buttonAddFurniture.UseVisualStyleBackColor = true;
            this.buttonAddFurniture.Click += new System.EventHandler(this.buttonAddFurniture_Click);
            // 
            // textBoxFurName
            // 
            this.textBoxFurName.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxFurName.Location = new System.Drawing.Point(257, 39);
            this.textBoxFurName.Name = "textBoxFurName";
            this.textBoxFurName.Size = new System.Drawing.Size(181, 28);
            this.textBoxFurName.TabIndex = 1;
            // 
            // textBoxFurPrice
            // 
            this.textBoxFurPrice.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxFurPrice.Location = new System.Drawing.Point(257, 346);
            this.textBoxFurPrice.Name = "textBoxFurPrice";
            this.textBoxFurPrice.Size = new System.Drawing.Size(181, 28);
            this.textBoxFurPrice.TabIndex = 3;
            // 
            // textBoxFurWeight
            // 
            this.textBoxFurWeight.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxFurWeight.Location = new System.Drawing.Point(257, 294);
            this.textBoxFurWeight.Name = "textBoxFurWeight";
            this.textBoxFurWeight.Size = new System.Drawing.Size(181, 28);
            this.textBoxFurWeight.TabIndex = 4;
            // 
            // textBoxFurWidth
            // 
            this.textBoxFurWidth.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxFurWidth.Location = new System.Drawing.Point(257, 243);
            this.textBoxFurWidth.Name = "textBoxFurWidth";
            this.textBoxFurWidth.Size = new System.Drawing.Size(181, 28);
            this.textBoxFurWidth.TabIndex = 5;
            // 
            // textBoxFurHeight
            // 
            this.textBoxFurHeight.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxFurHeight.Location = new System.Drawing.Point(257, 192);
            this.textBoxFurHeight.Name = "textBoxFurHeight";
            this.textBoxFurHeight.Size = new System.Drawing.Size(181, 28);
            this.textBoxFurHeight.TabIndex = 6;
            // 
            // textBoxFurMaterial
            // 
            this.textBoxFurMaterial.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxFurMaterial.Location = new System.Drawing.Point(257, 139);
            this.textBoxFurMaterial.Name = "textBoxFurMaterial";
            this.textBoxFurMaterial.Size = new System.Drawing.Size(181, 28);
            this.textBoxFurMaterial.TabIndex = 7;
            // 
            // textBoxFurColor
            // 
            this.textBoxFurColor.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxFurColor.Location = new System.Drawing.Point(257, 88);
            this.textBoxFurColor.Name = "textBoxFurColor";
            this.textBoxFurColor.Size = new System.Drawing.Size(181, 28);
            this.textBoxFurColor.TabIndex = 8;
            // 
            // labelFurName
            // 
            this.labelFurName.AutoSize = true;
            this.labelFurName.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFurName.Location = new System.Drawing.Point(144, 42);
            this.labelFurName.Name = "labelFurName";
            this.labelFurName.Size = new System.Drawing.Size(74, 22);
            this.labelFurName.TabIndex = 9;
            this.labelFurName.Text = "Nazwa:";
            // 
            // labelFurWidth
            // 
            this.labelFurWidth.AutoSize = true;
            this.labelFurWidth.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFurWidth.Location = new System.Drawing.Point(114, 246);
            this.labelFurWidth.Name = "labelFurWidth";
            this.labelFurWidth.Size = new System.Drawing.Size(104, 22);
            this.labelFurWidth.TabIndex = 10;
            this.labelFurWidth.Text = "Szerokość:";
            // 
            // labelFurWeight
            // 
            this.labelFurWeight.AutoSize = true;
            this.labelFurWeight.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFurWeight.Location = new System.Drawing.Point(152, 297);
            this.labelFurWeight.Name = "labelFurWeight";
            this.labelFurWeight.Size = new System.Drawing.Size(66, 22);
            this.labelFurWeight.TabIndex = 11;
            this.labelFurWeight.Text = "Waga:";
            // 
            // labelFurPrice
            // 
            this.labelFurPrice.AutoSize = true;
            this.labelFurPrice.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFurPrice.Location = new System.Drawing.Point(157, 349);
            this.labelFurPrice.Name = "labelFurPrice";
            this.labelFurPrice.Size = new System.Drawing.Size(61, 22);
            this.labelFurPrice.TabIndex = 12;
            this.labelFurPrice.Text = "Cena:";
            // 
            // labelFurHeight
            // 
            this.labelFurHeight.AutoSize = true;
            this.labelFurHeight.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFurHeight.Location = new System.Drawing.Point(116, 195);
            this.labelFurHeight.Name = "labelFurHeight";
            this.labelFurHeight.Size = new System.Drawing.Size(102, 22);
            this.labelFurHeight.TabIndex = 13;
            this.labelFurHeight.Text = "Wysokość:";
            // 
            // labelFurMaterial
            // 
            this.labelFurMaterial.AutoSize = true;
            this.labelFurMaterial.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFurMaterial.Location = new System.Drawing.Point(132, 142);
            this.labelFurMaterial.Name = "labelFurMaterial";
            this.labelFurMaterial.Size = new System.Drawing.Size(86, 22);
            this.labelFurMaterial.TabIndex = 14;
            this.labelFurMaterial.Text = "Materiał:";
            // 
            // labelFurColor
            // 
            this.labelFurColor.AutoSize = true;
            this.labelFurColor.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFurColor.Location = new System.Drawing.Point(156, 91);
            this.labelFurColor.Name = "labelFurColor";
            this.labelFurColor.Size = new System.Drawing.Size(62, 22);
            this.labelFurColor.TabIndex = 15;
            this.labelFurColor.Text = "Kolor:";
            // 
            // FormAddFurniture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 499);
            this.Controls.Add(this.labelFurColor);
            this.Controls.Add(this.labelFurMaterial);
            this.Controls.Add(this.labelFurHeight);
            this.Controls.Add(this.labelFurPrice);
            this.Controls.Add(this.labelFurWeight);
            this.Controls.Add(this.labelFurWidth);
            this.Controls.Add(this.labelFurName);
            this.Controls.Add(this.textBoxFurColor);
            this.Controls.Add(this.textBoxFurMaterial);
            this.Controls.Add(this.textBoxFurHeight);
            this.Controls.Add(this.textBoxFurWidth);
            this.Controls.Add(this.textBoxFurWeight);
            this.Controls.Add(this.textBoxFurPrice);
            this.Controls.Add(this.textBoxFurName);
            this.Controls.Add(this.buttonAddFurniture);
            this.Name = "FormAddFurniture";
            this.Text = "FormAddFurniture";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAddFurniture;
        private System.Windows.Forms.TextBox textBoxFurName;
        private System.Windows.Forms.TextBox textBoxFurPrice;
        private System.Windows.Forms.TextBox textBoxFurWeight;
        private System.Windows.Forms.TextBox textBoxFurWidth;
        private System.Windows.Forms.TextBox textBoxFurHeight;
        private System.Windows.Forms.TextBox textBoxFurMaterial;
        private System.Windows.Forms.TextBox textBoxFurColor;
        private System.Windows.Forms.Label labelFurName;
        private System.Windows.Forms.Label labelFurWidth;
        private System.Windows.Forms.Label labelFurWeight;
        private System.Windows.Forms.Label labelFurPrice;
        private System.Windows.Forms.Label labelFurHeight;
        private System.Windows.Forms.Label labelFurMaterial;
        private System.Windows.Forms.Label labelFurColor;
    }
}