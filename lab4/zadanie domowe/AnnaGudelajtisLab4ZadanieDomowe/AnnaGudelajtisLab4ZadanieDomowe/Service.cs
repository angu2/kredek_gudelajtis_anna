﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AnnaGudelajtisLab4ZadanieDomowe
{
    /// <summary>
    /// Klasa service do obsługi CRUD bazy danych
    /// </summary>
    public class Service
    {
        /// <summary>
        /// Dodawanie nowego mebla do bazy
        /// </summary>
        /// <param name="name"></param>
        /// <param name="color"></param>
        /// <param name="material"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="weight"></param>
        /// <param name="price"></param>
        public void AddFurniture(string name, string color, string material, string height, string width, string weight, string price)
        {
            using (AnnaGudelajtisLab4ZadanieDomoweEntities context = new AnnaGudelajtisLab4ZadanieDomoweEntities())
            {
                //zmienna pozwalająca na dodanie do bazy danych informacji o meblu
                var furnitureData = context.Furniture.FirstOrDefault();
                //dodanie informacji do zmiennej rekordu
                furnitureData.Name = name;
                furnitureData.Color = color;
                furnitureData.Material = material;
                furnitureData.Height = Int32.Parse(height);
                furnitureData.Width = Int32.Parse(width);
                furnitureData.Weight = Int32.Parse(weight);
                furnitureData.Price = Int32.Parse(price);

                //dodanie rekordu do bazy
                context.Furniture.Add(furnitureData);
                //zapis zmian
                context.SaveChanges();
            }
        }

        /// <summary>
        /// dostawa do magazynu (dodaje ilosc lub rekord; zmniejsza ilosc lub usuwa rekord; zmniejsza zapotrzebowanie w przypadku dostawy)
        /// </summary>
        /// <param name="furnitureId"></param>
        /// <param name="amount"></param>
        public void AddToWarehouse (string id, string amount)
        {
            //parsuje zmnienne 
            int furnitureId = Int32.Parse(id);
            int amountAdded = Int32.Parse(amount);

            //sprawdza czy id mebla jest poprawne
            if (CheckFurnitureExists(furnitureId) )
            {//Mebel istnieje

                //gdy ubywa rzeczy z magazynu
                if (amountAdded < 0)
                {
                    //zabiera rzeczy/sztuki z magazynu
                    RemoveFromWarehouse(furnitureId, amountAdded);
                    return;
                }
                    
                //aktualizuje (zmniejsza) zapotrzebowanie na dostarczony produkt
                UpdateToOrderList(furnitureId,amountAdded);

                using (AnnaGudelajtisLab4ZadanieDomoweEntities context = new AnnaGudelajtisLab4ZadanieDomoweEntities())
                {
                    //czy jest juz w magazynie
                    var checkMagazine = context.Warehouse.Where(x => x.FurnitureId == furnitureId).Count();

                    //nie ma w magazynie
                    if (checkMagazine < 1)
                    {
                        //zmienna pozwalająca na dodanie do bazy i jej atrybuty
                        var newData = context.Warehouse.FirstOrDefault();
                        newData.FurnitureId = furnitureId;
                        newData.Amount = amountAdded;

                        //dodanie produktu do magazynu
                        context.Warehouse.Add(newData);
                        context.SaveChanges();
                    }
                    //jest w magazynie, zmienia ilośc
                    else
                    {
                        //rekord z bazy
                        var entity = context.Warehouse.FirstOrDefault(item => item.FurnitureId == furnitureId);
                        //zwiększa ilosc produktu w danym rekordzie
                        entity.Amount = entity.Amount + amountAdded;
                        context.SaveChanges();
                    }
                }
            }
            //produkt nie istnieje
            else
            {
                MessageBox.Show("Zły ID");
            }
            
        }

        /// <summary>
        /// Sprawdza czy mebel widnieje w bazie katalogu (tabela Furniture)
        /// </summary>
        /// <param name="id"></param>
        public bool CheckFurnitureExists (int id)
        {
            using (AnnaGudelajtisLab4ZadanieDomoweEntities context = new AnnaGudelajtisLab4ZadanieDomoweEntities())
            {
                //liczy ilość wystąpien danego id w tabeli Furniture
                var checkFurnitureExist = context.Furniture.Where(x => x.ID == id).Count();

                // true gdy istnieje min 1
                if (checkFurnitureExist < 1)
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// aktualizuje liste zapotrzebowania
        /// </summary>
        /// <param name="furnitureId"></param>
        public void UpdateToOrderList(int id, int amount)
        {
            using (AnnaGudelajtisLab4ZadanieDomoweEntities context = new AnnaGudelajtisLab4ZadanieDomoweEntities())
            {
                //sprawdza czy produkt był na liscie zapotrzebowania
                var checkToOrderList = context.ToOrder.Where(x => x.FurnitureId == id).Count();

                // update tylko jak rekord z produktem istniał
                if (checkToOrderList > 0)
                {
                    //rekord z bazy
                    var entity = context.ToOrder.FirstOrDefault(item => item.FurnitureId == id);

                    //zapotrzebowanie nie większe niz dostarczona do magazynu ilość
                    if (entity.Amount <= amount)
                    {
                        //zapotrzebowanie zaspokojone, usunięcie rekordu
                        context.ToOrder.Remove(entity);
                        context.SaveChanges();
                    }
                    //zapotrzebowanie większe niż dostawa
                    else
                    {
                        //zapotrzebowanie o zmniejszonej ilości
                        entity.Amount = entity.Amount - amount;
                        context.SaveChanges();                    }
                }
            }
        }

        /// <summary>
        /// dodaje do listy z zapotrzebowaniem
        /// </summary>
        /// <param name="id"></param>
        /// <param name="amount"></param>
        public void AddToOrderList(string id, string amount)
        {
            //parsuje zmienne
            int furnitureId = Int32.Parse(id);
            int amountAdded = Int32.Parse(amount);

            //sprawdza czy zostal podany poprawny produkt
            if (CheckFurnitureExists(furnitureId))
            {//Mebel istnieje
                using (AnnaGudelajtisLab4ZadanieDomoweEntities context = new AnnaGudelajtisLab4ZadanieDomoweEntities())
                {
                    //sprawdza czy jest juz w zapotrzebowaniu
                    var checkOrder = context.ToOrder.Where(x => x.FurnitureId == furnitureId).Count();

                    //nie bylo takiego zapotrzebowania
                    if (checkOrder < 1)
                    {
                        //zmienna do dostepu do bazy i jej (rekordu) atrybuty
                        var newData = context.ToOrder.FirstOrDefault();
                        newData.FurnitureId = furnitureId;
                        newData.Amount = amountAdded;
                        
                        //dodaje rekord w zapotrzebowaniu
                        context.ToOrder.Add(newData);
                        context.SaveChanges();
                    }
                    //bylo zapotrzebowanie, zwieksza sie 
                    else
                    {
                        //rekord z bazy
                        var entity = context.ToOrder.FirstOrDefault(item => item.FurnitureId == furnitureId);
                        //zwieksza ilosc zapotrzebowania na zapopatrzenie
                        entity.Amount = entity.Amount + amountAdded;
                        context.SaveChanges();
                    }
                }
            }
            //mebel nie istnieje
            else
            {
                MessageBox.Show("Zły ID");
            }
        }

        /// <summary>
        /// zabiera (zmiesza ilosc lub usuwa rekord) z magazynu (tabela Warehouse)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="amount"></param>
        public void RemoveFromWarehouse(int id, int amount)
        {
            using (AnnaGudelajtisLab4ZadanieDomoweEntities context = new AnnaGudelajtisLab4ZadanieDomoweEntities())
            {
                //czy jest w magazynie
                var checkMagazine = context.Warehouse.Where(x => x.FurnitureId == id).Count();

                // update tylko jak rekord z produktem istniał
                if (checkMagazine > 0)
                {
                    //zmienna dla rekordu o danym id mebla
                    var entity = context.Warehouse.FirstOrDefault(item => item.FurnitureId == id);

                    //wszysko zostalo zabrane z magazynu
                    if (entity.Amount == -amount)
                    {
                        //usuwa rekord i zapisuje zmiany w bazie
                        context.Warehouse.Remove(entity);
                        context.SaveChanges();
                    }
                    //czesc zostala zabrana z magazynu
                    else if (entity.Amount > -amount) 
                    {
                        //zmniejsza ilosc posiadaną w magazynie i zapisuje
                        entity.Amount = entity.Amount + amount;                 //amount jest ujemne
                        context.SaveChanges();
                    }
                    //proba zabrania z magazynu wiekszej ilosci niz tam sie znajduje
                    else
                    {
                        MessageBox.Show("W magazynie nie ma tak duzej ilości");
                    }
                }
                else    //nie mozna zabrac jak produkt nie istnieje w magazynie
                {
                    MessageBox.Show("Nieprawidlowa ilosc");
                }
            }
        }
    }
}
