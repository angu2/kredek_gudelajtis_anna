USE [master]
GO
/****** Object:  Database [AnnaGudelajtisLab4ZadanieDomowe]    Script Date: 04.12.2018 06:07:33 ******/
CREATE DATABASE [AnnaGudelajtisLab4ZadanieDomowe]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AnnaGudelajtisLab4ZadanieDomowe', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.ANIAGUDSQL\MSSQL\DATA\AnnaGudelajtisLab4ZadanieDomowe.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'AnnaGudelajtisLab4ZadanieDomowe_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.ANIAGUDSQL\MSSQL\DATA\AnnaGudelajtisLab4ZadanieDomowe_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AnnaGudelajtisLab4ZadanieDomowe].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET ARITHABORT OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET  ENABLE_BROKER 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET RECOVERY FULL 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET  MULTI_USER 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'AnnaGudelajtisLab4ZadanieDomowe', N'ON'
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET QUERY_STORE = OFF
GO
USE [AnnaGudelajtisLab4ZadanieDomowe]
GO
/****** Object:  Table [dbo].[Furniture]    Script Date: 04.12.2018 06:07:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Furniture](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Color] [nvarchar](255) NULL,
	[Material] [nvarchar](255) NULL,
	[Height] [int] NULL,
	[Width] [int] NULL,
	[Weight] [int] NULL,
	[Price] [numeric](6, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warehouse]    Script Date: 04.12.2018 06:07:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warehouse](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FurnitureId] [int] NULL,
	[Amount] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[WarehouseView]    Script Date: 04.12.2018 06:07:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Tworzymy widok do przeglądania magazynu i zapotrzebowania na zaopatrzenie
CREATE VIEW [dbo].[WarehouseView] 
AS
SELECT w.ID, w.FurnitureId, f.Name, w.Amount
  FROM Warehouse w
    JOIN Furniture f on w.FurnitureId = f.ID;
GO
/****** Object:  Table [dbo].[ToOrder]    Script Date: 04.12.2018 06:07:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ToOrder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FurnitureId] [int] NULL,
	[Amount] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ToOrderView]    Script Date: 04.12.2018 06:07:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ToOrderView] 
AS
SELECT o.ID, o.FurnitureId, f.Name, o.Amount
  FROM ToOrder o
    JOIN Furniture f on o.FurnitureId = f.ID;
GO
SET IDENTITY_INSERT [dbo].[Furniture] ON 

INSERT [dbo].[Furniture] ([ID], [Name], [Color], [Material], [Height], [Width], [Weight], [Price]) VALUES (1, N'Duży stół', N'czarny', N'drewno', 120, 200, 20, CAST(500.00 AS Numeric(6, 2)))
INSERT [dbo].[Furniture] ([ID], [Name], [Color], [Material], [Height], [Width], [Weight], [Price]) VALUES (2, N'Krzesło', N'brązowy', N'materiał', 150, 60, 5, CAST(150.00 AS Numeric(6, 2)))
INSERT [dbo].[Furniture] ([ID], [Name], [Color], [Material], [Height], [Width], [Weight], [Price]) VALUES (3, N'Stolik', N'kasztan', N'drewno', 100, 75, 10, CAST(300.00 AS Numeric(6, 2)))
INSERT [dbo].[Furniture] ([ID], [Name], [Color], [Material], [Height], [Width], [Weight], [Price]) VALUES (4, N'Szafka', N'jasny', N'drewno', 140, 100, 25, CAST(250.00 AS Numeric(6, 2)))
INSERT [dbo].[Furniture] ([ID], [Name], [Color], [Material], [Height], [Width], [Weight], [Price]) VALUES (5, N'Regał', N'biały', N'drewno', 200, 60, 25, CAST(250.00 AS Numeric(6, 2)))
INSERT [dbo].[Furniture] ([ID], [Name], [Color], [Material], [Height], [Width], [Weight], [Price]) VALUES (6, N'Krzesło ogrodowe', N'zielony', N'plastik', 120, 70, 3, CAST(90.00 AS Numeric(6, 2)))
INSERT [dbo].[Furniture] ([ID], [Name], [Color], [Material], [Height], [Width], [Weight], [Price]) VALUES (7, N'półka', N'biały', N'drewno', 20, 50, 3, CAST(50.00 AS Numeric(6, 2)))
INSERT [dbo].[Furniture] ([ID], [Name], [Color], [Material], [Height], [Width], [Weight], [Price]) VALUES (8, N'Biurko', N'jasny brąz', N'drewno', 90, 120, 7, CAST(170.00 AS Numeric(6, 2)))
SET IDENTITY_INSERT [dbo].[Furniture] OFF
SET IDENTITY_INSERT [dbo].[ToOrder] ON 

INSERT [dbo].[ToOrder] ([ID], [FurnitureId], [Amount]) VALUES (2, 5, 45)
INSERT [dbo].[ToOrder] ([ID], [FurnitureId], [Amount]) VALUES (4, 8, 25)
INSERT [dbo].[ToOrder] ([ID], [FurnitureId], [Amount]) VALUES (5, 2, 10)
SET IDENTITY_INSERT [dbo].[ToOrder] OFF
SET IDENTITY_INSERT [dbo].[Warehouse] ON 

INSERT [dbo].[Warehouse] ([ID], [FurnitureId], [Amount]) VALUES (1, 1, 15)
INSERT [dbo].[Warehouse] ([ID], [FurnitureId], [Amount]) VALUES (2, 2, 30)
INSERT [dbo].[Warehouse] ([ID], [FurnitureId], [Amount]) VALUES (3, 4, 25)
INSERT [dbo].[Warehouse] ([ID], [FurnitureId], [Amount]) VALUES (4, 6, 40)
INSERT [dbo].[Warehouse] ([ID], [FurnitureId], [Amount]) VALUES (5, 8, 3)
INSERT [dbo].[Warehouse] ([ID], [FurnitureId], [Amount]) VALUES (7, 3, 30)
SET IDENTITY_INSERT [dbo].[Warehouse] OFF
ALTER TABLE [dbo].[ToOrder]  WITH CHECK ADD FOREIGN KEY([FurnitureId])
REFERENCES [dbo].[Furniture] ([ID])
GO
ALTER TABLE [dbo].[Warehouse]  WITH CHECK ADD FOREIGN KEY([FurnitureId])
REFERENCES [dbo].[Furniture] ([ID])
GO
USE [master]
GO
ALTER DATABASE [AnnaGudelajtisLab4ZadanieDomowe] SET  READ_WRITE 
GO
