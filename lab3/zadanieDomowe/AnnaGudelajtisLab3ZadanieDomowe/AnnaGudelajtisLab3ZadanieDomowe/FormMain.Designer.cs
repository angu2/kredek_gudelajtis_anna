﻿namespace AnnaGudelajtisLab3ZadanieDomowe
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewData = new System.Windows.Forms.DataGridView();
            this.buttonDisplay = new System.Windows.Forms.Button();
            this.buttonAddTrip = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.buttonClose = new System.Windows.Forms.Button();
            this.radioButtonTrip = new System.Windows.Forms.RadioButton();
            this.radioButtonPerson = new System.Windows.Forms.RadioButton();
            this.groupBoxDisplay = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonSearchTrip = new System.Windows.Forms.Button();
            this.radioButtonTripEvent = new System.Windows.Forms.RadioButton();
            this.radioButtonTripHiking = new System.Windows.Forms.RadioButton();
            this.radioButtonTripCityTour = new System.Windows.Forms.RadioButton();
            this.groupBoxParticipants = new System.Windows.Forms.GroupBox();
            this.buttonSearchParticipants = new System.Windows.Forms.Button();
            this.textBoxParticipantsTripID = new System.Windows.Forms.TextBox();
            this.labelParticipantsTripID = new System.Windows.Forms.Label();
            this.groupBoxSeeDetails = new System.Windows.Forms.GroupBox();
            this.radioButtonDetailsOneday = new System.Windows.Forms.RadioButton();
            this.radioButtonDetailsMountains = new System.Windows.Forms.RadioButton();
            this.buttonShowDetails = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxDeletePersonID = new System.Windows.Forms.TextBox();
            this.labelDeletePersonID = new System.Windows.Forms.Label();
            this.buttonDeleteParticipants = new System.Windows.Forms.Button();
            this.textBoxDeleteTripID = new System.Windows.Forms.TextBox();
            this.labelDeleteTripID = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxAddParticipantsPersonID = new System.Windows.Forms.TextBox();
            this.labelAddParticipantsPersonID = new System.Windows.Forms.Label();
            this.buttonAddParticipants = new System.Windows.Forms.Button();
            this.textBoxAddParticipantsTripID = new System.Windows.Forms.TextBox();
            this.labelAddParticipantsTripID = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).BeginInit();
            this.groupBoxDisplay.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxParticipants.SuspendLayout();
            this.groupBoxSeeDetails.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewData
            // 
            this.dataGridViewData.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewData.Location = new System.Drawing.Point(25, 60);
            this.dataGridViewData.Name = "dataGridViewData";
            this.dataGridViewData.RowTemplate.Height = 24;
            this.dataGridViewData.Size = new System.Drawing.Size(587, 431);
            this.dataGridViewData.TabIndex = 0;
            // 
            // buttonDisplay
            // 
            this.buttonDisplay.Location = new System.Drawing.Point(6, 80);
            this.buttonDisplay.Name = "buttonDisplay";
            this.buttonDisplay.Size = new System.Drawing.Size(115, 53);
            this.buttonDisplay.TabIndex = 1;
            this.buttonDisplay.Text = "Wyświetl";
            this.buttonDisplay.UseVisualStyleBackColor = true;
            this.buttonDisplay.Click += new System.EventHandler(this.buttonDisplay_Click);
            // 
            // buttonAddTrip
            // 
            this.buttonAddTrip.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonAddTrip.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddTrip.Location = new System.Drawing.Point(1177, 109);
            this.buttonAddTrip.Name = "buttonAddTrip";
            this.buttonAddTrip.Size = new System.Drawing.Size(115, 52);
            this.buttonAddTrip.TabIndex = 2;
            this.buttonAddTrip.Text = "Dodaj Wycieczkę";
            this.buttonAddTrip.UseVisualStyleBackColor = false;
            this.buttonAddTrip.Click += new System.EventHandler(this.buttonAddTrip_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(194, 19);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(238, 22);
            this.labelTitle.TabIndex = 3;
            this.labelTitle.Text = "Jedziemy na wycieczkę!";
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.RosyBrown;
            this.buttonClose.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonClose.Location = new System.Drawing.Point(1177, 35);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(115, 53);
            this.buttonClose.TabIndex = 4;
            this.buttonClose.Text = "Zamknij";
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // radioButtonTrip
            // 
            this.radioButtonTrip.AutoSize = true;
            this.radioButtonTrip.Checked = true;
            this.radioButtonTrip.Location = new System.Drawing.Point(15, 23);
            this.radioButtonTrip.Name = "radioButtonTrip";
            this.radioButtonTrip.Size = new System.Drawing.Size(96, 22);
            this.radioButtonTrip.TabIndex = 5;
            this.radioButtonTrip.TabStop = true;
            this.radioButtonTrip.Text = "Wycieczki";
            this.radioButtonTrip.UseVisualStyleBackColor = true;
            // 
            // radioButtonPerson
            // 
            this.radioButtonPerson.AutoSize = true;
            this.radioButtonPerson.Location = new System.Drawing.Point(15, 50);
            this.radioButtonPerson.Name = "radioButtonPerson";
            this.radioButtonPerson.Size = new System.Drawing.Size(74, 22);
            this.radioButtonPerson.TabIndex = 6;
            this.radioButtonPerson.Text = "Osoby";
            this.radioButtonPerson.UseVisualStyleBackColor = true;
            // 
            // groupBoxDisplay
            // 
            this.groupBoxDisplay.Controls.Add(this.radioButtonPerson);
            this.groupBoxDisplay.Controls.Add(this.radioButtonTrip);
            this.groupBoxDisplay.Controls.Add(this.buttonDisplay);
            this.groupBoxDisplay.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxDisplay.Location = new System.Drawing.Point(675, 28);
            this.groupBoxDisplay.Name = "groupBoxDisplay";
            this.groupBoxDisplay.Size = new System.Drawing.Size(200, 141);
            this.groupBoxDisplay.TabIndex = 7;
            this.groupBoxDisplay.TabStop = false;
            this.groupBoxDisplay.Text = "Wybierz dane:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonSearchTrip);
            this.groupBox1.Controls.Add(this.radioButtonTripEvent);
            this.groupBox1.Controls.Add(this.radioButtonTripHiking);
            this.groupBox1.Controls.Add(this.radioButtonTripCityTour);
            this.groupBox1.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(675, 175);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 171);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Wyszukaj wycieczkę:";
            // 
            // buttonSearchTrip
            // 
            this.buttonSearchTrip.Location = new System.Drawing.Point(6, 112);
            this.buttonSearchTrip.Name = "buttonSearchTrip";
            this.buttonSearchTrip.Size = new System.Drawing.Size(115, 53);
            this.buttonSearchTrip.TabIndex = 7;
            this.buttonSearchTrip.Text = "Wyszukaj";
            this.buttonSearchTrip.UseVisualStyleBackColor = true;
            this.buttonSearchTrip.Click += new System.EventHandler(this.buttonSearchTrip_Click);
            // 
            // radioButtonTripEvent
            // 
            this.radioButtonTripEvent.AutoSize = true;
            this.radioButtonTripEvent.Location = new System.Drawing.Point(15, 85);
            this.radioButtonTripEvent.Name = "radioButtonTripEvent";
            this.radioButtonTripEvent.Size = new System.Drawing.Size(69, 22);
            this.radioButtonTripEvent.TabIndex = 2;
            this.radioButtonTripEvent.Text = "Event";
            this.radioButtonTripEvent.UseVisualStyleBackColor = true;
            // 
            // radioButtonTripHiking
            // 
            this.radioButtonTripHiking.AutoSize = true;
            this.radioButtonTripHiking.Location = new System.Drawing.Point(15, 58);
            this.radioButtonTripHiking.Name = "radioButtonTripHiking";
            this.radioButtonTripHiking.Size = new System.Drawing.Size(71, 22);
            this.radioButtonTripHiking.TabIndex = 1;
            this.radioButtonTripHiking.Text = "Hiking";
            this.radioButtonTripHiking.UseVisualStyleBackColor = true;
            // 
            // radioButtonTripCityTour
            // 
            this.radioButtonTripCityTour.AutoSize = true;
            this.radioButtonTripCityTour.Checked = true;
            this.radioButtonTripCityTour.Location = new System.Drawing.Point(15, 31);
            this.radioButtonTripCityTour.Name = "radioButtonTripCityTour";
            this.radioButtonTripCityTour.Size = new System.Drawing.Size(94, 22);
            this.radioButtonTripCityTour.TabIndex = 0;
            this.radioButtonTripCityTour.TabStop = true;
            this.radioButtonTripCityTour.Text = "City Tour";
            this.radioButtonTripCityTour.UseVisualStyleBackColor = true;
            // 
            // groupBoxParticipants
            // 
            this.groupBoxParticipants.Controls.Add(this.buttonSearchParticipants);
            this.groupBoxParticipants.Controls.Add(this.textBoxParticipantsTripID);
            this.groupBoxParticipants.Controls.Add(this.labelParticipantsTripID);
            this.groupBoxParticipants.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxParticipants.Location = new System.Drawing.Point(675, 352);
            this.groupBoxParticipants.Name = "groupBoxParticipants";
            this.groupBoxParticipants.Size = new System.Drawing.Size(200, 139);
            this.groupBoxParticipants.TabIndex = 8;
            this.groupBoxParticipants.TabStop = false;
            this.groupBoxParticipants.Text = "Pokaż uczestników:";
            // 
            // buttonSearchParticipants
            // 
            this.buttonSearchParticipants.Location = new System.Drawing.Point(6, 76);
            this.buttonSearchParticipants.Name = "buttonSearchParticipants";
            this.buttonSearchParticipants.Size = new System.Drawing.Size(115, 53);
            this.buttonSearchParticipants.TabIndex = 8;
            this.buttonSearchParticipants.Text = "Wyszukaj";
            this.buttonSearchParticipants.UseVisualStyleBackColor = true;
            this.buttonSearchParticipants.Click += new System.EventHandler(this.buttonSearchParticipants_Click);
            // 
            // textBoxParticipantsTripID
            // 
            this.textBoxParticipantsTripID.Location = new System.Drawing.Point(6, 48);
            this.textBoxParticipantsTripID.Name = "textBoxParticipantsTripID";
            this.textBoxParticipantsTripID.Size = new System.Drawing.Size(115, 23);
            this.textBoxParticipantsTripID.TabIndex = 1;
            this.textBoxParticipantsTripID.Text = "1";
            // 
            // labelParticipantsTripID
            // 
            this.labelParticipantsTripID.AutoSize = true;
            this.labelParticipantsTripID.Location = new System.Drawing.Point(6, 29);
            this.labelParticipantsTripID.Name = "labelParticipantsTripID";
            this.labelParticipantsTripID.Size = new System.Drawing.Size(98, 18);
            this.labelParticipantsTripID.TabIndex = 0;
            this.labelParticipantsTripID.Text = "ID wycieczki:";
            // 
            // groupBoxSeeDetails
            // 
            this.groupBoxSeeDetails.Controls.Add(this.radioButtonDetailsOneday);
            this.groupBoxSeeDetails.Controls.Add(this.radioButtonDetailsMountains);
            this.groupBoxSeeDetails.Controls.Add(this.buttonShowDetails);
            this.groupBoxSeeDetails.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxSeeDetails.Location = new System.Drawing.Point(897, 28);
            this.groupBoxSeeDetails.Name = "groupBoxSeeDetails";
            this.groupBoxSeeDetails.Size = new System.Drawing.Size(200, 141);
            this.groupBoxSeeDetails.TabIndex = 8;
            this.groupBoxSeeDetails.TabStop = false;
            this.groupBoxSeeDetails.Text = "Zobacz szczegóły:";
            // 
            // radioButtonDetailsOneday
            // 
            this.radioButtonDetailsOneday.AutoSize = true;
            this.radioButtonDetailsOneday.Location = new System.Drawing.Point(15, 50);
            this.radioButtonDetailsOneday.Name = "radioButtonDetailsOneday";
            this.radioButtonDetailsOneday.Size = new System.Drawing.Size(119, 22);
            this.radioButtonDetailsOneday.TabIndex = 6;
            this.radioButtonDetailsOneday.Text = "Jednodniowe";
            this.radioButtonDetailsOneday.UseVisualStyleBackColor = true;
            // 
            // radioButtonDetailsMountains
            // 
            this.radioButtonDetailsMountains.AutoSize = true;
            this.radioButtonDetailsMountains.Checked = true;
            this.radioButtonDetailsMountains.Location = new System.Drawing.Point(15, 23);
            this.radioButtonDetailsMountains.Name = "radioButtonDetailsMountains";
            this.radioButtonDetailsMountains.Size = new System.Drawing.Size(82, 22);
            this.radioButtonDetailsMountains.TabIndex = 5;
            this.radioButtonDetailsMountains.TabStop = true;
            this.radioButtonDetailsMountains.Text = "Górskie";
            this.radioButtonDetailsMountains.UseVisualStyleBackColor = true;
            // 
            // buttonShowDetails
            // 
            this.buttonShowDetails.Location = new System.Drawing.Point(6, 80);
            this.buttonShowDetails.Name = "buttonShowDetails";
            this.buttonShowDetails.Size = new System.Drawing.Size(115, 53);
            this.buttonShowDetails.TabIndex = 1;
            this.buttonShowDetails.Text = "Wyświetl";
            this.buttonShowDetails.UseVisualStyleBackColor = true;
            this.buttonShowDetails.Click += new System.EventHandler(this.buttonShowDetails_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxDeletePersonID);
            this.groupBox2.Controls.Add(this.labelDeletePersonID);
            this.groupBox2.Controls.Add(this.buttonDeleteParticipants);
            this.groupBox2.Controls.Add(this.textBoxDeleteTripID);
            this.groupBox2.Controls.Add(this.labelDeleteTripID);
            this.groupBox2.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(1126, 352);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 139);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Wypisz uczestników:";
            // 
            // textBoxDeletePersonID
            // 
            this.textBoxDeletePersonID.Location = new System.Drawing.Point(107, 48);
            this.textBoxDeletePersonID.Name = "textBoxDeletePersonID";
            this.textBoxDeletePersonID.Size = new System.Drawing.Size(87, 23);
            this.textBoxDeletePersonID.TabIndex = 10;
            this.textBoxDeletePersonID.Text = "1";
            // 
            // labelDeletePersonID
            // 
            this.labelDeletePersonID.AutoSize = true;
            this.labelDeletePersonID.Location = new System.Drawing.Point(104, 28);
            this.labelDeletePersonID.Name = "labelDeletePersonID";
            this.labelDeletePersonID.Size = new System.Drawing.Size(78, 18);
            this.labelDeletePersonID.TabIndex = 9;
            this.labelDeletePersonID.Text = "ID osoby:";
            // 
            // buttonDeleteParticipants
            // 
            this.buttonDeleteParticipants.Location = new System.Drawing.Point(6, 76);
            this.buttonDeleteParticipants.Name = "buttonDeleteParticipants";
            this.buttonDeleteParticipants.Size = new System.Drawing.Size(115, 53);
            this.buttonDeleteParticipants.TabIndex = 8;
            this.buttonDeleteParticipants.Text = "Usuń";
            this.buttonDeleteParticipants.UseVisualStyleBackColor = true;
            this.buttonDeleteParticipants.Click += new System.EventHandler(this.buttonDeleteParticipants_Click);
            // 
            // textBoxDeleteTripID
            // 
            this.textBoxDeleteTripID.Location = new System.Drawing.Point(6, 48);
            this.textBoxDeleteTripID.Name = "textBoxDeleteTripID";
            this.textBoxDeleteTripID.Size = new System.Drawing.Size(87, 23);
            this.textBoxDeleteTripID.TabIndex = 1;
            this.textBoxDeleteTripID.Text = "1";
            // 
            // labelDeleteTripID
            // 
            this.labelDeleteTripID.AutoSize = true;
            this.labelDeleteTripID.Location = new System.Drawing.Point(6, 29);
            this.labelDeleteTripID.Name = "labelDeleteTripID";
            this.labelDeleteTripID.Size = new System.Drawing.Size(98, 18);
            this.labelDeleteTripID.TabIndex = 0;
            this.labelDeleteTripID.Text = "ID wycieczki:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxAddParticipantsPersonID);
            this.groupBox3.Controls.Add(this.labelAddParticipantsPersonID);
            this.groupBox3.Controls.Add(this.buttonAddParticipants);
            this.groupBox3.Controls.Add(this.textBoxAddParticipantsTripID);
            this.groupBox3.Controls.Add(this.labelAddParticipantsTripID);
            this.groupBox3.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(897, 352);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 139);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Zapisz uczestników:";
            // 
            // textBoxAddParticipantsPersonID
            // 
            this.textBoxAddParticipantsPersonID.Location = new System.Drawing.Point(107, 48);
            this.textBoxAddParticipantsPersonID.Name = "textBoxAddParticipantsPersonID";
            this.textBoxAddParticipantsPersonID.Size = new System.Drawing.Size(87, 23);
            this.textBoxAddParticipantsPersonID.TabIndex = 10;
            this.textBoxAddParticipantsPersonID.Text = "1";
            // 
            // labelAddParticipantsPersonID
            // 
            this.labelAddParticipantsPersonID.AutoSize = true;
            this.labelAddParticipantsPersonID.Location = new System.Drawing.Point(104, 28);
            this.labelAddParticipantsPersonID.Name = "labelAddParticipantsPersonID";
            this.labelAddParticipantsPersonID.Size = new System.Drawing.Size(78, 18);
            this.labelAddParticipantsPersonID.TabIndex = 9;
            this.labelAddParticipantsPersonID.Text = "ID osoby:";
            // 
            // buttonAddParticipants
            // 
            this.buttonAddParticipants.Location = new System.Drawing.Point(6, 76);
            this.buttonAddParticipants.Name = "buttonAddParticipants";
            this.buttonAddParticipants.Size = new System.Drawing.Size(115, 53);
            this.buttonAddParticipants.TabIndex = 8;
            this.buttonAddParticipants.Text = "Dodaj";
            this.buttonAddParticipants.UseVisualStyleBackColor = true;
            this.buttonAddParticipants.Click += new System.EventHandler(this.buttonAddParticipants_Click);
            // 
            // textBoxAddParticipantsTripID
            // 
            this.textBoxAddParticipantsTripID.Location = new System.Drawing.Point(6, 48);
            this.textBoxAddParticipantsTripID.Name = "textBoxAddParticipantsTripID";
            this.textBoxAddParticipantsTripID.Size = new System.Drawing.Size(87, 23);
            this.textBoxAddParticipantsTripID.TabIndex = 1;
            this.textBoxAddParticipantsTripID.Text = "1";
            // 
            // labelAddParticipantsTripID
            // 
            this.labelAddParticipantsTripID.AutoSize = true;
            this.labelAddParticipantsTripID.Location = new System.Drawing.Point(6, 29);
            this.labelAddParticipantsTripID.Name = "labelAddParticipantsTripID";
            this.labelAddParticipantsTripID.Size = new System.Drawing.Size(98, 18);
            this.labelAddParticipantsTripID.TabIndex = 0;
            this.labelAddParticipantsTripID.Text = "ID wycieczki:";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1353, 551);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBoxSeeDetails);
            this.Controls.Add(this.groupBoxParticipants);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxDisplay);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.buttonAddTrip);
            this.Controls.Add(this.dataGridViewData);
            this.Name = "FormMain";
            this.Text = "FormMain";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).EndInit();
            this.groupBoxDisplay.ResumeLayout(false);
            this.groupBoxDisplay.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxParticipants.ResumeLayout(false);
            this.groupBoxParticipants.PerformLayout();
            this.groupBoxSeeDetails.ResumeLayout(false);
            this.groupBoxSeeDetails.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewData;
        private System.Windows.Forms.Button buttonDisplay;
        private System.Windows.Forms.Button buttonAddTrip;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.RadioButton radioButtonTrip;
        private System.Windows.Forms.RadioButton radioButtonPerson;
        private System.Windows.Forms.GroupBox groupBoxDisplay;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonSearchTrip;
        private System.Windows.Forms.RadioButton radioButtonTripEvent;
        private System.Windows.Forms.RadioButton radioButtonTripHiking;
        private System.Windows.Forms.RadioButton radioButtonTripCityTour;
        private System.Windows.Forms.GroupBox groupBoxParticipants;
        private System.Windows.Forms.Button buttonSearchParticipants;
        private System.Windows.Forms.TextBox textBoxParticipantsTripID;
        private System.Windows.Forms.Label labelParticipantsTripID;
        private System.Windows.Forms.GroupBox groupBoxSeeDetails;
        private System.Windows.Forms.RadioButton radioButtonDetailsOneday;
        private System.Windows.Forms.RadioButton radioButtonDetailsMountains;
        private System.Windows.Forms.Button buttonShowDetails;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxDeletePersonID;
        private System.Windows.Forms.Label labelDeletePersonID;
        private System.Windows.Forms.Button buttonDeleteParticipants;
        private System.Windows.Forms.TextBox textBoxDeleteTripID;
        private System.Windows.Forms.Label labelDeleteTripID;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxAddParticipantsPersonID;
        private System.Windows.Forms.Label labelAddParticipantsPersonID;
        private System.Windows.Forms.Button buttonAddParticipants;
        private System.Windows.Forms.TextBox textBoxAddParticipantsTripID;
        private System.Windows.Forms.Label labelAddParticipantsTripID;
    }
}

