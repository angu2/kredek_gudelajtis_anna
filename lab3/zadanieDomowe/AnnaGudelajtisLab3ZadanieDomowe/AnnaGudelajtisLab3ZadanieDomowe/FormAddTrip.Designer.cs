﻿namespace AnnaGudelajtisLab3ZadanieDomowe
{
    partial class FormAddTrip
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxDestination = new System.Windows.Forms.TextBox();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.textBoxCapacity = new System.Windows.Forms.TextBox();
            this.labelDestination = new System.Windows.Forms.Label();
            this.labelTripType = new System.Windows.Forms.Label();
            this.labelCapacity = new System.Windows.Forms.Label();
            this.labelDuration = new System.Windows.Forms.Label();
            this.textBoxDuration = new System.Windows.Forms.TextBox();
            this.labelPrice = new System.Windows.Forms.Label();
            this.comboBoxTripType = new System.Windows.Forms.ComboBox();
            this.labelPutTripData = new System.Windows.Forms.Label();
            this.buttonAddTripNext = new System.Windows.Forms.Button();
            this.buttonMountainsDataSave = new System.Windows.Forms.Button();
            this.labelMountainsData = new System.Windows.Forms.Label();
            this.comboBoxMountainsDataDifficulty = new System.Windows.Forms.ComboBox();
            this.labelMountainsDataDifficulty = new System.Windows.Forms.Label();
            this.textBoxMountainsDataAltitude = new System.Windows.Forms.TextBox();
            this.labelMountainsDataAltitude = new System.Windows.Forms.Label();
            this.labelMountainsDataPeak = new System.Windows.Forms.Label();
            this.labelMountainsDataName = new System.Windows.Forms.Label();
            this.labelMountainsDataTripID = new System.Windows.Forms.Label();
            this.textBoxMountainsDataPeak = new System.Windows.Forms.TextBox();
            this.textBoxMountainsDataName = new System.Windows.Forms.TextBox();
            this.textBoxMountainsDataTripID = new System.Windows.Forms.TextBox();
            this.buttonOnedayDataSave = new System.Windows.Forms.Button();
            this.labelOnedayData = new System.Windows.Forms.Label();
            this.textBoxOnedayDataTransport = new System.Windows.Forms.TextBox();
            this.labelOnedayDataTransport = new System.Windows.Forms.Label();
            this.labelOnedayDataDurationH = new System.Windows.Forms.Label();
            this.labelOnedayDataTitle = new System.Windows.Forms.Label();
            this.labelOnedayDataTripID = new System.Windows.Forms.Label();
            this.textBoxOnedayDataDurationH = new System.Windows.Forms.TextBox();
            this.textBoxOnedayDataTitle = new System.Windows.Forms.TextBox();
            this.textBoxOnedayDataTripID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxDestination
            // 
            this.textBoxDestination.Location = new System.Drawing.Point(196, 60);
            this.textBoxDestination.Name = "textBoxDestination";
            this.textBoxDestination.Size = new System.Drawing.Size(172, 22);
            this.textBoxDestination.TabIndex = 0;
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(196, 270);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(172, 22);
            this.textBoxPrice.TabIndex = 1;
            // 
            // textBoxCapacity
            // 
            this.textBoxCapacity.Location = new System.Drawing.Point(196, 164);
            this.textBoxCapacity.Name = "textBoxCapacity";
            this.textBoxCapacity.Size = new System.Drawing.Size(172, 22);
            this.textBoxCapacity.TabIndex = 2;
            // 
            // labelDestination
            // 
            this.labelDestination.AutoSize = true;
            this.labelDestination.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDestination.Location = new System.Drawing.Point(81, 60);
            this.labelDestination.Name = "labelDestination";
            this.labelDestination.Size = new System.Drawing.Size(39, 19);
            this.labelDestination.TabIndex = 3;
            this.labelDestination.Text = "Cel:";
            // 
            // labelTripType
            // 
            this.labelTripType.AutoSize = true;
            this.labelTripType.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTripType.Location = new System.Drawing.Point(81, 115);
            this.labelTripType.Name = "labelTripType";
            this.labelTripType.Size = new System.Drawing.Size(43, 19);
            this.labelTripType.TabIndex = 4;
            this.labelTripType.Text = "Typ:";
            // 
            // labelCapacity
            // 
            this.labelCapacity.AutoSize = true;
            this.labelCapacity.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCapacity.Location = new System.Drawing.Point(81, 164);
            this.labelCapacity.Name = "labelCapacity";
            this.labelCapacity.Size = new System.Drawing.Size(92, 19);
            this.labelCapacity.TabIndex = 5;
            this.labelCapacity.Text = "Ilość osób:";
            // 
            // labelDuration
            // 
            this.labelDuration.AutoSize = true;
            this.labelDuration.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDuration.Location = new System.Drawing.Point(81, 215);
            this.labelDuration.Name = "labelDuration";
            this.labelDuration.Size = new System.Drawing.Size(88, 19);
            this.labelDuration.TabIndex = 6;
            this.labelDuration.Text = "Liczba dni:";
            // 
            // textBoxDuration
            // 
            this.textBoxDuration.Location = new System.Drawing.Point(196, 215);
            this.textBoxDuration.Name = "textBoxDuration";
            this.textBoxDuration.Size = new System.Drawing.Size(172, 22);
            this.textBoxDuration.TabIndex = 7;
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPrice.Location = new System.Drawing.Point(81, 273);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(54, 19);
            this.labelPrice.TabIndex = 8;
            this.labelPrice.Text = "Cena:";
            // 
            // comboBoxTripType
            // 
            this.comboBoxTripType.FormattingEnabled = true;
            this.comboBoxTripType.Items.AddRange(new object[] {
            "City Tour",
            "Hiking",
            "Event"});
            this.comboBoxTripType.Location = new System.Drawing.Point(196, 115);
            this.comboBoxTripType.Name = "comboBoxTripType";
            this.comboBoxTripType.Size = new System.Drawing.Size(172, 24);
            this.comboBoxTripType.TabIndex = 9;
            // 
            // labelPutTripData
            // 
            this.labelPutTripData.AutoSize = true;
            this.labelPutTripData.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPutTripData.Location = new System.Drawing.Point(143, 19);
            this.labelPutTripData.Name = "labelPutTripData";
            this.labelPutTripData.Size = new System.Drawing.Size(225, 22);
            this.labelPutTripData.TabIndex = 10;
            this.labelPutTripData.Text = "Wpisz dane wycieczki:";
            // 
            // buttonAddTripNext
            // 
            this.buttonAddTripNext.Location = new System.Drawing.Point(262, 333);
            this.buttonAddTripNext.Name = "buttonAddTripNext";
            this.buttonAddTripNext.Size = new System.Drawing.Size(105, 49);
            this.buttonAddTripNext.TabIndex = 11;
            this.buttonAddTripNext.Text = "Dalej";
            this.buttonAddTripNext.UseVisualStyleBackColor = true;
            this.buttonAddTripNext.Click += new System.EventHandler(this.buttonAddTripNext_Click);
            // 
            // buttonMountainsDataSave
            // 
            this.buttonMountainsDataSave.Enabled = false;
            this.buttonMountainsDataSave.Location = new System.Drawing.Point(627, 333);
            this.buttonMountainsDataSave.Name = "buttonMountainsDataSave";
            this.buttonMountainsDataSave.Size = new System.Drawing.Size(105, 49);
            this.buttonMountainsDataSave.TabIndex = 23;
            this.buttonMountainsDataSave.Text = "Zapisz";
            this.buttonMountainsDataSave.UseVisualStyleBackColor = true;
            this.buttonMountainsDataSave.Click += new System.EventHandler(this.buttonMountainsDataSave_Click);
            // 
            // labelMountainsData
            // 
            this.labelMountainsData.AutoSize = true;
            this.labelMountainsData.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMountainsData.Location = new System.Drawing.Point(446, 19);
            this.labelMountainsData.Name = "labelMountainsData";
            this.labelMountainsData.Size = new System.Drawing.Size(300, 19);
            this.labelMountainsData.TabIndex = 22;
            this.labelMountainsData.Text = "Szczegóły dla wycieczek górskich:";
            // 
            // comboBoxMountainsDataDifficulty
            // 
            this.comboBoxMountainsDataDifficulty.Enabled = false;
            this.comboBoxMountainsDataDifficulty.FormattingEnabled = true;
            this.comboBoxMountainsDataDifficulty.Items.AddRange(new object[] {
            "niska",
            "średnia",
            "wysoka"});
            this.comboBoxMountainsDataDifficulty.Location = new System.Drawing.Point(560, 268);
            this.comboBoxMountainsDataDifficulty.Name = "comboBoxMountainsDataDifficulty";
            this.comboBoxMountainsDataDifficulty.Size = new System.Drawing.Size(172, 24);
            this.comboBoxMountainsDataDifficulty.TabIndex = 21;
            // 
            // labelMountainsDataDifficulty
            // 
            this.labelMountainsDataDifficulty.AutoSize = true;
            this.labelMountainsDataDifficulty.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMountainsDataDifficulty.Location = new System.Drawing.Point(446, 273);
            this.labelMountainsDataDifficulty.Name = "labelMountainsDataDifficulty";
            this.labelMountainsDataDifficulty.Size = new System.Drawing.Size(84, 19);
            this.labelMountainsDataDifficulty.TabIndex = 20;
            this.labelMountainsDataDifficulty.Text = "Trudność:";
            // 
            // textBoxMountainsDataAltitude
            // 
            this.textBoxMountainsDataAltitude.Enabled = false;
            this.textBoxMountainsDataAltitude.Location = new System.Drawing.Point(561, 215);
            this.textBoxMountainsDataAltitude.Name = "textBoxMountainsDataAltitude";
            this.textBoxMountainsDataAltitude.Size = new System.Drawing.Size(172, 22);
            this.textBoxMountainsDataAltitude.TabIndex = 19;
            // 
            // labelMountainsDataAltitude
            // 
            this.labelMountainsDataAltitude.AutoSize = true;
            this.labelMountainsDataAltitude.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMountainsDataAltitude.Location = new System.Drawing.Point(446, 215);
            this.labelMountainsDataAltitude.Name = "labelMountainsDataAltitude";
            this.labelMountainsDataAltitude.Size = new System.Drawing.Size(93, 19);
            this.labelMountainsDataAltitude.TabIndex = 18;
            this.labelMountainsDataAltitude.Text = "Wysokość:";
            // 
            // labelMountainsDataPeak
            // 
            this.labelMountainsDataPeak.AutoSize = true;
            this.labelMountainsDataPeak.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMountainsDataPeak.Location = new System.Drawing.Point(446, 164);
            this.labelMountainsDataPeak.Name = "labelMountainsDataPeak";
            this.labelMountainsDataPeak.Size = new System.Drawing.Size(67, 19);
            this.labelMountainsDataPeak.TabIndex = 17;
            this.labelMountainsDataPeak.Text = "Szczyt:";
            // 
            // labelMountainsDataName
            // 
            this.labelMountainsDataName.AutoSize = true;
            this.labelMountainsDataName.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMountainsDataName.Location = new System.Drawing.Point(446, 115);
            this.labelMountainsDataName.Name = "labelMountainsDataName";
            this.labelMountainsDataName.Size = new System.Drawing.Size(53, 19);
            this.labelMountainsDataName.TabIndex = 16;
            this.labelMountainsDataName.Text = "Góry:";
            // 
            // labelMountainsDataTripID
            // 
            this.labelMountainsDataTripID.AutoSize = true;
            this.labelMountainsDataTripID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMountainsDataTripID.Location = new System.Drawing.Point(446, 60);
            this.labelMountainsDataTripID.Name = "labelMountainsDataTripID";
            this.labelMountainsDataTripID.Size = new System.Drawing.Size(59, 19);
            this.labelMountainsDataTripID.TabIndex = 15;
            this.labelMountainsDataTripID.Text = "TripID:";
            // 
            // textBoxMountainsDataPeak
            // 
            this.textBoxMountainsDataPeak.Enabled = false;
            this.textBoxMountainsDataPeak.Location = new System.Drawing.Point(561, 164);
            this.textBoxMountainsDataPeak.Name = "textBoxMountainsDataPeak";
            this.textBoxMountainsDataPeak.Size = new System.Drawing.Size(172, 22);
            this.textBoxMountainsDataPeak.TabIndex = 14;
            // 
            // textBoxMountainsDataName
            // 
            this.textBoxMountainsDataName.Enabled = false;
            this.textBoxMountainsDataName.Location = new System.Drawing.Point(561, 117);
            this.textBoxMountainsDataName.Name = "textBoxMountainsDataName";
            this.textBoxMountainsDataName.Size = new System.Drawing.Size(172, 22);
            this.textBoxMountainsDataName.TabIndex = 13;
            // 
            // textBoxMountainsDataTripID
            // 
            this.textBoxMountainsDataTripID.Enabled = false;
            this.textBoxMountainsDataTripID.Location = new System.Drawing.Point(561, 60);
            this.textBoxMountainsDataTripID.Name = "textBoxMountainsDataTripID";
            this.textBoxMountainsDataTripID.Size = new System.Drawing.Size(172, 22);
            this.textBoxMountainsDataTripID.TabIndex = 12;
            // 
            // buttonOnedayDataSave
            // 
            this.buttonOnedayDataSave.Enabled = false;
            this.buttonOnedayDataSave.Location = new System.Drawing.Point(999, 333);
            this.buttonOnedayDataSave.Name = "buttonOnedayDataSave";
            this.buttonOnedayDataSave.Size = new System.Drawing.Size(105, 49);
            this.buttonOnedayDataSave.TabIndex = 35;
            this.buttonOnedayDataSave.Text = "Zapisz";
            this.buttonOnedayDataSave.UseVisualStyleBackColor = true;
            this.buttonOnedayDataSave.Click += new System.EventHandler(this.buttonOnedayDataSave_Click);
            // 
            // labelOnedayData
            // 
            this.labelOnedayData.AutoSize = true;
            this.labelOnedayData.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOnedayData.Location = new System.Drawing.Point(851, 22);
            this.labelOnedayData.Name = "labelOnedayData";
            this.labelOnedayData.Size = new System.Drawing.Size(253, 19);
            this.labelOnedayData.TabIndex = 34;
            this.labelOnedayData.Text = "Sczegóły dla jednodniowych:";
            // 
            // textBoxOnedayDataTransport
            // 
            this.textBoxOnedayDataTransport.Enabled = false;
            this.textBoxOnedayDataTransport.Location = new System.Drawing.Point(932, 215);
            this.textBoxOnedayDataTransport.Name = "textBoxOnedayDataTransport";
            this.textBoxOnedayDataTransport.Size = new System.Drawing.Size(172, 22);
            this.textBoxOnedayDataTransport.TabIndex = 31;
            // 
            // labelOnedayDataTransport
            // 
            this.labelOnedayDataTransport.AutoSize = true;
            this.labelOnedayDataTransport.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOnedayDataTransport.Location = new System.Drawing.Point(817, 215);
            this.labelOnedayDataTransport.Name = "labelOnedayDataTransport";
            this.labelOnedayDataTransport.Size = new System.Drawing.Size(88, 19);
            this.labelOnedayDataTransport.TabIndex = 30;
            this.labelOnedayDataTransport.Text = "Transport:";
            // 
            // labelOnedayDataDurationH
            // 
            this.labelOnedayDataDurationH.AutoSize = true;
            this.labelOnedayDataDurationH.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOnedayDataDurationH.Location = new System.Drawing.Point(817, 164);
            this.labelOnedayDataDurationH.Name = "labelOnedayDataDurationH";
            this.labelOnedayDataDurationH.Size = new System.Drawing.Size(83, 19);
            this.labelOnedayDataDurationH.TabIndex = 29;
            this.labelOnedayDataDurationH.Text = "Czas (H):";
            // 
            // labelOnedayDataTitle
            // 
            this.labelOnedayDataTitle.AutoSize = true;
            this.labelOnedayDataTitle.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOnedayDataTitle.Location = new System.Drawing.Point(817, 115);
            this.labelOnedayDataTitle.Name = "labelOnedayDataTitle";
            this.labelOnedayDataTitle.Size = new System.Drawing.Size(52, 19);
            this.labelOnedayDataTitle.TabIndex = 28;
            this.labelOnedayDataTitle.Text = "Tytuł:";
            // 
            // labelOnedayDataTripID
            // 
            this.labelOnedayDataTripID.AutoSize = true;
            this.labelOnedayDataTripID.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOnedayDataTripID.Location = new System.Drawing.Point(817, 60);
            this.labelOnedayDataTripID.Name = "labelOnedayDataTripID";
            this.labelOnedayDataTripID.Size = new System.Drawing.Size(59, 19);
            this.labelOnedayDataTripID.TabIndex = 27;
            this.labelOnedayDataTripID.Text = "TripID:";
            // 
            // textBoxOnedayDataDurationH
            // 
            this.textBoxOnedayDataDurationH.Enabled = false;
            this.textBoxOnedayDataDurationH.Location = new System.Drawing.Point(932, 164);
            this.textBoxOnedayDataDurationH.Name = "textBoxOnedayDataDurationH";
            this.textBoxOnedayDataDurationH.Size = new System.Drawing.Size(172, 22);
            this.textBoxOnedayDataDurationH.TabIndex = 26;
            // 
            // textBoxOnedayDataTitle
            // 
            this.textBoxOnedayDataTitle.Enabled = false;
            this.textBoxOnedayDataTitle.Location = new System.Drawing.Point(931, 115);
            this.textBoxOnedayDataTitle.Name = "textBoxOnedayDataTitle";
            this.textBoxOnedayDataTitle.Size = new System.Drawing.Size(172, 22);
            this.textBoxOnedayDataTitle.TabIndex = 25;
            // 
            // textBoxOnedayDataTripID
            // 
            this.textBoxOnedayDataTripID.Enabled = false;
            this.textBoxOnedayDataTripID.Location = new System.Drawing.Point(932, 60);
            this.textBoxOnedayDataTripID.Name = "textBoxOnedayDataTripID";
            this.textBoxOnedayDataTripID.Size = new System.Drawing.Size(172, 22);
            this.textBoxOnedayDataTripID.TabIndex = 24;
            // 
            // FormAddTrip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 450);
            this.Controls.Add(this.buttonOnedayDataSave);
            this.Controls.Add(this.labelOnedayData);
            this.Controls.Add(this.textBoxOnedayDataTransport);
            this.Controls.Add(this.labelOnedayDataTransport);
            this.Controls.Add(this.labelOnedayDataDurationH);
            this.Controls.Add(this.labelOnedayDataTitle);
            this.Controls.Add(this.labelOnedayDataTripID);
            this.Controls.Add(this.textBoxOnedayDataDurationH);
            this.Controls.Add(this.textBoxOnedayDataTitle);
            this.Controls.Add(this.textBoxOnedayDataTripID);
            this.Controls.Add(this.buttonMountainsDataSave);
            this.Controls.Add(this.labelMountainsData);
            this.Controls.Add(this.comboBoxMountainsDataDifficulty);
            this.Controls.Add(this.labelMountainsDataDifficulty);
            this.Controls.Add(this.textBoxMountainsDataAltitude);
            this.Controls.Add(this.labelMountainsDataAltitude);
            this.Controls.Add(this.labelMountainsDataPeak);
            this.Controls.Add(this.labelMountainsDataName);
            this.Controls.Add(this.labelMountainsDataTripID);
            this.Controls.Add(this.textBoxMountainsDataPeak);
            this.Controls.Add(this.textBoxMountainsDataName);
            this.Controls.Add(this.textBoxMountainsDataTripID);
            this.Controls.Add(this.buttonAddTripNext);
            this.Controls.Add(this.labelPutTripData);
            this.Controls.Add(this.comboBoxTripType);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.textBoxDuration);
            this.Controls.Add(this.labelDuration);
            this.Controls.Add(this.labelCapacity);
            this.Controls.Add(this.labelTripType);
            this.Controls.Add(this.labelDestination);
            this.Controls.Add(this.textBoxCapacity);
            this.Controls.Add(this.textBoxPrice);
            this.Controls.Add(this.textBoxDestination);
            this.Name = "FormAddTrip";
            this.Text = "FormAddTrip";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxDestination;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.TextBox textBoxCapacity;
        private System.Windows.Forms.Label labelDestination;
        private System.Windows.Forms.Label labelTripType;
        private System.Windows.Forms.Label labelCapacity;
        private System.Windows.Forms.Label labelDuration;
        private System.Windows.Forms.TextBox textBoxDuration;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.ComboBox comboBoxTripType;
        private System.Windows.Forms.Label labelPutTripData;
        private System.Windows.Forms.Button buttonAddTripNext;
        private System.Windows.Forms.Button buttonMountainsDataSave;
        private System.Windows.Forms.Label labelMountainsData;
        private System.Windows.Forms.ComboBox comboBoxMountainsDataDifficulty;
        private System.Windows.Forms.Label labelMountainsDataDifficulty;
        private System.Windows.Forms.TextBox textBoxMountainsDataAltitude;
        private System.Windows.Forms.Label labelMountainsDataAltitude;
        private System.Windows.Forms.Label labelMountainsDataPeak;
        private System.Windows.Forms.Label labelMountainsDataName;
        private System.Windows.Forms.Label labelMountainsDataTripID;
        private System.Windows.Forms.TextBox textBoxMountainsDataPeak;
        private System.Windows.Forms.TextBox textBoxMountainsDataName;
        private System.Windows.Forms.TextBox textBoxMountainsDataTripID;
        private System.Windows.Forms.Button buttonOnedayDataSave;
        private System.Windows.Forms.Label labelOnedayData;
        private System.Windows.Forms.TextBox textBoxOnedayDataTransport;
        private System.Windows.Forms.Label labelOnedayDataTransport;
        private System.Windows.Forms.Label labelOnedayDataDurationH;
        private System.Windows.Forms.Label labelOnedayDataTitle;
        private System.Windows.Forms.Label labelOnedayDataTripID;
        private System.Windows.Forms.TextBox textBoxOnedayDataDurationH;
        private System.Windows.Forms.TextBox textBoxOnedayDataTitle;
        private System.Windows.Forms.TextBox textBoxOnedayDataTripID;
    }
}