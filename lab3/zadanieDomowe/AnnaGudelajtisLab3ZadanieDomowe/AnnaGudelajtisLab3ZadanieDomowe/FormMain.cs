﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AnnaGudelajtisLab3ZadanieDomowe
{
    public partial class FormMain : Form
    {
        // Połączenie z bazą danych
        SqlConnection connection;
        //ctor
        public FormMain()
        {
            InitializeComponent();
            connection = new SqlConnection(@"Data Source = ANIA-PC\ANIAGUDSQL; 
                                            database=AnnaGudelajtisLab3ZadanieDomowe;
                                            Trusted_Connection=yes");
        }

        /// <summary>
        /// Wyswietla wybrane dane - wycieczki, osoby 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDisplay_Click(object sender, EventArgs e)
        {
            //wyswietla dane w zaleznosci od wybranej w radioButtonie opcji
            if (radioButtonTrip.Checked)
                ShowData("Trip");
            else if (radioButtonPerson.Checked)
                ShowData("ContactView");
        }

        /// <summary>
        /// wyświetlenie dowolnych danych z tabel
        /// </summary>
        /// <param name="tableName"></param>
        private void ShowData(string tableName)
        {
            //utworzenie obiektu do odczytu bazy danych
            SqlDataAdapter dataAdapter = new SqlDataAdapter($"SELECT * FROM {tableName}", connection);
            DataTable table = new DataTable();
            //wypełnienie tablicy danymi z bazy
            dataAdapter.Fill(table);
            //wyświetlenie bazy danych w dataGridView
            dataGridViewData.DataSource = table;
        }

        /// <summary>
        /// funkcja wyswietla dialog obslugujacy dodanie nowej wycieczki
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddTrip_Click(object sender, EventArgs e)
        {
            //Wyświetla nowe okno (FormAddTrip) do wpisania danych, ktore obsluguje funkcjonalnosc dodawania wycieczek
            FormAddTrip newForm = new FormAddTrip();
            newForm.ShowDialog(this);
        }

        /// <summary>
        /// Zamyka aplikacje
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            //zamyka apke
            Close();
        }

        /// <summary>
        /// Wyszukuje wycieczki po ich typie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearchTrip_Click(object sender, EventArgs e)
        {
            //Wyszukuje po odpowiednich parametrach w zalezności od ustawionego radioButtona
            if (radioButtonTripCityTour.Checked)
                SearchData("Trip", "TripType = 'City Tour'");   //przekazuje tabele i warunek do WHERE
            else if (radioButtonTripHiking.Checked)
                SearchData("Trip", "TripType = 'Hiking'");
            else if (radioButtonTripEvent.Checked)
                SearchData("Trip", "TripType = 'Event'");
        }

        /// <summary>
        /// Wyszukuje z dowolnej tabeli po podanym parametrze ograniczenia
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="whereCondition"></param>
        private void SearchData(string tableName, string whereCondition)
        {
            //utworzenie obiektu do odczytu bazy danych z klauzula WHERE
            SqlDataAdapter dataAdapter = new SqlDataAdapter($"SELECT * FROM {tableName} WHERE {whereCondition}", connection);
            DataTable table = new DataTable();
            //wypełnienie tablicy danymi z bazy
            dataAdapter.Fill(table);
            //wyświetlenie bazy danych w dataGridView
            dataGridViewData.DataSource = table;
        }

        /// <summary>
        /// wyszukuje uczestnikow po ID wycieczki
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearchParticipants_Click(object sender, EventArgs e)
        {
            //przekazuje parametry do wyszukania do funkcji SearchData wraz z poleceniem sortowania wyniku wg ID
            SearchData("ParticipantsView", "TripID = '" + textBoxParticipantsTripID.Text + "' ORDER BY ID");
        }

        /// <summary>
        /// Wyswietla szczegoly wycieczki w zaleznosci od jej typu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonShowDetails_Click(object sender, EventArgs e)
        {
            //Wyswietla dane w zalezności od ustawionego radioButtona
            if (radioButtonDetailsMountains.Checked)
                ShowData("MountainsView");
            else if (radioButtonDetailsOneday.Checked)
                ShowData("OnedayView");
        }

        /// <summary>
        /// Usuwa osobe z wycieczki (tabela uczestnicy)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDeleteParticipants_Click(object sender, EventArgs e)
        {
            //zmiennna dla warunku złożonego do WHERE
            string condition = "PersonID = '" + textBoxDeletePersonID.Text + "' AND TripID = '" + textBoxDeleteTripID.Text + "'";
            //usuwa uczestników pod podanym wyzej warunkiem
            DeleteData("Participants", condition);

            //odświeża widok
            SearchData("ParticipantsView", "TripID = '" + textBoxDeleteTripID.Text + "' ORDER BY ID");
        }

        /// <summary>
        /// Usuwa rekordy z dowolnej tabeli po podanym parametrze ograniczenia
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="whereCondition"></param>
        private void DeleteData(string tableName, string whereCondition)
        {
            //utworzenie obiektu do odczytu bazy danych z komenda USUŃ pod warunkiem WHERE
            SqlCommand command = new SqlCommand($"DELETE FROM {tableName} WHERE {whereCondition}", connection);

            //otwiera połączenie
            connection.Open();
            //wykonanie kwerendy
            command.ExecuteNonQuery();
            //zamyka połączenie
            connection.Close();
        }

        /// <summary>
        /// Dodaje osobe do wycieczki (tabela uczestnicy)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddParticipants_Click(object sender, EventArgs e)
        {
            //utworzenie kwerendy
            SqlCommand command = new SqlCommand(@"INSERT INTO Participants(PersonID, TripID) 
                                                     VALUES (@PersonID, @TripID) ", connection);
            //dodanie parametrów kwerendy
            command.Parameters.Add("@PersonID", SqlDbType.Int).Value = Int32.Parse(textBoxAddParticipantsPersonID.Text);
            command.Parameters.Add("@TripID", SqlDbType.Int).Value = Int32.Parse(textBoxAddParticipantsTripID.Text);
            //otwiera połączenie
            connection.Open();
            //wykonanie kwerendy
            command.ExecuteNonQuery();
            //zamyka połączenie
            connection.Close();

            //odświeza widok
            SearchData("ParticipantsView", "TripID = '" + textBoxAddParticipantsTripID.Text + "' ORDER BY ID");
        }
    }
}
