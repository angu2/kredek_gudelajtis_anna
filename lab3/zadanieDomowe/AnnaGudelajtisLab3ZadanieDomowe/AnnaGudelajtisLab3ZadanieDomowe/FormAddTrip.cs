﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AnnaGudelajtisLab3ZadanieDomowe
{
    public partial class FormAddTrip : Form
    {
        // Połączenie z bazą danych
        SqlConnection connection;

        public FormAddTrip()
        {
            InitializeComponent();
            connection = new SqlConnection(@"Data Source = ANIA-PC\ANIAGUDSQL; 
                                            database=AnnaGudelajtisLab3ZadanieDomowe;
                                            Trusted_Connection=yes");
        }

        /// <summary>
        /// dodaje dane nowej wycieczki
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddTripNext_Click(object sender, EventArgs e)
        {
            //utworzenie kwerendy
            SqlCommand command = new SqlCommand(@"INSERT INTO Trip(Destination, TripType, Capacity, Duration, Price) 
                                                     output INSERTED.ID
                                                     VALUES (@Destination, @TripType, @Capacity, @Duration, @Price) ", connection);
            //dodanie parametrów kwerendy z pól od użytkownika
            command.Parameters.Add("@Destination", SqlDbType.NVarChar).Value = textBoxDestination.Text;
            command.Parameters.Add("@TripType", SqlDbType.NVarChar).Value = comboBoxTripType.SelectedItem;
            command.Parameters.Add("@Capacity", SqlDbType.NVarChar).Value = textBoxCapacity.Text;
            command.Parameters.Add("@Duration", SqlDbType.Int).Value = Int32.Parse(textBoxDuration.Text);
            command.Parameters.Add("@Price", SqlDbType.Int).Value = Int32.Parse(textBoxPrice.Text);
            //otwiera połączenie
            connection.Open();
            //wykonanie kwerendy  (  zamiast command.ExecuteNonQuery();  )
            //zwraca ID dodanego rekordu do zmiennej
            int insertedID = (int)command.ExecuteScalar();
            //zamyka połączenie
            connection.Close();

            //Sprawdza czy trzeba dodac szczegoly do wycieczki (jeśli gorska lub jednodniowa)
            if (comboBoxTripType.SelectedItem.ToString() == "Hiking")
            {
                //ustawia pole z id zwroconym dla dodanej wycieczki ID
                textBoxMountainsDataTripID.Text = insertedID.ToString();
                //odblokowanie pól dla gorskich wycieczek
                #region Hiking Enabled
                textBoxMountainsDataName.Enabled = true;
                textBoxMountainsDataPeak.Enabled = true;
                textBoxMountainsDataAltitude.Enabled = true;
                comboBoxMountainsDataDifficulty.Enabled = true;
                buttonMountainsDataSave.Enabled = true;
                #endregion
            }
            if (Int32.Parse(textBoxDuration.Text) == 1 )
            {
                //ustawia pole z id zwroconym dla dodanej wycieczki ID
                textBoxOnedayDataTripID.Text = insertedID.ToString();
                //odblokowanie pól dla gorskich wycieczek
                #region Oneday Enabled
                textBoxOnedayDataTitle.Enabled = true;
                textBoxOnedayDataDurationH.Enabled = true;
                textBoxOnedayDataTransport.Enabled = true;
                buttonOnedayDataSave.Enabled = true;
                #endregion
            }

            //blokuje dodanie wielokrotnie
            buttonAddTripNext.Enabled = false;
            //wyswiatla wiadomosc o dodaniu
            MessageBox.Show("Pomyślnie dodano wycieczkę. ID: "+ insertedID);

            //zamyka pośrednie okienko (FormAddTrip)
            //Close();
        }

        /// <summary>
        /// dodaje szczegoly wycieczki do tabeli Mountains
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMountainsDataSave_Click(object sender, EventArgs e)
        {
            //utworzenie kwerendy
            SqlCommand command = new SqlCommand(@"INSERT INTO Mountains(TripID, Name, Peak, Altitude, Difficulty) 
                                                     VALUES (@TripID, @Name, @Peak, @Altitude, @Difficulty) ", connection);
            //dodanie parametrów kwerendy z pól od użytkownika
            command.Parameters.Add("@TripID", SqlDbType.Int).Value = Int32.Parse(textBoxMountainsDataTripID.Text);
            command.Parameters.Add("@Name", SqlDbType.NVarChar).Value = textBoxMountainsDataName.Text;
            command.Parameters.Add("@Peak", SqlDbType.NVarChar).Value = textBoxMountainsDataPeak.Text;
            command.Parameters.Add("@Altitude", SqlDbType.Int).Value = Int32.Parse(textBoxMountainsDataAltitude.Text);
            command.Parameters.Add("@Difficulty", SqlDbType.NVarChar).Value = comboBoxMountainsDataDifficulty.SelectedItem.ToString();
            //otwiera połączenie
            connection.Open();
            //wykonanie kwerendy 
            command.ExecuteNonQuery();  
            //zamyka połączenie
            connection.Close();

            //blokuje przycisk
            buttonMountainsDataSave.Enabled = false;
            //wyswiatla wiadomosc o dodaniu
            MessageBox.Show("Pomyślnie dodano szczegóły wyjazdu w góry.");
        }

        /// <summary>
        /// dodaje szczegoly wycieczki do tabeli Oneday
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOnedayDataSave_Click(object sender, EventArgs e)
        {
            //utworzenie kwerendy
            SqlCommand command = new SqlCommand(@"INSERT INTO Oneday(TripID, Title, DurationH, Transport) 
                                                     VALUES (@TripID, @Title, @DurationH, @Transport) ", connection);
            //dodanie parametrów kwerendy z pól od użytkownika
            command.Parameters.Add("@TripID", SqlDbType.Int).Value = Int32.Parse(textBoxOnedayDataTripID.Text);
            command.Parameters.Add("@Title", SqlDbType.NVarChar).Value = textBoxOnedayDataTitle.Text;
            command.Parameters.Add("@DurationH", SqlDbType.Int).Value = Int32.Parse(textBoxOnedayDataDurationH.Text);
            command.Parameters.Add("@Transport", SqlDbType.NVarChar).Value = textBoxOnedayDataTransport.Text;
            //otwiera połączenie
            connection.Open();
            //wykonanie kwerendy 
            command.ExecuteNonQuery();
            //zamyka połączenie
            connection.Close();

            //blokuje przycisk
            buttonOnedayDataSave.Enabled = false;
            //wyswiatla wiadomosc o dodaniu
            MessageBox.Show("Pomyślnie dodano szczegóły wycieczki jednodniowej.");
        }
    }
}
