USE [master]
GO
/****** Object:  Database [AnnaGudelajtisLab3ZadanieDomowe]    Script Date: 27.11.2018 06:00:25 ******/
CREATE DATABASE [AnnaGudelajtisLab3ZadanieDomowe]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AnnaGudelajtisLab3ZadanieDomowe', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.ANIAGUDSQL\MSSQL\DATA\AnnaGudelajtisLab3ZadanieDomowe.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'AnnaGudelajtisLab3ZadanieDomowe_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.ANIAGUDSQL\MSSQL\DATA\AnnaGudelajtisLab3ZadanieDomowe_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AnnaGudelajtisLab3ZadanieDomowe].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET ARITHABORT OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET  ENABLE_BROKER 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET RECOVERY FULL 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET  MULTI_USER 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'AnnaGudelajtisLab3ZadanieDomowe', N'ON'
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET QUERY_STORE = OFF
GO
USE [AnnaGudelajtisLab3ZadanieDomowe]
GO
/****** Object:  Table [dbo].[Trip]    Script Date: 27.11.2018 06:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Trip](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Destination] [nvarchar](255) NULL,
	[TripType] [nvarchar](255) NULL,
	[Capacity] [nvarchar](255) NULL,
	[Duration] [numeric](2, 0) NULL,
	[Price] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mountains]    Script Date: 27.11.2018 06:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mountains](
	[TripID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Peak] [nvarchar](255) NULL,
	[Altitude] [numeric](4, 0) NULL,
	[Difficulty] [nvarchar](60) NULL,
PRIMARY KEY CLUSTERED 
(
	[TripID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MountainsView]    Script Date: 27.11.2018 06:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MountainsView] 
AS
SELECT t.ID, t.Destination, t.TripType, t.Duration, t.Price, m.Name, m.Peak, m.Altitude, m.Difficulty
  FROM Mountains m
    JOIN Trip t on m.TripID = t.ID
GO
/****** Object:  Table [dbo].[Oneday]    Script Date: 27.11.2018 06:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Oneday](
	[TripID] [int] NOT NULL,
	[Title] [nvarchar](255) NULL,
	[DurationH] [numeric](3, 1) NULL,
	[Transport] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[TripID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[OnedayView]    Script Date: 27.11.2018 06:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[OnedayView] 
AS
SELECT t.ID, t.Destination, t.TripType, t.Duration, t.Price, o.Title, o.DurationH, o.Transport
  FROM Oneday o
    JOIN Trip t on o.TripID = t.ID
GO
/****** Object:  View [dbo].[OnedayMountainsView]    Script Date: 27.11.2018 06:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[OnedayMountainsView] 
AS
SELECT t.ID, t.Destination, t.TripType, t.Duration, t.Price, o.Title, o.DurationH, o.Transport,  m.Name, m.Peak, m.Altitude, m.Difficulty
  FROM Trip t
    JOIN Mountains m on m.TripID = t.ID
	JOIN Oneday o on o.TripID = t.ID
GO
/****** Object:  Table [dbo].[Person]    Script Date: 27.11.2018 06:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[Age] [numeric](2, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 27.11.2018 06:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[PersonID] [int] NOT NULL,
	[Phone] [nvarchar](15) NULL,
	[Email] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ContactView]    Script Date: 27.11.2018 06:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ContactView] 
AS
SELECT p.ID, p.Name, p.Surname, p.Age, C.Phone, C.Email
  FROM Contact c
    JOIN Person p on c.PersonID = p.ID;
GO
/****** Object:  Table [dbo].[Participants]    Script Date: 27.11.2018 06:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Participants](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NULL,
	[TripID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ParticipantsView]    Script Date: 27.11.2018 06:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ParticipantsView] 
AS
SELECT p.ID, p.Name, p.Surname, c.Phone, pa.TripID
  FROM Participants pa
    JOIN Person p on pa.PersonID = p.ID
	JOIN Contact c on c.PersonID = p.ID;
GO
INSERT [dbo].[Contact] ([PersonID], [Phone], [Email]) VALUES (1, N'698547125', N'jk@onet.pl')
INSERT [dbo].[Contact] ([PersonID], [Phone], [Email]) VALUES (2, N'+48 459856654', N'pn@gmail.com')
INSERT [dbo].[Contact] ([PersonID], [Phone], [Email]) VALUES (3, N'568897784', N'mwis@wp.pl')
INSERT [dbo].[Mountains] ([TripID], [Name], [Peak], [Altitude], [Difficulty]) VALUES (2, N'Gory Sokole', N'Krzyzna Gora', CAST(654 AS Numeric(4, 0)), N'srednia')
INSERT [dbo].[Mountains] ([TripID], [Name], [Peak], [Altitude], [Difficulty]) VALUES (3, N'Sudety', N'Wielka Kopa', CAST(871 AS Numeric(4, 0)), N'srednia')
INSERT [dbo].[Mountains] ([TripID], [Name], [Peak], [Altitude], [Difficulty]) VALUES (6, N'Tatry', N'Ciemniak', CAST(2096 AS Numeric(4, 0)), N'wysoka')
INSERT [dbo].[Mountains] ([TripID], [Name], [Peak], [Altitude], [Difficulty]) VALUES (9, N'Góry Kaczawskie', N'Łysa Góra', CAST(707 AS Numeric(4, 0)), N'średnia')
INSERT [dbo].[Oneday] ([TripID], [Title], [DurationH], [Transport]) VALUES (2, N'Rudawski Park Krajobrazowy', CAST(7.0 AS Numeric(3, 1)), N'pociag')
INSERT [dbo].[Oneday] ([TripID], [Title], [DurationH], [Transport]) VALUES (3, N'Rudawski Park Krajobrazowy', CAST(6.0 AS Numeric(3, 1)), N'pociag')
INSERT [dbo].[Oneday] ([TripID], [Title], [DurationH], [Transport]) VALUES (4, N'Creed 2', CAST(2.0 AS Numeric(3, 1)), N'wlasny')
INSERT [dbo].[Oneday] ([TripID], [Title], [DurationH], [Transport]) VALUES (5, N'MayDay', CAST(2.0 AS Numeric(3, 1)), N'Autobus')
INSERT [dbo].[Oneday] ([TripID], [Title], [DurationH], [Transport]) VALUES (8, N'Integracja', CAST(3.0 AS Numeric(3, 1)), N'Autobus')
INSERT [dbo].[Oneday] ([TripID], [Title], [DurationH], [Transport]) VALUES (9, N'Sudety', CAST(8.0 AS Numeric(3, 1)), N'Pociag')
SET IDENTITY_INSERT [dbo].[Participants] ON 

INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (1, 1, 1)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (2, 1, 3)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (4, 2, 1)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (6, 3, 2)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (10, 3, 4)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (18, 2, 4)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (19, 1, 2)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (20, 2, 3)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (21, 2, 5)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (22, 2, 6)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (23, 3, 6)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (24, 1, 7)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (25, 2, 7)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (26, 2, 8)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (27, 1, 8)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (28, 3, 8)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (29, 1, 9)
INSERT [dbo].[Participants] ([ID], [PersonID], [TripID]) VALUES (30, 3, 9)
SET IDENTITY_INSERT [dbo].[Participants] OFF
SET IDENTITY_INSERT [dbo].[Person] ON 

INSERT [dbo].[Person] ([ID], [Name], [Surname], [Age]) VALUES (1, N'Jan', N'Kowalski', CAST(20 AS Numeric(2, 0)))
INSERT [dbo].[Person] ([ID], [Name], [Surname], [Age]) VALUES (2, N'Piotr', N'Nowak', CAST(22 AS Numeric(2, 0)))
INSERT [dbo].[Person] ([ID], [Name], [Surname], [Age]) VALUES (3, N'Maria', N'Wisniewska', CAST(19 AS Numeric(2, 0)))
SET IDENTITY_INSERT [dbo].[Person] OFF
SET IDENTITY_INSERT [dbo].[Trip] ON 

INSERT [dbo].[Trip] ([ID], [Destination], [TripType], [Capacity], [Duration], [Price]) VALUES (1, N'Krakow', N'City Tour', N'50', CAST(3 AS Numeric(2, 0)), 250)
INSERT [dbo].[Trip] ([ID], [Destination], [TripType], [Capacity], [Duration], [Price]) VALUES (2, N'Rudawy Janowickie', N'Hiking', N'15', CAST(1 AS Numeric(2, 0)), 25)
INSERT [dbo].[Trip] ([ID], [Destination], [TripType], [Capacity], [Duration], [Price]) VALUES (3, N'Kolorowe Jeziorka', N'Hiking', N'20', CAST(1 AS Numeric(2, 0)), 30)
INSERT [dbo].[Trip] ([ID], [Destination], [TripType], [Capacity], [Duration], [Price]) VALUES (4, N'Kino', N'Event', N'20', CAST(1 AS Numeric(2, 0)), 15)
INSERT [dbo].[Trip] ([ID], [Destination], [TripType], [Capacity], [Duration], [Price]) VALUES (5, N'Teatr', N'Event', N'30', CAST(1 AS Numeric(2, 0)), 17)
INSERT [dbo].[Trip] ([ID], [Destination], [TripType], [Capacity], [Duration], [Price]) VALUES (6, N'Czerwone Wierchy', N'Hiking', N'10', CAST(3 AS Numeric(2, 0)), 150)
INSERT [dbo].[Trip] ([ID], [Destination], [TripType], [Capacity], [Duration], [Price]) VALUES (7, N'Wrocław', N'City Tour', N'40', CAST(2 AS Numeric(2, 0)), 100)
INSERT [dbo].[Trip] ([ID], [Destination], [TripType], [Capacity], [Duration], [Price]) VALUES (8, N'Kręgle', N'Event', N'12', CAST(1 AS Numeric(2, 0)), 10)
INSERT [dbo].[Trip] ([ID], [Destination], [TripType], [Capacity], [Duration], [Price]) VALUES (9, N'Góry Sowie', N'Hiking', N'20', CAST(1 AS Numeric(2, 0)), 25)
SET IDENTITY_INSERT [dbo].[Trip] OFF
ALTER TABLE [dbo].[Contact]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Mountains]  WITH CHECK ADD FOREIGN KEY([TripID])
REFERENCES [dbo].[Trip] ([ID])
GO
ALTER TABLE [dbo].[Oneday]  WITH CHECK ADD FOREIGN KEY([TripID])
REFERENCES [dbo].[Trip] ([ID])
GO
ALTER TABLE [dbo].[Participants]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Participants]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Participants]  WITH CHECK ADD FOREIGN KEY([TripID])
REFERENCES [dbo].[Trip] ([ID])
GO
ALTER TABLE [dbo].[Participants]  WITH CHECK ADD FOREIGN KEY([TripID])
REFERENCES [dbo].[Trip] ([ID])
GO
USE [master]
GO
ALTER DATABASE [AnnaGudelajtisLab3ZadanieDomowe] SET  READ_WRITE 
GO
