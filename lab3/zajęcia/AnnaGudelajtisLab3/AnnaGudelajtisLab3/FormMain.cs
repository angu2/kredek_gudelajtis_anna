﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AnnaGudelajtisLab3
{
    public partial class FormMain : Form
    {
        // Połączenie z bazą danych
        SqlConnection connection;
        public FormMain()
        {
            InitializeComponent();
            connection = new SqlConnection(@"Data Source = ANIA-PC\ANIAGUDSQL; 
                                            database=AnnaGudelajtisLab3;
                                            Trusted_Connection=yes");
        }

        /// <summary>
        /// Wyświetlenie widoku ViewGrades
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonViewGrades_Click(object sender, EventArgs e)
        {
            ShowData("GradesView");
            /*
            //utworzenie obiektu do odczytu bazy danych
            SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM GradesView", connection);
            DataTable table = new DataTable();
            //wypełnienie tablicy danymi z bazy
            dataAdapter.Fill(table);
            //wyświetlenie bazy danych w dataGridView
            dataGridViewData.DataSource = table;
            */
        }

        /// <summary>
        /// Dodawanie nowego kursu do bazy danych
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddCourse_Click(object sender, EventArgs e)
        {
            //utworzenie kwerendy
            SqlCommand command = new SqlCommand(@"INSERT INTO Courses(Name, Teacher) 
                                                     VALUES (@Name, @Teacher) ", connection);
            //dodanie parametrów kwerendy
            command.Parameters.Add("@Name", SqlDbType.NVarChar).Value = textBoxCourseName.Text;
            command.Parameters.Add("@Teacher", SqlDbType.NVarChar).Value = textBoxTeacherName.Text;
            //otwiera połączenie
            connection.Open();
            //wykonanie kwerendy
            command.ExecuteNonQuery();
            //zamyka połączenie
            connection.Close();
        }

        /// <summary>
        /// wyświetlenie dowolnych danych z tabel
        /// </summary>
        /// <param name="table"></param>
        private void ShowData(string tableName)
        {
            //utworzenie obiektu do odczytu bazy danych
            SqlDataAdapter dataAdapter = new SqlDataAdapter($"SELECT * FROM {tableName}", connection);
            DataTable table = new DataTable();
            //wypełnienie tablicy danymi z bazy
            dataAdapter.Fill(table);
            //wyświetlenie bazy danych w dataGridView
            dataGridViewData.DataSource = table;
        }

        private void buttonViewCourses_Click(object sender, EventArgs e)
        {
            ShowData("Courses");
        }
    }
}

///utworzenie aplikacji bazodanowej
///min 5 tabel
///powiazane z tematyka programu, nowa apka, nie kontynuacja
///konwencja dowolna
///uzywanie widoków
///operacja JOIN polaczenia tabel
///dodawanie, usuwanie, edytowanie elementow - insert, delete, select?from where dla min 1 tabeli
///ciekawy i pomyslowy program
///timer, progressbar,itp.
///w katalogu kod, plik script z danymi
///zarządzanie danymi
