﻿namespace AnnaGudelajtisLab3
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelMyName = new System.Windows.Forms.Label();
            this.dataGridViewData = new System.Windows.Forms.DataGridView();
            this.buttonViewGrades = new System.Windows.Forms.Button();
            this.textBoxTeacherName = new System.Windows.Forms.TextBox();
            this.textBoxCourseName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelCourseName = new System.Windows.Forms.Label();
            this.buttonAddCourse = new System.Windows.Forms.Button();
            this.buttonViewCourses = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).BeginInit();
            this.SuspendLayout();
            // 
            // labelMyName
            // 
            this.labelMyName.AutoSize = true;
            this.labelMyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMyName.Location = new System.Drawing.Point(271, 35);
            this.labelMyName.Name = "labelMyName";
            this.labelMyName.Size = new System.Drawing.Size(224, 32);
            this.labelMyName.TabIndex = 0;
            this.labelMyName.Text = "Ania Gudełajtis";
            // 
            // dataGridViewData
            // 
            this.dataGridViewData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewData.Location = new System.Drawing.Point(28, 95);
            this.dataGridViewData.Name = "dataGridViewData";
            this.dataGridViewData.RowTemplate.Height = 24;
            this.dataGridViewData.Size = new System.Drawing.Size(734, 389);
            this.dataGridViewData.TabIndex = 1;
            // 
            // buttonViewGrades
            // 
            this.buttonViewGrades.Location = new System.Drawing.Point(875, 95);
            this.buttonViewGrades.Name = "buttonViewGrades";
            this.buttonViewGrades.Size = new System.Drawing.Size(125, 51);
            this.buttonViewGrades.TabIndex = 2;
            this.buttonViewGrades.Text = "View Grades";
            this.buttonViewGrades.UseVisualStyleBackColor = true;
            this.buttonViewGrades.Click += new System.EventHandler(this.buttonViewGrades_Click);
            // 
            // textBoxTeacherName
            // 
            this.textBoxTeacherName.Location = new System.Drawing.Point(875, 197);
            this.textBoxTeacherName.Name = "textBoxTeacherName";
            this.textBoxTeacherName.Size = new System.Drawing.Size(125, 22);
            this.textBoxTeacherName.TabIndex = 3;
            // 
            // textBoxCourseName
            // 
            this.textBoxCourseName.Location = new System.Drawing.Point(875, 261);
            this.textBoxCourseName.Name = "textBoxCourseName";
            this.textBoxCourseName.Size = new System.Drawing.Size(125, 22);
            this.textBoxCourseName.TabIndex = 4;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(875, 174);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(110, 17);
            this.labelName.TabIndex = 5;
            this.labelName.Text = "Imię wykładowcy";
            // 
            // labelCourseName
            // 
            this.labelCourseName.AutoSize = true;
            this.labelCourseName.Location = new System.Drawing.Point(875, 231);
            this.labelCourseName.Name = "labelCourseName";
            this.labelCourseName.Size = new System.Drawing.Size(91, 17);
            this.labelCourseName.TabIndex = 6;
            this.labelCourseName.Text = "Nazwa Kursu";
            // 
            // buttonAddCourse
            // 
            this.buttonAddCourse.Location = new System.Drawing.Point(875, 315);
            this.buttonAddCourse.Name = "buttonAddCourse";
            this.buttonAddCourse.Size = new System.Drawing.Size(125, 51);
            this.buttonAddCourse.TabIndex = 7;
            this.buttonAddCourse.Text = "Dodaj kurs";
            this.buttonAddCourse.UseVisualStyleBackColor = true;
            this.buttonAddCourse.Click += new System.EventHandler(this.buttonAddCourse_Click);
            // 
            // buttonViewCourses
            // 
            this.buttonViewCourses.Location = new System.Drawing.Point(875, 433);
            this.buttonViewCourses.Name = "buttonViewCourses";
            this.buttonViewCourses.Size = new System.Drawing.Size(125, 51);
            this.buttonViewCourses.TabIndex = 8;
            this.buttonViewCourses.Text = "Zobacz kurs";
            this.buttonViewCourses.UseVisualStyleBackColor = true;
            this.buttonViewCourses.Click += new System.EventHandler(this.buttonViewCourses_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 520);
            this.Controls.Add(this.buttonViewCourses);
            this.Controls.Add(this.buttonAddCourse);
            this.Controls.Add(this.labelCourseName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxCourseName);
            this.Controls.Add(this.textBoxTeacherName);
            this.Controls.Add(this.buttonViewGrades);
            this.Controls.Add(this.dataGridViewData);
            this.Controls.Add(this.labelMyName);
            this.Name = "FormMain";
            this.Text = "FormMain";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMyName;
        private System.Windows.Forms.DataGridView dataGridViewData;
        private System.Windows.Forms.Button buttonViewGrades;
        private System.Windows.Forms.TextBox textBoxTeacherName;
        private System.Windows.Forms.TextBox textBoxCourseName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelCourseName;
        private System.Windows.Forms.Button buttonAddCourse;
        private System.Windows.Forms.Button buttonViewCourses;
    }
}

