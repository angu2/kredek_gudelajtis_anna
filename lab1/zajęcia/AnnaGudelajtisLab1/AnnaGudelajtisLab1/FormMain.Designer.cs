﻿namespace AnnaGudelajtisLab1
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonClose = new System.Windows.Forms.Button();
            this.name = new System.Windows.Forms.Label();
            this.buttonChange = new System.Windows.Forms.Button();
            this.textBoxData = new System.Windows.Forms.TextBox();
            this.buttonMore = new System.Windows.Forms.Button();
            this.textBoxVariable1 = new System.Windows.Forms.TextBox();
            this.textBoxVariable2 = new System.Windows.Forms.TextBox();
            this.labelSign = new System.Windows.Forms.Label();
            this.buttonMultiply = new System.Windows.Forms.Button();
            this.labelResult = new System.Windows.Forms.Label();
            this.timerCounter = new System.Windows.Forms.Timer(this.components);
            this.buttonStart = new System.Windows.Forms.Button();
            this.labelSignEquals = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(592, 374);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(106, 63);
            this.buttonClose.TabIndex = 0;
            this.buttonClose.Text = "Zamknij";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.name.Location = new System.Drawing.Point(26, 24);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(596, 91);
            this.name.TabIndex = 1;
            this.name.Text = "Anna Gudełajtis";
            // 
            // buttonChange
            // 
            this.buttonChange.Location = new System.Drawing.Point(42, 135);
            this.buttonChange.Name = "buttonChange";
            this.buttonChange.Size = new System.Drawing.Size(106, 63);
            this.buttonChange.TabIndex = 2;
            this.buttonChange.Text = "Zmień";
            this.buttonChange.UseVisualStyleBackColor = true;
            this.buttonChange.Click += new System.EventHandler(this.buttonChange_Click);
            // 
            // textBoxData
            // 
            this.textBoxData.Location = new System.Drawing.Point(42, 221);
            this.textBoxData.Name = "textBoxData";
            this.textBoxData.Size = new System.Drawing.Size(106, 22);
            this.textBoxData.TabIndex = 3;
            this.textBoxData.Text = "1";
            // 
            // buttonMore
            // 
            this.buttonMore.Location = new System.Drawing.Point(191, 221);
            this.buttonMore.Name = "buttonMore";
            this.buttonMore.Size = new System.Drawing.Size(106, 63);
            this.buttonMore.TabIndex = 4;
            this.buttonMore.Text = "Więcej";
            this.buttonMore.UseVisualStyleBackColor = true;
            this.buttonMore.Click += new System.EventHandler(this.buttonMore_Click);
            // 
            // textBoxVariable1
            // 
            this.textBoxVariable1.Location = new System.Drawing.Point(42, 323);
            this.textBoxVariable1.Name = "textBoxVariable1";
            this.textBoxVariable1.Size = new System.Drawing.Size(106, 22);
            this.textBoxVariable1.TabIndex = 5;
            // 
            // textBoxVariable2
            // 
            this.textBoxVariable2.Location = new System.Drawing.Point(191, 323);
            this.textBoxVariable2.Name = "textBoxVariable2";
            this.textBoxVariable2.Size = new System.Drawing.Size(106, 22);
            this.textBoxVariable2.TabIndex = 6;
            // 
            // labelSign
            // 
            this.labelSign.AutoSize = true;
            this.labelSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSign.Location = new System.Drawing.Point(158, 319);
            this.labelSign.Name = "labelSign";
            this.labelSign.Size = new System.Drawing.Size(27, 36);
            this.labelSign.TabIndex = 7;
            this.labelSign.Text = "*";
            // 
            // buttonMultiply
            // 
            this.buttonMultiply.Location = new System.Drawing.Point(191, 374);
            this.buttonMultiply.Name = "buttonMultiply";
            this.buttonMultiply.Size = new System.Drawing.Size(106, 64);
            this.buttonMultiply.TabIndex = 8;
            this.buttonMultiply.Text = "Oblicz";
            this.buttonMultiply.UseVisualStyleBackColor = true;
            this.buttonMultiply.Click += new System.EventHandler(this.buttonMultiply_Click);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelResult.Location = new System.Drawing.Point(339, 319);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(23, 25);
            this.labelResult.TabIndex = 9;
            this.labelResult.Text = "?";
            // 
            // timerCounter
            // 
            this.timerCounter.Tick += new System.EventHandler(this.timerCounter_Tick);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(391, 374);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(106, 63);
            this.buttonStart.TabIndex = 10;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // labelSignEquals
            // 
            this.labelSignEquals.AutoSize = true;
            this.labelSignEquals.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSignEquals.Location = new System.Drawing.Point(310, 319);
            this.labelSignEquals.Name = "labelSignEquals";
            this.labelSignEquals.Size = new System.Drawing.Size(24, 25);
            this.labelSignEquals.TabIndex = 11;
            this.labelSignEquals.Text = "=";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 450);
            this.Controls.Add(this.labelSignEquals);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.buttonMultiply);
            this.Controls.Add(this.labelSign);
            this.Controls.Add(this.textBoxVariable2);
            this.Controls.Add(this.textBoxVariable1);
            this.Controls.Add(this.buttonMore);
            this.Controls.Add(this.textBoxData);
            this.Controls.Add(this.buttonChange);
            this.Controls.Add(this.name);
            this.Controls.Add(this.buttonClose);
            this.Name = "FormMain";
            this.Text = "FormMain";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Button buttonChange;
        private System.Windows.Forms.TextBox textBoxData;
        private System.Windows.Forms.Button buttonMore;
        private System.Windows.Forms.TextBox textBoxVariable1;
        private System.Windows.Forms.TextBox textBoxVariable2;
        private System.Windows.Forms.Label labelSign;
        private System.Windows.Forms.Button buttonMultiply;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Timer timerCounter;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label labelSignEquals;
    }
}

