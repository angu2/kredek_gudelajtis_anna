﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnaGudelajtisLab1
{
    public partial class FormMain : Form
    {
        int counter = 0;    //domyślnie private

        public FormMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Funkcja zamykająca okienko
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            //Zamknięcie okna
            Close();
        }

        /// <summary>
        /// Funkcja zmieniająca kolor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChange_Click(object sender, EventArgs e)
        {
            //Ustawienie koloru tła
            this.BackColor = Color.LightBlue;    //(SystemColors.Comtrol - domyślny kolor tła)

            //Zmiana koloru przycisku Zamknij
            buttonClose.BackColor = Color.RosyBrown;
        }

        /// <summary>
        /// Zmienia wartość w textBoxData
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMore_Click(object sender, EventArgs e)
        {
            // Zmienna przechowująca wartość textBoxa
            string dataString = " ";
            // Przypisanie wartości textBoxa do zmiennej typu string
            dataString = textBoxData.Text;
            // Zamiana typów zmiennych, parsowanie
            counter = Int32.Parse(dataString);
            // Zwiększanie licznika
            counter++;
            // Wyświetlenie nowej wartości w okienku
            // MessageBox.Show(counter.ToString());
            // Zmiana zawartości textBoxa
            textBoxData.Text = counter.ToString();
        }

        /// <summary>
        /// Funkcja mnożąca 2 liczby
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMultiply_Click(object sender, EventArgs e)
        {
            // Liczby do pomnożenia
            int variableA;
            int variableB;
            // Zmiana typu zmiennych
            variableA = Int32.Parse(textBoxVariable1.Text);
            variableB = Int32.Parse(textBoxVariable2.Text);
            // Obliczenie wyniku
            int result = variableA * variableB;
            // Przypisanie wyniku do etykietki
            labelResult.Text = result.ToString();
        }

        /// <summary>
        ///  Funkcja uruchamiająca zegar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStart_Click(object sender, EventArgs e)
        {
            // Ustawia interwał
            timerCounter.Interval = 1000;
            // Uruchamia zegar
            timerCounter.Start();
        }

        /// <summary>
        /// Funkcja uruchamiana po każdym interwale zegara
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerCounter_Tick(object sender, EventArgs e)
        {
            // Zwiększa licznik
            counter++;
            // Wyświetla nową wartość licznika
            textBoxData.Text = counter.ToString();
            // Zmienia tło, gdy licznik osiąga min 10
            if (counter > 9)
                this.BackColor = Color.BlueViolet;
        }
    }
}

// zadanie domowe - termin poniedziałek programistyczny wieczór
// na repozytorium git
// 0 - program z lab - mailem
// 1 - program symuluje działanie przedsiębiorstwa / imperium
// pojawianie sie min 5 zasaobów w zaleznosci od czasu, przybywają z upływem czasu
// tartak / kamieniołom / zloto na rozbudowe i przybywa szybciej
// lepszy magazyn
// zasoby powiazane warunkami (if) - np koniec miejsca / zasobow
// zdarzenia negatywne / katastrofy np po 100 intewalach ubywa surowcow
// mozliwosc ich unikniecia
// !! pomysl i estetyka, ciekawostki graficzne; elementy audio/ picture box

