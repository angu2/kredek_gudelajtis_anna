﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnaGudelajtisZadanieDomoweLab1
{
    public partial class MainForm : Form
    {
        int timeCounter = 0;        //liczy czas
        int animals = 0;            //liczy zwierzeta
        int animalsCapacity = 2;    //pojemnosc na zwierzeta w zagrodzie
        int goal = 100;             //cel gry
        int wood = 0;               //posiadana ilosc drewna
        int stone = 0;              //posiadana ilosc kamienia
        int steel = 0;              //posiadana ilosc zelaza
        int food = 0;              //posiadana ilosc zywnosci
        int woodProduction = 10;    //produkcja drewna
        int stoneProduction = 10;   //produkcja kamienia
        int steelProduction = 10;   //produkcja zelaza
        int foodProduction = 10;   //produkcja zywności

        int sawmillLevel = 1;       //poziom tartaku
        int stonePitLevel = 1;       //poziom kamieniołomu
        int mineLevel = 1;       //poziom kopalni
        int foodLevel = 1;       //poziom zywnosci
        int farmLevel = 1;          //poziom zagrody
        int hospitalLevel = 0;          //poziom szpitala
        int wallLevel = 0;          //poziom muru


        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Funkcja dodaje zasoby według ich aktualnej produkcji, wywoływana przy każdym interwale czasu
        /// </summary>
        private void AddResources()
        {
            //Dodaje drewno wg produkcji i wyświetla jego nową wartość
            wood += woodProduction;
            labelWood.Text = wood.ToString();
            //Dodaje kamień wg produkcji i wyświetla jego nową wartość
            stone += stoneProduction;
            labelStone.Text = stone.ToString();
            //Dodaje żelazo wg produkcji i wyświetla jego nową wartość
            steel += steelProduction;
            labelSteel.Text = steel.ToString();
            //Dodaje żywność wg produkcji i wyświetla jej nową wartość
            food += foodProduction;
            labelFood.Text = food.ToString();
        }
        
        /// <summary>
        /// Funkcja rozpoczyna symulacje, aktywuje zegar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStart_Click(object sender, EventArgs e)
        {
            //Zmiana koloru przycisku i jego dezaktywacja
            buttonStart.BackColor = Color.Transparent;
            buttonStart.Enabled = false;
            //Zmiana tekstu etykiet na czas i liczbę ludności
            labelTime.Text = "Czas: " + timeCounter.ToString();
            labelGoal.Text = "Zwierzęta:  " + animals.ToString() + "  /  " + goal;
            // Uruchamia zegar
            timerCounter.Start();
        }

        /// <summary>
        /// Funkcja zamykająca okno
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            //Zamyka okno
            Close();
        }

        /// <summary>
        /// Funkcja uruchamiana po każdym interwale zegara - przybywanie surowców, katastrofy uzależnione od czasu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerCounter_Tick(object sender, EventArgs e)
        {
            //Liczy czas i wyświetla go 
            timeCounter++;
            labelTime.Text = "Czas: " + timeCounter.ToString();
            //Dodaje zasoby
            AddResources();
            //KATASTROFY
            //ZARAZA co 100s
            if (timeCounter % 100 == 0)
            {
                //liczy strate w zaleznosci od poziomu szpitala
                int lost = animals * (35 - 15 * hospitalLevel) / 100;
                //usuwa odpowiednio zwierzeta i wyswietla nowa wartosc
                animals -= lost;
                labelAnimals.Text = animals.ToString();
                labelGoal.Text = "Zwierzęta:  " + animals.ToString() + "  /  " + goal;
                //wyswietla komunikat o smierci zwierzat
                MessageBox.Show("Panuje ZARAZA !! Giną zwierzęta: " + lost);
            }
            //RABUNEK co 100s z przesunięciem o 50s
            if (timeCounter % 100 == 50)
            {
                //liczy straty poszczegolnych zasobow w zaleznosci od poziomu muru
                int woodLost = wood * (50 - 20 * wallLevel) / 100;
                int stoneLost = stone * (50 - 20 * wallLevel) / 100;
                int steelLost = steel * (50 - 20 * wallLevel) / 100;
                int foodLost = food * (50 - 20 * wallLevel) / 100;
                //usuwa odpowiednio zasoby i wyswietla ich nowe wartosci
                wood -= woodLost;
                stone -= stoneLost;
                steel -= steelLost;
                food -= foodLost;
                labelWood.Text = wood.ToString();
                labelStone.Text = stone.ToString();
                labelSteel.Text = steel.ToString();
                labelFood.Text = food.ToString();
                //wyswietla komunikat o kradzieży zasobów
                MessageBox.Show("Wkradł się ZŁODZIEJ !! Zrabowano zasoby! drewno:  " + woodLost + " kamień: " + stoneLost + " żelazo: " + steelLost + " żywność: " + foodLost);
            }
        }

        /// <summary>
        /// Funkcja rozbudowa tartaku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildSawmill_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na robudowe
            if (wood >= 150 * sawmillLevel & stone >= 150 * sawmillLevel & steel >= 150 * sawmillLevel)
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 150 * sawmillLevel;
                stone -= 150 * sawmillLevel;
                steel -= 150 * sawmillLevel;
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                sawmillLevel++;
                labelSawmillLevel.Text = "Poziom " + sawmillLevel;
                //Zwiększa produkcje zasobu i wyswietla nową w etykietce
                woodProduction = sawmillLevel * 15;
                labelWoodProduction.Text = woodProduction.ToString() + " / s";
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelSawmillCost.Text = (150 * sawmillLevel).ToString();
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 150 * sawmillLevel + " wszystkiego");
            
        }

        /// <summary>
        /// Funkcja rozbudowa kamieniołomu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildStonePit_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na robudowe
            if (wood >= 150 * stonePitLevel & stone >= 150 * stonePitLevel & steel >= 150 * stonePitLevel)
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 150 * stonePitLevel;
                stone -= 150 * stonePitLevel;
                steel -= 150 * stonePitLevel;
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                stonePitLevel++;
                labelStonePitLevel.Text = "Poziom " + stonePitLevel;
                //Zwiększa produkcje zasobu i wyswietla nową w etykietce
                stoneProduction = stonePitLevel * 15;
                labelStoneProduction.Text = stoneProduction.ToString() + " / s";
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelStonePitCost.Text = (150 * stonePitLevel).ToString();
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 150 * stonePitLevel + " wszystkiego");
        }

        /// <summary>
        /// Funkcja rozbudowa kopalni
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildMine_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na robudowe
            if (wood >= 150 * mineLevel & stone >= 150 * mineLevel & steel >= 150 * mineLevel)
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 150 * mineLevel;
                stone -= 150 * mineLevel;
                steel -= 150 * mineLevel;
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                mineLevel++;
                labelMineLevel.Text = "Poziom " + mineLevel;
                //Zwiększa produkcje zasobu i wyswietla nową w etykietce
                steelProduction = mineLevel * 15;
                labelSteelProduction.Text = steelProduction.ToString() + " / s";
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelMineCost.Text = (150 * mineLevel).ToString();
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 150 * mineLevel + " wszystkiego");
        }

        /// <summary>
        /// Funkcja rozbudowa zywnosci
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildFood_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na robudowe
            if (wood >= 150 * foodLevel & stone >= 150 * foodLevel & steel >= 150 * foodLevel)
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 150 * foodLevel;
                stone -= 150 * foodLevel;
                steel -= 150 * foodLevel;
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                foodLevel++;
                labelFoodLevel.Text = "Poziom " + foodLevel;
                //Zwiększa produkcje zasobu i wyswietla nową w etykietce
                foodProduction = foodLevel * 10;
                labelFoodProduction.Text = foodProduction.ToString() + " / s";
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelFoodCost.Text = (150 * foodLevel).ToString();
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 150 * foodLevel + " wszystkiego");
        }

        /// <summary>
        /// Funkcja rozbudowa zagrody
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildFarm_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na robudowe
            if (wood >= 150 * farmLevel & stone >= 150 * farmLevel & steel >= 150 * farmLevel & food >= 150* farmLevel)
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 150 * farmLevel;
                stone -= 150 * farmLevel;
                steel -= 150 * farmLevel;
                food -= 150 * farmLevel;
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                farmLevel++;
                labelFarmLevel.Text = "Poziom " + farmLevel;
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelFarmCost.Text = (150 * farmLevel).ToString();
                //Zwieksza pojemnosc zagrody i wyswietla nową w etykietce
                animalsCapacity = farmLevel * 10;
                labelAnimalsCapacity.Text = "max " + animalsCapacity;
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 150 * farmLevel + " wszystkich surowców i żywności");
        }

        /// <summary>
        /// Funkcja kopowania zwierzęcia
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuyAnimal_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zywnosci na kupno i miejsca w zagrodzie
            if (food >= 200 & animalsCapacity > animals)
            {
                //Zmniejsza odpowiednio ilość zywnosci
                food -= 200;
                //Zwieksza ilosci zwierzat i wyswietla nową w etykietkach
                animals++;
                labelAnimals.Text = animals.ToString();
                labelGoal.Text = "Zwierzęta:  " + animals.ToString() + "  /  " + goal;
                //Sprawdza czy cel zostal osiągniety
                if (animals==goal )
                {
                    //Zatrzymuje zegar
                    timerCounter.Stop();
                    //Wyswietla wiadomosc o wygranej i czasie rozgrywki
                    MessageBox.Show("Wygrałeś!! Cel został osiągnięty w czasie: " + timeCounter + " s.");
                }
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu i potrzebach przed kupnem
                MessageBox.Show("Nie wystarczająco żywnosci lub miejsca w zagrodzie. Potrzeba min 200 żywności.");
        }

        /// <summary>
        /// Funkcja budowa szpitala, zmniejsza skutki ZARAZY
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildHospital_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na budowe
            if (wood >= 500 * (hospitalLevel+1) & stone >= 500 * (hospitalLevel + 1) & steel >= 500 * (hospitalLevel + 1))
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 500 * (hospitalLevel + 1);
                stone -= 500 * (hospitalLevel + 1);
                steel -= 500 * (hospitalLevel + 1);
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                hospitalLevel++;
                labelHospitalLevel.Text = "Poziom " + hospitalLevel;
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelHospitalCost.Text = (500 * (hospitalLevel + 1)).ToString();
                //Zmniejsza skutki ZARAZY - wyswietla wartos w etykkietce
                labelVirusChance.Text = "Uśmierca " + (35 - 15 * hospitalLevel) + "%";
                // Zmienia guzik na 'zbuduj' na 'ulepsz'
                buttonBuildHospital.Text = "Ulepsz";
                // Uniemożliwia dalsza rozbudowe po uzyskaniu poziomu 2
                if (hospitalLevel == 2)
                {
                    buttonBuildHospital.Enabled = false;
                }
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 500 * (hospitalLevel + 1) + " wszystkich surowców.");
        }

        /// <summary>
        /// Funkcja budowa muru, zmniejsza skutki rabunku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildWall_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na budowe
            if (wood >= 500 * (wallLevel + 1) & stone >= 500 * (wallLevel + 1) & steel >= 500 * (wallLevel + 1))
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 500 * (wallLevel + 1);
                stone -= 500 * (wallLevel + 1);
                steel -= 500 * (wallLevel + 1);
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                wallLevel++;
                labelWallLevel.Text = "Poziom " + wallLevel;
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelWallCost.Text = (500 * (wallLevel + 1)).ToString();
                //Zmniejsza skutki RABUNKU - wyswietla wartosc w etykietce
                labelThiefChance.Text = "Znika " + (50 - 20 * wallLevel) + "%";
                // Zmienia guzik na 'zbuduj' na 'ulepsz'
                buttonBuildWall.Text = "Ulepsz";
                // Uniemożliwia dalsza rozbudowe po uzyskaniu poziomu 2
                if (wallLevel == 2)
                {
                    buttonBuildWall.Enabled = false;
                }
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 500 * (wallLevel + 1) + " wszystkich surowców.");
        }

        /// <summary>
        /// Zmienia cel gry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChangeGoal_Click(object sender, EventArgs e)
        {
            //Sprawdza czy nowy cel jest odpowiedni
            if (Int32.Parse(textBoxGoal.Text) > animals)
            {
                //Ustawia nowy cel
                goal = Int32.Parse(textBoxGoal.Text);
                //Wyswietla nowy cel w etykietce w zależności od tego czy juz rozpoczęto gre
                if (buttonStart.Enabled == true)
                    labelGoal.Text = "Cel:  " + goal + " zwierząt";
                else
                    labelGoal.Text = "Zwierzęta:  " + animals.ToString() + "  /  " + goal;
            }
            else
                MessageBox.Show("Cel jest za niski, już został osiągnięty! Nie można zmienić.");
        }
    }
}
