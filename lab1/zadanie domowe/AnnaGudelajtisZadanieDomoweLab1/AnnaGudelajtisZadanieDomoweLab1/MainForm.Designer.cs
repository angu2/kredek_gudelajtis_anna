﻿namespace AnnaGudelajtisZadanieDomoweLab1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.buttonStart = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabMainView = new System.Windows.Forms.TabPage();
            this.labelAnimalCost = new System.Windows.Forms.Label();
            this.labelAdditionalCostAnimal = new System.Windows.Forms.Label();
            this.buttonBuyAnimal = new System.Windows.Forms.Button();
            this.labelWallCost = new System.Windows.Forms.Label();
            this.labelHospitalCost = new System.Windows.Forms.Label();
            this.labelDangers = new System.Windows.Forms.Label();
            this.labelAnimalsCapacity = new System.Windows.Forms.Label();
            this.labelAnimals = new System.Windows.Forms.Label();
            this.labelAdditionalCost = new System.Windows.Forms.Label();
            this.labelFarmCost = new System.Windows.Forms.Label();
            this.buttonBuildFarm = new System.Windows.Forms.Button();
            this.labelFoodCost = new System.Windows.Forms.Label();
            this.labelMineCost = new System.Windows.Forms.Label();
            this.labelStonePitCost = new System.Windows.Forms.Label();
            this.labelSawmillCost = new System.Windows.Forms.Label();
            this.labelCost = new System.Windows.Forms.Label();
            this.buttonBuildWall = new System.Windows.Forms.Button();
            this.buttonBuildHospital = new System.Windows.Forms.Button();
            this.labelThief = new System.Windows.Forms.Label();
            this.labelVirus = new System.Windows.Forms.Label();
            this.labelThiefProtection = new System.Windows.Forms.Label();
            this.labelVirusProtection = new System.Windows.Forms.Label();
            this.labelWall = new System.Windows.Forms.Label();
            this.labelHospital = new System.Windows.Forms.Label();
            this.labelWallLevel = new System.Windows.Forms.Label();
            this.labelHospitalLevel = new System.Windows.Forms.Label();
            this.pictureBoxWall = new System.Windows.Forms.PictureBox();
            this.pictureBoxHospital = new System.Windows.Forms.PictureBox();
            this.buttonBuildFood = new System.Windows.Forms.Button();
            this.labelFoodProduction = new System.Windows.Forms.Label();
            this.labelFood = new System.Windows.Forms.Label();
            this.labelFoodLevel = new System.Windows.Forms.Label();
            this.labelFoodField = new System.Windows.Forms.Label();
            this.pictureBoxFood = new System.Windows.Forms.PictureBox();
            this.labelFarmLevel = new System.Windows.Forms.Label();
            this.labelFarm = new System.Windows.Forms.Label();
            this.labelThiefChance = new System.Windows.Forms.Label();
            this.labelVirusChance = new System.Windows.Forms.Label();
            this.pictureBoxVirus = new System.Windows.Forms.PictureBox();
            this.pictureBoxThief = new System.Windows.Forms.PictureBox();
            this.pictureBoxAnimal = new System.Windows.Forms.PictureBox();
            this.buttonBuildMine = new System.Windows.Forms.Button();
            this.labelMineLevel = new System.Windows.Forms.Label();
            this.buttonBuildStonePit = new System.Windows.Forms.Button();
            this.buttonBuildSawmill = new System.Windows.Forms.Button();
            this.labelStonePitLevel = new System.Windows.Forms.Label();
            this.labelSawmillLevel = new System.Windows.Forms.Label();
            this.pictureBoxMine = new System.Windows.Forms.PictureBox();
            this.pictureBoxStonePit = new System.Windows.Forms.PictureBox();
            this.pictureBoxSawmill = new System.Windows.Forms.PictureBox();
            this.labelSteel = new System.Windows.Forms.Label();
            this.labelSteelProduction = new System.Windows.Forms.Label();
            this.labelStoneProduction = new System.Windows.Forms.Label();
            this.labelStone = new System.Windows.Forms.Label();
            this.labelWoodProduction = new System.Windows.Forms.Label();
            this.labelWood = new System.Windows.Forms.Label();
            this.labelMine = new System.Windows.Forms.Label();
            this.labelStonePit = new System.Windows.Forms.Label();
            this.labelSawmill = new System.Windows.Forms.Label();
            this.tabDetails = new System.Windows.Forms.TabPage();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelGoal = new System.Windows.Forms.Label();
            this.timerCounter = new System.Windows.Forms.Timer(this.components);
            this.labelChangeGoal = new System.Windows.Forms.Label();
            this.textBoxGoal = new System.Windows.Forms.TextBox();
            this.buttonChangeGoal = new System.Windows.Forms.Button();
            this.labelDetailsGoal = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tabMainView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHospital)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVirus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxThief)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAnimal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStonePit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSawmill)).BeginInit();
            this.tabDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.BackColor = System.Drawing.Color.PaleGreen;
            this.buttonStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonStart.Location = new System.Drawing.Point(866, 40);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(116, 65);
            this.buttonStart.TabIndex = 1;
            this.buttonStart.Text = "START";
            this.buttonStart.UseVisualStyleBackColor = false;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl.Controls.Add(this.tabMainView);
            this.tabControl.Controls.Add(this.tabDetails);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(756, 632);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 2;
            // 
            // tabMainView
            // 
            this.tabMainView.BackColor = System.Drawing.Color.Transparent;
            this.tabMainView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabMainView.Controls.Add(this.labelAnimalCost);
            this.tabMainView.Controls.Add(this.labelAdditionalCostAnimal);
            this.tabMainView.Controls.Add(this.buttonBuyAnimal);
            this.tabMainView.Controls.Add(this.labelWallCost);
            this.tabMainView.Controls.Add(this.labelHospitalCost);
            this.tabMainView.Controls.Add(this.labelDangers);
            this.tabMainView.Controls.Add(this.labelAnimalsCapacity);
            this.tabMainView.Controls.Add(this.labelAnimals);
            this.tabMainView.Controls.Add(this.labelAdditionalCost);
            this.tabMainView.Controls.Add(this.labelFarmCost);
            this.tabMainView.Controls.Add(this.buttonBuildFarm);
            this.tabMainView.Controls.Add(this.labelFoodCost);
            this.tabMainView.Controls.Add(this.labelMineCost);
            this.tabMainView.Controls.Add(this.labelStonePitCost);
            this.tabMainView.Controls.Add(this.labelSawmillCost);
            this.tabMainView.Controls.Add(this.labelCost);
            this.tabMainView.Controls.Add(this.buttonBuildWall);
            this.tabMainView.Controls.Add(this.buttonBuildHospital);
            this.tabMainView.Controls.Add(this.labelThief);
            this.tabMainView.Controls.Add(this.labelVirus);
            this.tabMainView.Controls.Add(this.labelThiefProtection);
            this.tabMainView.Controls.Add(this.labelVirusProtection);
            this.tabMainView.Controls.Add(this.labelWall);
            this.tabMainView.Controls.Add(this.labelHospital);
            this.tabMainView.Controls.Add(this.labelWallLevel);
            this.tabMainView.Controls.Add(this.labelHospitalLevel);
            this.tabMainView.Controls.Add(this.pictureBoxWall);
            this.tabMainView.Controls.Add(this.pictureBoxHospital);
            this.tabMainView.Controls.Add(this.buttonBuildFood);
            this.tabMainView.Controls.Add(this.labelFoodProduction);
            this.tabMainView.Controls.Add(this.labelFood);
            this.tabMainView.Controls.Add(this.labelFoodLevel);
            this.tabMainView.Controls.Add(this.labelFoodField);
            this.tabMainView.Controls.Add(this.pictureBoxFood);
            this.tabMainView.Controls.Add(this.labelFarmLevel);
            this.tabMainView.Controls.Add(this.labelFarm);
            this.tabMainView.Controls.Add(this.labelThiefChance);
            this.tabMainView.Controls.Add(this.labelVirusChance);
            this.tabMainView.Controls.Add(this.pictureBoxVirus);
            this.tabMainView.Controls.Add(this.pictureBoxThief);
            this.tabMainView.Controls.Add(this.pictureBoxAnimal);
            this.tabMainView.Controls.Add(this.buttonBuildMine);
            this.tabMainView.Controls.Add(this.labelMineLevel);
            this.tabMainView.Controls.Add(this.buttonBuildStonePit);
            this.tabMainView.Controls.Add(this.buttonBuildSawmill);
            this.tabMainView.Controls.Add(this.labelStonePitLevel);
            this.tabMainView.Controls.Add(this.labelSawmillLevel);
            this.tabMainView.Controls.Add(this.pictureBoxMine);
            this.tabMainView.Controls.Add(this.pictureBoxStonePit);
            this.tabMainView.Controls.Add(this.pictureBoxSawmill);
            this.tabMainView.Controls.Add(this.labelSteel);
            this.tabMainView.Controls.Add(this.labelSteelProduction);
            this.tabMainView.Controls.Add(this.labelStoneProduction);
            this.tabMainView.Controls.Add(this.labelStone);
            this.tabMainView.Controls.Add(this.labelWoodProduction);
            this.tabMainView.Controls.Add(this.labelWood);
            this.tabMainView.Controls.Add(this.labelMine);
            this.tabMainView.Controls.Add(this.labelStonePit);
            this.tabMainView.Controls.Add(this.labelSawmill);
            this.tabMainView.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabMainView.Location = new System.Drawing.Point(4, 28);
            this.tabMainView.Name = "tabMainView";
            this.tabMainView.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainView.Size = new System.Drawing.Size(748, 600);
            this.tabMainView.TabIndex = 0;
            this.tabMainView.Text = "Podgląd";
            // 
            // labelAnimalCost
            // 
            this.labelAnimalCost.AutoSize = true;
            this.labelAnimalCost.Location = new System.Drawing.Point(670, 308);
            this.labelAnimalCost.Name = "labelAnimalCost";
            this.labelAnimalCost.Size = new System.Drawing.Size(32, 17);
            this.labelAnimalCost.TabIndex = 57;
            this.labelAnimalCost.Text = "200";
            // 
            // labelAdditionalCostAnimal
            // 
            this.labelAdditionalCostAnimal.AutoSize = true;
            this.labelAdditionalCostAnimal.Location = new System.Drawing.Point(651, 278);
            this.labelAdditionalCostAnimal.Name = "labelAdditionalCostAnimal";
            this.labelAdditionalCostAnimal.Size = new System.Drawing.Size(71, 17);
            this.labelAdditionalCostAnimal.TabIndex = 56;
            this.labelAdditionalCostAnimal.Text = "(żywność)";
            // 
            // buttonBuyAnimal
            // 
            this.buttonBuyAnimal.BackColor = System.Drawing.Color.Khaki;
            this.buttonBuyAnimal.Location = new System.Drawing.Point(641, 229);
            this.buttonBuyAnimal.Name = "buttonBuyAnimal";
            this.buttonBuyAnimal.Size = new System.Drawing.Size(81, 46);
            this.buttonBuyAnimal.TabIndex = 55;
            this.buttonBuyAnimal.Text = "Kup zwierzę";
            this.buttonBuyAnimal.UseVisualStyleBackColor = false;
            this.buttonBuyAnimal.Click += new System.EventHandler(this.buttonBuyAnimal_Click);
            // 
            // labelWallCost
            // 
            this.labelWallCost.AutoSize = true;
            this.labelWallCost.Location = new System.Drawing.Point(143, 578);
            this.labelWallCost.Name = "labelWallCost";
            this.labelWallCost.Size = new System.Drawing.Size(32, 17);
            this.labelWallCost.TabIndex = 54;
            this.labelWallCost.Text = "500";
            // 
            // labelHospitalCost
            // 
            this.labelHospitalCost.AutoSize = true;
            this.labelHospitalCost.Location = new System.Drawing.Point(32, 578);
            this.labelHospitalCost.Name = "labelHospitalCost";
            this.labelHospitalCost.Size = new System.Drawing.Size(32, 17);
            this.labelHospitalCost.TabIndex = 53;
            this.labelHospitalCost.Text = "500";
            // 
            // labelDangers
            // 
            this.labelDangers.AutoSize = true;
            this.labelDangers.Location = new System.Drawing.Point(431, 382);
            this.labelDangers.Name = "labelDangers";
            this.labelDangers.Size = new System.Drawing.Size(98, 17);
            this.labelDangers.TabIndex = 52;
            this.labelDangers.Text = "ZAGROŻENIA";
            // 
            // labelAnimalsCapacity
            // 
            this.labelAnimalsCapacity.AutoSize = true;
            this.labelAnimalsCapacity.Location = new System.Drawing.Point(524, 198);
            this.labelAnimalsCapacity.Name = "labelAnimalsCapacity";
            this.labelAnimalsCapacity.Size = new System.Drawing.Size(45, 17);
            this.labelAnimalsCapacity.TabIndex = 51;
            this.labelAnimalsCapacity.Text = "max 2";
            // 
            // labelAnimals
            // 
            this.labelAnimals.AutoSize = true;
            this.labelAnimals.Location = new System.Drawing.Point(540, 168);
            this.labelAnimals.Name = "labelAnimals";
            this.labelAnimals.Size = new System.Drawing.Size(16, 17);
            this.labelAnimals.TabIndex = 50;
            this.labelAnimals.Text = "0";
            // 
            // labelAdditionalCost
            // 
            this.labelAdditionalCost.AutoSize = true;
            this.labelAdditionalCost.Location = new System.Drawing.Point(488, 278);
            this.labelAdditionalCost.Name = "labelAdditionalCost";
            this.labelAdditionalCost.Size = new System.Drawing.Size(123, 17);
            this.labelAdditionalCost.TabIndex = 49;
            this.labelAdditionalCost.Text = "(również żywność)";
            // 
            // labelFarmCost
            // 
            this.labelFarmCost.AutoSize = true;
            this.labelFarmCost.Location = new System.Drawing.Point(524, 308);
            this.labelFarmCost.Name = "labelFarmCost";
            this.labelFarmCost.Size = new System.Drawing.Size(32, 17);
            this.labelFarmCost.TabIndex = 48;
            this.labelFarmCost.Text = "150";
            // 
            // buttonBuildFarm
            // 
            this.buttonBuildFarm.Location = new System.Drawing.Point(505, 229);
            this.buttonBuildFarm.Name = "buttonBuildFarm";
            this.buttonBuildFarm.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildFarm.TabIndex = 47;
            this.buttonBuildFarm.Text = "Buduj";
            this.buttonBuildFarm.UseVisualStyleBackColor = true;
            this.buttonBuildFarm.Click += new System.EventHandler(this.buttonBuildFarm_Click);
            // 
            // labelFoodCost
            // 
            this.labelFoodCost.AutoSize = true;
            this.labelFoodCost.Location = new System.Drawing.Point(407, 308);
            this.labelFoodCost.Name = "labelFoodCost";
            this.labelFoodCost.Size = new System.Drawing.Size(32, 17);
            this.labelFoodCost.TabIndex = 46;
            this.labelFoodCost.Text = "150";
            // 
            // labelMineCost
            // 
            this.labelMineCost.AutoSize = true;
            this.labelMineCost.Location = new System.Drawing.Point(247, 308);
            this.labelMineCost.Name = "labelMineCost";
            this.labelMineCost.Size = new System.Drawing.Size(32, 17);
            this.labelMineCost.TabIndex = 45;
            this.labelMineCost.Text = "150";
            // 
            // labelStonePitCost
            // 
            this.labelStonePitCost.AutoSize = true;
            this.labelStonePitCost.Location = new System.Drawing.Point(143, 308);
            this.labelStonePitCost.Name = "labelStonePitCost";
            this.labelStonePitCost.Size = new System.Drawing.Size(32, 17);
            this.labelStonePitCost.TabIndex = 44;
            this.labelStonePitCost.Text = "150";
            // 
            // labelSawmillCost
            // 
            this.labelSawmillCost.AutoSize = true;
            this.labelSawmillCost.Location = new System.Drawing.Point(37, 308);
            this.labelSawmillCost.Name = "labelSawmillCost";
            this.labelSawmillCost.Size = new System.Drawing.Size(32, 17);
            this.labelSawmillCost.TabIndex = 43;
            this.labelSawmillCost.Text = "150";
            // 
            // labelCost
            // 
            this.labelCost.AutoSize = true;
            this.labelCost.Location = new System.Drawing.Point(66, 278);
            this.labelCost.Name = "labelCost";
            this.labelCost.Size = new System.Drawing.Size(191, 17);
            this.labelCost.TabIndex = 42;
            this.labelCost.Text = "Koszt (każdego z surowców):";
            // 
            // buttonBuildWall
            // 
            this.buttonBuildWall.Location = new System.Drawing.Point(122, 517);
            this.buttonBuildWall.Name = "buttonBuildWall";
            this.buttonBuildWall.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildWall.TabIndex = 41;
            this.buttonBuildWall.Text = "Zbuduj";
            this.buttonBuildWall.UseVisualStyleBackColor = true;
            this.buttonBuildWall.Click += new System.EventHandler(this.buttonBuildWall_Click);
            // 
            // buttonBuildHospital
            // 
            this.buttonBuildHospital.Location = new System.Drawing.Point(15, 517);
            this.buttonBuildHospital.Name = "buttonBuildHospital";
            this.buttonBuildHospital.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildHospital.TabIndex = 40;
            this.buttonBuildHospital.Text = "Zbuduj";
            this.buttonBuildHospital.UseVisualStyleBackColor = true;
            this.buttonBuildHospital.Click += new System.EventHandler(this.buttonBuildHospital_Click);
            // 
            // labelThief
            // 
            this.labelThief.AutoSize = true;
            this.labelThief.Location = new System.Drawing.Point(502, 399);
            this.labelThief.Name = "labelThief";
            this.labelThief.Size = new System.Drawing.Size(74, 17);
            this.labelThief.TabIndex = 39;
            this.labelThief.Text = "RABUNEK";
            // 
            // labelVirus
            // 
            this.labelVirus.AutoSize = true;
            this.labelVirus.Location = new System.Drawing.Point(396, 399);
            this.labelVirus.Name = "labelVirus";
            this.labelVirus.Size = new System.Drawing.Size(63, 17);
            this.labelVirus.TabIndex = 38;
            this.labelVirus.Text = "ZARAZA";
            // 
            // labelThiefProtection
            // 
            this.labelThiefProtection.AutoSize = true;
            this.labelThiefProtection.Location = new System.Drawing.Point(497, 578);
            this.labelThiefProtection.Name = "labelThiefProtection";
            this.labelThiefProtection.Size = new System.Drawing.Size(95, 17);
            this.labelThiefProtection.TabIndex = 37;
            this.labelThiefProtection.Text = "Ochrona: mur";
            // 
            // labelVirusProtection
            // 
            this.labelVirusProtection.AutoSize = true;
            this.labelVirusProtection.Location = new System.Drawing.Point(375, 578);
            this.labelVirusProtection.Name = "labelVirusProtection";
            this.labelVirusProtection.Size = new System.Drawing.Size(111, 17);
            this.labelVirusProtection.TabIndex = 36;
            this.labelVirusProtection.Text = "Ochrona: szpital";
            // 
            // labelWall
            // 
            this.labelWall.AutoSize = true;
            this.labelWall.Location = new System.Drawing.Point(143, 360);
            this.labelWall.Name = "labelWall";
            this.labelWall.Size = new System.Drawing.Size(39, 17);
            this.labelWall.TabIndex = 35;
            this.labelWall.Text = "MUR";
            // 
            // labelHospital
            // 
            this.labelHospital.AutoSize = true;
            this.labelHospital.Location = new System.Drawing.Point(23, 360);
            this.labelHospital.Name = "labelHospital";
            this.labelHospital.Size = new System.Drawing.Size(64, 17);
            this.labelHospital.TabIndex = 34;
            this.labelHospital.Text = "SZPITAL";
            // 
            // labelWallLevel
            // 
            this.labelWallLevel.AutoSize = true;
            this.labelWallLevel.Location = new System.Drawing.Point(145, 382);
            this.labelWallLevel.Name = "labelWallLevel";
            this.labelWallLevel.Size = new System.Drawing.Size(37, 17);
            this.labelWallLevel.TabIndex = 33;
            this.labelWallLevel.Text = "Brak";
            // 
            // labelHospitalLevel
            // 
            this.labelHospitalLevel.AutoSize = true;
            this.labelHospitalLevel.Location = new System.Drawing.Point(37, 382);
            this.labelHospitalLevel.Name = "labelHospitalLevel";
            this.labelHospitalLevel.Size = new System.Drawing.Size(37, 17);
            this.labelHospitalLevel.TabIndex = 32;
            this.labelHospitalLevel.Text = "Brak";
            // 
            // pictureBoxWall
            // 
            this.pictureBoxWall.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconWall;
            this.pictureBoxWall.Location = new System.Drawing.Point(113, 402);
            this.pictureBoxWall.Name = "pictureBoxWall";
            this.pictureBoxWall.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxWall.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWall.TabIndex = 31;
            this.pictureBoxWall.TabStop = false;
            // 
            // pictureBoxHospital
            // 
            this.pictureBoxHospital.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconHospital;
            this.pictureBoxHospital.Location = new System.Drawing.Point(6, 402);
            this.pictureBoxHospital.Name = "pictureBoxHospital";
            this.pictureBoxHospital.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxHospital.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxHospital.TabIndex = 30;
            this.pictureBoxHospital.TabStop = false;
            // 
            // buttonBuildFood
            // 
            this.buttonBuildFood.Location = new System.Drawing.Point(387, 229);
            this.buttonBuildFood.Name = "buttonBuildFood";
            this.buttonBuildFood.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildFood.TabIndex = 29;
            this.buttonBuildFood.Text = "Buduj";
            this.buttonBuildFood.UseVisualStyleBackColor = true;
            this.buttonBuildFood.Click += new System.EventHandler(this.buttonBuildFood_Click);
            // 
            // labelFoodProduction
            // 
            this.labelFoodProduction.AutoSize = true;
            this.labelFoodProduction.Location = new System.Drawing.Point(407, 198);
            this.labelFoodProduction.Name = "labelFoodProduction";
            this.labelFoodProduction.Size = new System.Drawing.Size(43, 17);
            this.labelFoodProduction.TabIndex = 28;
            this.labelFoodProduction.Text = "10 / s";
            // 
            // labelFood
            // 
            this.labelFood.AutoSize = true;
            this.labelFood.Location = new System.Drawing.Point(420, 168);
            this.labelFood.Name = "labelFood";
            this.labelFood.Size = new System.Drawing.Size(16, 17);
            this.labelFood.TabIndex = 27;
            this.labelFood.Text = "0";
            // 
            // labelFoodLevel
            // 
            this.labelFoodLevel.AutoSize = true;
            this.labelFoodLevel.Location = new System.Drawing.Point(393, 30);
            this.labelFoodLevel.Name = "labelFoodLevel";
            this.labelFoodLevel.Size = new System.Drawing.Size(66, 17);
            this.labelFoodLevel.TabIndex = 26;
            this.labelFoodLevel.Text = "Poziom 1";
            // 
            // labelFoodField
            // 
            this.labelFoodField.AutoSize = true;
            this.labelFoodField.Location = new System.Drawing.Point(393, 13);
            this.labelFoodField.Name = "labelFoodField";
            this.labelFoodField.Size = new System.Drawing.Size(78, 17);
            this.labelFoodField.TabIndex = 25;
            this.labelFoodField.Text = "ŻYWNOŚĆ";
            // 
            // pictureBoxFood
            // 
            this.pictureBoxFood.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconFood;
            this.pictureBoxFood.Location = new System.Drawing.Point(378, 61);
            this.pictureBoxFood.Name = "pictureBoxFood";
            this.pictureBoxFood.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxFood.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxFood.TabIndex = 24;
            this.pictureBoxFood.TabStop = false;
            // 
            // labelFarmLevel
            // 
            this.labelFarmLevel.AutoSize = true;
            this.labelFarmLevel.Location = new System.Drawing.Point(509, 30);
            this.labelFarmLevel.Name = "labelFarmLevel";
            this.labelFarmLevel.Size = new System.Drawing.Size(66, 17);
            this.labelFarmLevel.TabIndex = 23;
            this.labelFarmLevel.Text = "Poziom 1";
            // 
            // labelFarm
            // 
            this.labelFarm.AutoSize = true;
            this.labelFarm.Location = new System.Drawing.Point(502, 13);
            this.labelFarm.Name = "labelFarm";
            this.labelFarm.Size = new System.Drawing.Size(77, 17);
            this.labelFarm.TabIndex = 22;
            this.labelFarm.Text = "ZAGRODA";
            // 
            // labelThiefChance
            // 
            this.labelThiefChance.AutoSize = true;
            this.labelThiefChance.Location = new System.Drawing.Point(502, 546);
            this.labelThiefChance.Name = "labelThiefChance";
            this.labelThiefChance.Size = new System.Drawing.Size(79, 17);
            this.labelThiefChance.TabIndex = 21;
            this.labelThiefChance.Text = "Znika 50 %";
            // 
            // labelVirusChance
            // 
            this.labelVirusChance.AutoSize = true;
            this.labelVirusChance.Location = new System.Drawing.Point(376, 546);
            this.labelVirusChance.Name = "labelVirusChance";
            this.labelVirusChance.Size = new System.Drawing.Size(103, 17);
            this.labelVirusChance.TabIndex = 20;
            this.labelVirusChance.Text = "Uśmierca 35 %";
            // 
            // pictureBoxVirus
            // 
            this.pictureBoxVirus.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconVirus;
            this.pictureBoxVirus.Location = new System.Drawing.Point(378, 428);
            this.pictureBoxVirus.Name = "pictureBoxVirus";
            this.pictureBoxVirus.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxVirus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxVirus.TabIndex = 19;
            this.pictureBoxVirus.TabStop = false;
            // 
            // pictureBoxThief
            // 
            this.pictureBoxThief.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconThief;
            this.pictureBoxThief.Location = new System.Drawing.Point(491, 428);
            this.pictureBoxThief.Name = "pictureBoxThief";
            this.pictureBoxThief.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxThief.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxThief.TabIndex = 18;
            this.pictureBoxThief.TabStop = false;
            // 
            // pictureBoxAnimal
            // 
            this.pictureBoxAnimal.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconAnimal;
            this.pictureBoxAnimal.Location = new System.Drawing.Point(491, 61);
            this.pictureBoxAnimal.Name = "pictureBoxAnimal";
            this.pictureBoxAnimal.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxAnimal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxAnimal.TabIndex = 17;
            this.pictureBoxAnimal.TabStop = false;
            // 
            // buttonBuildMine
            // 
            this.buttonBuildMine.Location = new System.Drawing.Point(229, 229);
            this.buttonBuildMine.Name = "buttonBuildMine";
            this.buttonBuildMine.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildMine.TabIndex = 16;
            this.buttonBuildMine.Text = "Buduj";
            this.buttonBuildMine.UseVisualStyleBackColor = true;
            this.buttonBuildMine.Click += new System.EventHandler(this.buttonBuildMine_Click);
            // 
            // labelMineLevel
            // 
            this.labelMineLevel.AutoSize = true;
            this.labelMineLevel.Location = new System.Drawing.Point(244, 30);
            this.labelMineLevel.Name = "labelMineLevel";
            this.labelMineLevel.Size = new System.Drawing.Size(66, 17);
            this.labelMineLevel.TabIndex = 15;
            this.labelMineLevel.Text = "Poziom 1";
            // 
            // buttonBuildStonePit
            // 
            this.buttonBuildStonePit.Location = new System.Drawing.Point(122, 229);
            this.buttonBuildStonePit.Name = "buttonBuildStonePit";
            this.buttonBuildStonePit.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildStonePit.TabIndex = 14;
            this.buttonBuildStonePit.Text = "Buduj";
            this.buttonBuildStonePit.UseVisualStyleBackColor = true;
            this.buttonBuildStonePit.Click += new System.EventHandler(this.buttonBuildStonePit_Click);
            // 
            // buttonBuildSawmill
            // 
            this.buttonBuildSawmill.Location = new System.Drawing.Point(15, 229);
            this.buttonBuildSawmill.Name = "buttonBuildSawmill";
            this.buttonBuildSawmill.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildSawmill.TabIndex = 6;
            this.buttonBuildSawmill.Text = "Buduj";
            this.buttonBuildSawmill.UseVisualStyleBackColor = true;
            this.buttonBuildSawmill.Click += new System.EventHandler(this.buttonBuildSawmill_Click);
            // 
            // labelStonePitLevel
            // 
            this.labelStonePitLevel.AutoSize = true;
            this.labelStonePitLevel.Location = new System.Drawing.Point(137, 30);
            this.labelStonePitLevel.Name = "labelStonePitLevel";
            this.labelStonePitLevel.Size = new System.Drawing.Size(66, 17);
            this.labelStonePitLevel.TabIndex = 13;
            this.labelStonePitLevel.Text = "Poziom 1";
            // 
            // labelSawmillLevel
            // 
            this.labelSawmillLevel.AutoSize = true;
            this.labelSawmillLevel.Location = new System.Drawing.Point(21, 30);
            this.labelSawmillLevel.Name = "labelSawmillLevel";
            this.labelSawmillLevel.Size = new System.Drawing.Size(66, 17);
            this.labelSawmillLevel.TabIndex = 12;
            this.labelSawmillLevel.Text = "Poziom 1";
            // 
            // pictureBoxMine
            // 
            this.pictureBoxMine.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconMine;
            this.pictureBoxMine.Location = new System.Drawing.Point(220, 61);
            this.pictureBoxMine.Name = "pictureBoxMine";
            this.pictureBoxMine.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxMine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxMine.TabIndex = 11;
            this.pictureBoxMine.TabStop = false;
            // 
            // pictureBoxStonePit
            // 
            this.pictureBoxStonePit.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconStonePit;
            this.pictureBoxStonePit.Location = new System.Drawing.Point(113, 61);
            this.pictureBoxStonePit.Name = "pictureBoxStonePit";
            this.pictureBoxStonePit.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxStonePit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxStonePit.TabIndex = 10;
            this.pictureBoxStonePit.TabStop = false;
            // 
            // pictureBoxSawmill
            // 
            this.pictureBoxSawmill.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconSawmill;
            this.pictureBoxSawmill.Location = new System.Drawing.Point(6, 61);
            this.pictureBoxSawmill.Name = "pictureBoxSawmill";
            this.pictureBoxSawmill.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxSawmill.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSawmill.TabIndex = 9;
            this.pictureBoxSawmill.TabStop = false;
            // 
            // labelSteel
            // 
            this.labelSteel.AutoSize = true;
            this.labelSteel.Location = new System.Drawing.Point(263, 168);
            this.labelSteel.Name = "labelSteel";
            this.labelSteel.Size = new System.Drawing.Size(16, 17);
            this.labelSteel.TabIndex = 8;
            this.labelSteel.Text = "0";
            // 
            // labelSteelProduction
            // 
            this.labelSteelProduction.AutoSize = true;
            this.labelSteelProduction.Location = new System.Drawing.Point(244, 198);
            this.labelSteelProduction.Name = "labelSteelProduction";
            this.labelSteelProduction.Size = new System.Drawing.Size(43, 17);
            this.labelSteelProduction.TabIndex = 7;
            this.labelSteelProduction.Text = "10 / s";
            // 
            // labelStoneProduction
            // 
            this.labelStoneProduction.AutoSize = true;
            this.labelStoneProduction.Location = new System.Drawing.Point(137, 198);
            this.labelStoneProduction.Name = "labelStoneProduction";
            this.labelStoneProduction.Size = new System.Drawing.Size(43, 17);
            this.labelStoneProduction.TabIndex = 6;
            this.labelStoneProduction.Text = "10 / s";
            // 
            // labelStone
            // 
            this.labelStone.AutoSize = true;
            this.labelStone.Location = new System.Drawing.Point(155, 168);
            this.labelStone.Name = "labelStone";
            this.labelStone.Size = new System.Drawing.Size(16, 17);
            this.labelStone.TabIndex = 5;
            this.labelStone.Text = "0";
            // 
            // labelWoodProduction
            // 
            this.labelWoodProduction.AutoSize = true;
            this.labelWoodProduction.Location = new System.Drawing.Point(31, 198);
            this.labelWoodProduction.Name = "labelWoodProduction";
            this.labelWoodProduction.Size = new System.Drawing.Size(43, 17);
            this.labelWoodProduction.TabIndex = 4;
            this.labelWoodProduction.Text = "10 / s";
            // 
            // labelWood
            // 
            this.labelWood.AutoSize = true;
            this.labelWood.Location = new System.Drawing.Point(48, 168);
            this.labelWood.Name = "labelWood";
            this.labelWood.Size = new System.Drawing.Size(16, 17);
            this.labelWood.TabIndex = 3;
            this.labelWood.Text = "0";
            // 
            // labelMine
            // 
            this.labelMine.AutoSize = true;
            this.labelMine.Location = new System.Drawing.Point(234, 13);
            this.labelMine.Name = "labelMine";
            this.labelMine.Size = new System.Drawing.Size(76, 17);
            this.labelMine.TabIndex = 2;
            this.labelMine.Text = "KOPALNIA";
            // 
            // labelStonePit
            // 
            this.labelStonePit.AutoSize = true;
            this.labelStonePit.Location = new System.Drawing.Point(111, 13);
            this.labelStonePit.Name = "labelStonePit";
            this.labelStonePit.Size = new System.Drawing.Size(103, 17);
            this.labelStonePit.TabIndex = 1;
            this.labelStonePit.Text = "KAMIENIOŁOM";
            // 
            // labelSawmill
            // 
            this.labelSawmill.AutoSize = true;
            this.labelSawmill.Location = new System.Drawing.Point(21, 13);
            this.labelSawmill.Name = "labelSawmill";
            this.labelSawmill.Size = new System.Drawing.Size(63, 17);
            this.labelSawmill.TabIndex = 0;
            this.labelSawmill.Text = "TARTAK";
            // 
            // tabDetails
            // 
            this.tabDetails.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabDetails.Controls.Add(this.labelDetailsGoal);
            this.tabDetails.Location = new System.Drawing.Point(4, 28);
            this.tabDetails.Name = "tabDetails";
            this.tabDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetails.Size = new System.Drawing.Size(748, 600);
            this.tabDetails.TabIndex = 1;
            this.tabDetails.Text = "Szczegóły";
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.RosyBrown;
            this.buttonClose.Location = new System.Drawing.Point(866, 575);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(116, 65);
            this.buttonClose.TabIndex = 3;
            this.buttonClose.Text = "Zamknij";
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelTime
            // 
            this.labelTime.Location = new System.Drawing.Point(788, 136);
            this.labelTime.Name = "labelTime";
            this.labelTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelTime.Size = new System.Drawing.Size(194, 17);
            this.labelTime.TabIndex = 4;
            this.labelTime.Text = "Wciśnij start, aby rozpocząć";
            this.labelTime.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelGoal
            // 
            this.labelGoal.Location = new System.Drawing.Point(766, 180);
            this.labelGoal.Name = "labelGoal";
            this.labelGoal.Size = new System.Drawing.Size(216, 23);
            this.labelGoal.TabIndex = 5;
            this.labelGoal.Text = "Cel: 100 zwierząt";
            this.labelGoal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // timerCounter
            // 
            this.timerCounter.Interval = 1000;
            this.timerCounter.Tick += new System.EventHandler(this.timerCounter_Tick);
            // 
            // labelChangeGoal
            // 
            this.labelChangeGoal.AutoSize = true;
            this.labelChangeGoal.Location = new System.Drawing.Point(814, 319);
            this.labelChangeGoal.Name = "labelChangeGoal";
            this.labelChangeGoal.Size = new System.Drawing.Size(168, 17);
            this.labelChangeGoal.TabIndex = 6;
            this.labelChangeGoal.Text = "Docelowa liczba zwierząt:";
            // 
            // textBoxGoal
            // 
            this.textBoxGoal.Location = new System.Drawing.Point(866, 346);
            this.textBoxGoal.Name = "textBoxGoal";
            this.textBoxGoal.Size = new System.Drawing.Size(116, 22);
            this.textBoxGoal.TabIndex = 7;
            this.textBoxGoal.Text = "100";
            this.textBoxGoal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonChangeGoal
            // 
            this.buttonChangeGoal.Location = new System.Drawing.Point(866, 379);
            this.buttonChangeGoal.Name = "buttonChangeGoal";
            this.buttonChangeGoal.Size = new System.Drawing.Size(116, 61);
            this.buttonChangeGoal.TabIndex = 8;
            this.buttonChangeGoal.Text = "Zmień cel";
            this.buttonChangeGoal.UseVisualStyleBackColor = true;
            this.buttonChangeGoal.Click += new System.EventHandler(this.buttonChangeGoal_Click);
            // 
            // labelDetailsGoal
            // 
            this.labelDetailsGoal.AutoSize = true;
            this.labelDetailsGoal.Location = new System.Drawing.Point(31, 32);
            this.labelDetailsGoal.Name = "labelDetailsGoal";
            this.labelDetailsGoal.Size = new System.Drawing.Size(660, 527);
            this.labelDetailsGoal.TabIndex = 0;
            this.labelDetailsGoal.Text = resources.GetString("labelDetailsGoal.Text");
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 656);
            this.Controls.Add(this.buttonChangeGoal);
            this.Controls.Add(this.textBoxGoal);
            this.Controls.Add(this.labelChangeGoal);
            this.Controls.Add(this.labelGoal);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.buttonStart);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.tabControl.ResumeLayout(false);
            this.tabMainView.ResumeLayout(false);
            this.tabMainView.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHospital)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVirus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxThief)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAnimal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStonePit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSawmill)).EndInit();
            this.tabDetails.ResumeLayout(false);
            this.tabDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabMainView;
        private System.Windows.Forms.TabPage tabDetails;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelGoal;
        private System.Windows.Forms.Timer timerCounter;
        private System.Windows.Forms.Label labelWoodProduction;
        private System.Windows.Forms.Label labelWood;
        private System.Windows.Forms.Label labelMine;
        private System.Windows.Forms.Label labelStonePit;
        private System.Windows.Forms.Label labelSawmill;
        private System.Windows.Forms.Label labelSteel;
        private System.Windows.Forms.Label labelSteelProduction;
        private System.Windows.Forms.Label labelStoneProduction;
        private System.Windows.Forms.Label labelStone;
        private System.Windows.Forms.PictureBox pictureBoxSawmill;
        private System.Windows.Forms.PictureBox pictureBoxStonePit;
        private System.Windows.Forms.PictureBox pictureBoxMine;
        private System.Windows.Forms.PictureBox pictureBoxAnimal;
        private System.Windows.Forms.Button buttonBuildMine;
        private System.Windows.Forms.Label labelMineLevel;
        private System.Windows.Forms.Button buttonBuildStonePit;
        private System.Windows.Forms.Button buttonBuildSawmill;
        private System.Windows.Forms.Label labelStonePitLevel;
        private System.Windows.Forms.Label labelSawmillLevel;
        private System.Windows.Forms.PictureBox pictureBoxThief;
        private System.Windows.Forms.PictureBox pictureBoxVirus;
        private System.Windows.Forms.PictureBox pictureBoxFood;
        private System.Windows.Forms.Label labelFarmLevel;
        private System.Windows.Forms.Label labelFarm;
        private System.Windows.Forms.Label labelThiefChance;
        private System.Windows.Forms.Label labelVirusChance;
        private System.Windows.Forms.Button buttonBuildFood;
        private System.Windows.Forms.Label labelFoodProduction;
        private System.Windows.Forms.Label labelFood;
        private System.Windows.Forms.Label labelFoodLevel;
        private System.Windows.Forms.Label labelFoodField;
        private System.Windows.Forms.PictureBox pictureBoxHospital;
        private System.Windows.Forms.PictureBox pictureBoxWall;
        private System.Windows.Forms.Button buttonBuildWall;
        private System.Windows.Forms.Button buttonBuildHospital;
        private System.Windows.Forms.Label labelThief;
        private System.Windows.Forms.Label labelVirus;
        private System.Windows.Forms.Label labelThiefProtection;
        private System.Windows.Forms.Label labelVirusProtection;
        private System.Windows.Forms.Label labelWall;
        private System.Windows.Forms.Label labelHospital;
        private System.Windows.Forms.Label labelWallLevel;
        private System.Windows.Forms.Label labelHospitalLevel;
        private System.Windows.Forms.Label labelMineCost;
        private System.Windows.Forms.Label labelStonePitCost;
        private System.Windows.Forms.Label labelSawmillCost;
        private System.Windows.Forms.Label labelCost;
        private System.Windows.Forms.Label labelFoodCost;
        private System.Windows.Forms.Label labelFarmCost;
        private System.Windows.Forms.Button buttonBuildFarm;
        private System.Windows.Forms.Button buttonBuyAnimal;
        private System.Windows.Forms.Label labelWallCost;
        private System.Windows.Forms.Label labelHospitalCost;
        private System.Windows.Forms.Label labelDangers;
        private System.Windows.Forms.Label labelAnimalsCapacity;
        private System.Windows.Forms.Label labelAnimals;
        private System.Windows.Forms.Label labelAdditionalCost;
        private System.Windows.Forms.Label labelAdditionalCostAnimal;
        private System.Windows.Forms.Label labelAnimalCost;
        private System.Windows.Forms.Label labelChangeGoal;
        private System.Windows.Forms.TextBox textBoxGoal;
        private System.Windows.Forms.Button buttonChangeGoal;
        private System.Windows.Forms.Label labelDetailsGoal;
    }
}

