﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnaGudelajtisZadanieDomoweLab1
{
    /// <summary>
    /// Publiczna klasa farmer, dziedziczy po psie, jednostka defensywna
    /// </summary>
    public class Farmer : Dog
    {
        //konstruktor
        public Farmer()
        {
            this.Name = "Pastuch";
            this.Strength = 5;
        }
    }
}
