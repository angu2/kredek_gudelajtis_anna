﻿namespace AnnaGudelajtisZadanieDomoweLab1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.buttonStart = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabMainView = new System.Windows.Forms.TabPage();
            this.labelBowCost = new System.Windows.Forms.Label();
            this.labelRifleCost = new System.Windows.Forms.Label();
            this.labelHunterAttactCost = new System.Windows.Forms.Label();
            this.labelKnifeCost = new System.Windows.Forms.Label();
            this.labelOfense = new System.Windows.Forms.Label();
            this.buttonHunterAttack = new System.Windows.Forms.Button();
            this.buttonAddBow = new System.Windows.Forms.Button();
            this.buttonAddRifle = new System.Windows.Forms.Button();
            this.buttonAddKnife = new System.Windows.Forms.Button();
            this.pictureBoxRifle = new System.Windows.Forms.PictureBox();
            this.pictureBoxBow = new System.Windows.Forms.PictureBox();
            this.pictureBoxKnife = new System.Windows.Forms.PictureBox();
            this.pictureBoxHunter = new System.Windows.Forms.PictureBox();
            this.buttonAddFarmer = new System.Windows.Forms.Button();
            this.buttonAddSmallDog = new System.Windows.Forms.Button();
            this.buttonAddBigDog = new System.Windows.Forms.Button();
            this.labelBigDogCost = new System.Windows.Forms.Label();
            this.labelFarmerCost = new System.Windows.Forms.Label();
            this.labelSmallDogCost = new System.Windows.Forms.Label();
            this.labelProtection = new System.Windows.Forms.Label();
            this.pictureBoxFarmer = new System.Windows.Forms.PictureBox();
            this.pictureBoxBigDog = new System.Windows.Forms.PictureBox();
            this.pictureBoxSmallDog = new System.Windows.Forms.PictureBox();
            this.labelAnimalCost = new System.Windows.Forms.Label();
            this.labelAdditionalCostAnimal = new System.Windows.Forms.Label();
            this.buttonBuyAnimal = new System.Windows.Forms.Button();
            this.labelWallCost = new System.Windows.Forms.Label();
            this.labelHospitalCost = new System.Windows.Forms.Label();
            this.labelAnimalsCapacity = new System.Windows.Forms.Label();
            this.labelAnimals = new System.Windows.Forms.Label();
            this.labelAdditionalCost = new System.Windows.Forms.Label();
            this.labelFarmCost = new System.Windows.Forms.Label();
            this.buttonBuildFarm = new System.Windows.Forms.Button();
            this.labelFoodCost = new System.Windows.Forms.Label();
            this.labelMineCost = new System.Windows.Forms.Label();
            this.labelStonePitCost = new System.Windows.Forms.Label();
            this.labelSawmillCost = new System.Windows.Forms.Label();
            this.labelCost = new System.Windows.Forms.Label();
            this.buttonBuildWall = new System.Windows.Forms.Button();
            this.buttonBuildHospital = new System.Windows.Forms.Button();
            this.labelWall = new System.Windows.Forms.Label();
            this.labelHospital = new System.Windows.Forms.Label();
            this.labelWallLevel = new System.Windows.Forms.Label();
            this.labelHospitalLevel = new System.Windows.Forms.Label();
            this.pictureBoxWall = new System.Windows.Forms.PictureBox();
            this.pictureBoxHospital = new System.Windows.Forms.PictureBox();
            this.buttonBuildFood = new System.Windows.Forms.Button();
            this.labelFoodProduction = new System.Windows.Forms.Label();
            this.labelFood = new System.Windows.Forms.Label();
            this.labelFoodLevel = new System.Windows.Forms.Label();
            this.labelFoodField = new System.Windows.Forms.Label();
            this.pictureBoxFood = new System.Windows.Forms.PictureBox();
            this.labelFarmLevel = new System.Windows.Forms.Label();
            this.labelFarm = new System.Windows.Forms.Label();
            this.pictureBoxAnimal = new System.Windows.Forms.PictureBox();
            this.buttonBuildMine = new System.Windows.Forms.Button();
            this.labelMineLevel = new System.Windows.Forms.Label();
            this.buttonBuildStonePit = new System.Windows.Forms.Button();
            this.buttonBuildSawmill = new System.Windows.Forms.Button();
            this.labelStonePitLevel = new System.Windows.Forms.Label();
            this.labelSawmillLevel = new System.Windows.Forms.Label();
            this.pictureBoxMine = new System.Windows.Forms.PictureBox();
            this.pictureBoxStonePit = new System.Windows.Forms.PictureBox();
            this.pictureBoxSawmill = new System.Windows.Forms.PictureBox();
            this.labelSteel = new System.Windows.Forms.Label();
            this.labelSteelProduction = new System.Windows.Forms.Label();
            this.labelStoneProduction = new System.Windows.Forms.Label();
            this.labelStone = new System.Windows.Forms.Label();
            this.labelWoodProduction = new System.Windows.Forms.Label();
            this.labelWood = new System.Windows.Forms.Label();
            this.labelMine = new System.Windows.Forms.Label();
            this.labelStonePit = new System.Windows.Forms.Label();
            this.labelSawmill = new System.Windows.Forms.Label();
            this.tabDetailsResources = new System.Windows.Forms.TabPage();
            this.labelDetailsResources = new System.Windows.Forms.Label();
            this.tabDescriptionFight = new System.Windows.Forms.TabPage();
            this.labelDangers = new System.Windows.Forms.Label();
            this.labelThief = new System.Windows.Forms.Label();
            this.labelVirus = new System.Windows.Forms.Label();
            this.labelThiefProtection = new System.Windows.Forms.Label();
            this.labelVirusProtection = new System.Windows.Forms.Label();
            this.labelThiefChance = new System.Windows.Forms.Label();
            this.labelVirusChance = new System.Windows.Forms.Label();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelGoal = new System.Windows.Forms.Label();
            this.timerCounter = new System.Windows.Forms.Timer(this.components);
            this.labelChangeGoal = new System.Windows.Forms.Label();
            this.textBoxGoal = new System.Windows.Forms.TextBox();
            this.buttonChangeGoal = new System.Windows.Forms.Button();
            this.progressBarAnimals = new System.Windows.Forms.ProgressBar();
            this.timerWolves = new System.Windows.Forms.Timer(this.components);
            this.labelWolvesGroup = new System.Windows.Forms.Label();
            this.progressBarWolves = new System.Windows.Forms.ProgressBar();
            this.pictureBoxWolf3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxWolf2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxWolf1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxVirus = new System.Windows.Forms.PictureBox();
            this.pictureBoxThief = new System.Windows.Forms.PictureBox();
            this.labelDescriptionFight = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tabMainView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRifle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKnife)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHunter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFarmer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBigDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSmallDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHospital)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAnimal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStonePit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSawmill)).BeginInit();
            this.tabDetailsResources.SuspendLayout();
            this.tabDescriptionFight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWolf3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWolf2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWolf1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVirus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxThief)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.BackColor = System.Drawing.Color.PaleGreen;
            this.buttonStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonStart.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonStart.Location = new System.Drawing.Point(1227, 46);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(116, 65);
            this.buttonStart.TabIndex = 1;
            this.buttonStart.Text = "START";
            this.buttonStart.UseVisualStyleBackColor = false;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl.Controls.Add(this.tabMainView);
            this.tabControl.Controls.Add(this.tabDetailsResources);
            this.tabControl.Controls.Add(this.tabDescriptionFight);
            this.tabControl.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabControl.ItemSize = new System.Drawing.Size(130, 30);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1073, 641);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 2;
            // 
            // tabMainView
            // 
            this.tabMainView.BackColor = System.Drawing.Color.LightGreen;
            this.tabMainView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabMainView.Controls.Add(this.labelBowCost);
            this.tabMainView.Controls.Add(this.labelRifleCost);
            this.tabMainView.Controls.Add(this.labelHunterAttactCost);
            this.tabMainView.Controls.Add(this.labelKnifeCost);
            this.tabMainView.Controls.Add(this.labelOfense);
            this.tabMainView.Controls.Add(this.buttonHunterAttack);
            this.tabMainView.Controls.Add(this.buttonAddBow);
            this.tabMainView.Controls.Add(this.buttonAddRifle);
            this.tabMainView.Controls.Add(this.buttonAddKnife);
            this.tabMainView.Controls.Add(this.pictureBoxRifle);
            this.tabMainView.Controls.Add(this.pictureBoxBow);
            this.tabMainView.Controls.Add(this.pictureBoxKnife);
            this.tabMainView.Controls.Add(this.pictureBoxHunter);
            this.tabMainView.Controls.Add(this.buttonAddFarmer);
            this.tabMainView.Controls.Add(this.buttonAddSmallDog);
            this.tabMainView.Controls.Add(this.buttonAddBigDog);
            this.tabMainView.Controls.Add(this.labelBigDogCost);
            this.tabMainView.Controls.Add(this.labelFarmerCost);
            this.tabMainView.Controls.Add(this.labelSmallDogCost);
            this.tabMainView.Controls.Add(this.labelProtection);
            this.tabMainView.Controls.Add(this.pictureBoxFarmer);
            this.tabMainView.Controls.Add(this.pictureBoxBigDog);
            this.tabMainView.Controls.Add(this.pictureBoxSmallDog);
            this.tabMainView.Controls.Add(this.labelAnimalCost);
            this.tabMainView.Controls.Add(this.labelAdditionalCostAnimal);
            this.tabMainView.Controls.Add(this.buttonBuyAnimal);
            this.tabMainView.Controls.Add(this.labelWallCost);
            this.tabMainView.Controls.Add(this.labelHospitalCost);
            this.tabMainView.Controls.Add(this.labelAnimalsCapacity);
            this.tabMainView.Controls.Add(this.labelAnimals);
            this.tabMainView.Controls.Add(this.labelAdditionalCost);
            this.tabMainView.Controls.Add(this.labelFarmCost);
            this.tabMainView.Controls.Add(this.buttonBuildFarm);
            this.tabMainView.Controls.Add(this.labelFoodCost);
            this.tabMainView.Controls.Add(this.labelMineCost);
            this.tabMainView.Controls.Add(this.labelStonePitCost);
            this.tabMainView.Controls.Add(this.labelSawmillCost);
            this.tabMainView.Controls.Add(this.labelCost);
            this.tabMainView.Controls.Add(this.buttonBuildWall);
            this.tabMainView.Controls.Add(this.buttonBuildHospital);
            this.tabMainView.Controls.Add(this.labelWall);
            this.tabMainView.Controls.Add(this.labelHospital);
            this.tabMainView.Controls.Add(this.labelWallLevel);
            this.tabMainView.Controls.Add(this.labelHospitalLevel);
            this.tabMainView.Controls.Add(this.pictureBoxWall);
            this.tabMainView.Controls.Add(this.pictureBoxHospital);
            this.tabMainView.Controls.Add(this.buttonBuildFood);
            this.tabMainView.Controls.Add(this.labelFoodProduction);
            this.tabMainView.Controls.Add(this.labelFood);
            this.tabMainView.Controls.Add(this.labelFoodLevel);
            this.tabMainView.Controls.Add(this.labelFoodField);
            this.tabMainView.Controls.Add(this.pictureBoxFood);
            this.tabMainView.Controls.Add(this.labelFarmLevel);
            this.tabMainView.Controls.Add(this.labelFarm);
            this.tabMainView.Controls.Add(this.pictureBoxAnimal);
            this.tabMainView.Controls.Add(this.buttonBuildMine);
            this.tabMainView.Controls.Add(this.labelMineLevel);
            this.tabMainView.Controls.Add(this.buttonBuildStonePit);
            this.tabMainView.Controls.Add(this.buttonBuildSawmill);
            this.tabMainView.Controls.Add(this.labelStonePitLevel);
            this.tabMainView.Controls.Add(this.labelSawmillLevel);
            this.tabMainView.Controls.Add(this.pictureBoxMine);
            this.tabMainView.Controls.Add(this.pictureBoxStonePit);
            this.tabMainView.Controls.Add(this.pictureBoxSawmill);
            this.tabMainView.Controls.Add(this.labelSteel);
            this.tabMainView.Controls.Add(this.labelSteelProduction);
            this.tabMainView.Controls.Add(this.labelStoneProduction);
            this.tabMainView.Controls.Add(this.labelStone);
            this.tabMainView.Controls.Add(this.labelWoodProduction);
            this.tabMainView.Controls.Add(this.labelWood);
            this.tabMainView.Controls.Add(this.labelMine);
            this.tabMainView.Controls.Add(this.labelStonePit);
            this.tabMainView.Controls.Add(this.labelSawmill);
            this.tabMainView.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabMainView.Location = new System.Drawing.Point(4, 34);
            this.tabMainView.Name = "tabMainView";
            this.tabMainView.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainView.Size = new System.Drawing.Size(1065, 603);
            this.tabMainView.TabIndex = 0;
            this.tabMainView.Text = "Podgląd";
            // 
            // labelBowCost
            // 
            this.labelBowCost.AutoSize = true;
            this.labelBowCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelBowCost.Location = new System.Drawing.Point(689, 560);
            this.labelBowCost.Name = "labelBowCost";
            this.labelBowCost.Size = new System.Drawing.Size(44, 18);
            this.labelBowCost.TabIndex = 81;
            this.labelBowCost.Text = "1000";
            this.labelBowCost.Visible = false;
            // 
            // labelRifleCost
            // 
            this.labelRifleCost.AutoSize = true;
            this.labelRifleCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRifleCost.Location = new System.Drawing.Point(820, 560);
            this.labelRifleCost.Name = "labelRifleCost";
            this.labelRifleCost.Size = new System.Drawing.Size(44, 18);
            this.labelRifleCost.TabIndex = 80;
            this.labelRifleCost.Text = "1500";
            this.labelRifleCost.Visible = false;
            // 
            // labelHunterAttactCost
            // 
            this.labelHunterAttactCost.AutoSize = true;
            this.labelHunterAttactCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHunterAttactCost.Location = new System.Drawing.Point(970, 560);
            this.labelHunterAttactCost.Name = "labelHunterAttactCost";
            this.labelHunterAttactCost.Size = new System.Drawing.Size(44, 18);
            this.labelHunterAttactCost.TabIndex = 79;
            this.labelHunterAttactCost.Text = "1000";
            this.labelHunterAttactCost.Visible = false;
            // 
            // labelKnifeCost
            // 
            this.labelKnifeCost.AutoSize = true;
            this.labelKnifeCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelKnifeCost.Location = new System.Drawing.Point(551, 560);
            this.labelKnifeCost.Name = "labelKnifeCost";
            this.labelKnifeCost.Size = new System.Drawing.Size(35, 18);
            this.labelKnifeCost.TabIndex = 78;
            this.labelKnifeCost.Text = "300";
            this.labelKnifeCost.Visible = false;
            // 
            // labelOfense
            // 
            this.labelOfense.AutoSize = true;
            this.labelOfense.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOfense.Location = new System.Drawing.Point(331, 356);
            this.labelOfense.Name = "labelOfense";
            this.labelOfense.Size = new System.Drawing.Size(250, 36);
            this.labelOfense.TabIndex = 77;
            this.labelOfense.Text = "MYŚLIWY\r\nAtak na wilki (koszt: surowce)";
            this.labelOfense.Visible = false;
            // 
            // buttonHunterAttack
            // 
            this.buttonHunterAttack.BackColor = System.Drawing.Color.MediumTurquoise;
            this.buttonHunterAttack.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonHunterAttack.Location = new System.Drawing.Point(941, 510);
            this.buttonHunterAttack.Name = "buttonHunterAttack";
            this.buttonHunterAttack.Size = new System.Drawing.Size(100, 45);
            this.buttonHunterAttack.TabIndex = 76;
            this.buttonHunterAttack.Text = "Atakuj";
            this.buttonHunterAttack.UseVisualStyleBackColor = false;
            this.buttonHunterAttack.Visible = false;
            this.buttonHunterAttack.Click += new System.EventHandler(this.buttonHunterAttack_Click);
            // 
            // buttonAddBow
            // 
            this.buttonAddBow.BackColor = System.Drawing.Color.Aquamarine;
            this.buttonAddBow.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddBow.Location = new System.Drawing.Point(659, 512);
            this.buttonAddBow.Name = "buttonAddBow";
            this.buttonAddBow.Size = new System.Drawing.Size(100, 45);
            this.buttonAddBow.TabIndex = 75;
            this.buttonAddBow.Text = "Dodaj";
            this.buttonAddBow.UseVisualStyleBackColor = false;
            this.buttonAddBow.Visible = false;
            this.buttonAddBow.Click += new System.EventHandler(this.buttonAddBow_Click);
            // 
            // buttonAddRifle
            // 
            this.buttonAddRifle.BackColor = System.Drawing.Color.Aquamarine;
            this.buttonAddRifle.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddRifle.Location = new System.Drawing.Point(796, 512);
            this.buttonAddRifle.Name = "buttonAddRifle";
            this.buttonAddRifle.Size = new System.Drawing.Size(100, 45);
            this.buttonAddRifle.TabIndex = 74;
            this.buttonAddRifle.Text = "Dodaj";
            this.buttonAddRifle.UseVisualStyleBackColor = false;
            this.buttonAddRifle.Visible = false;
            this.buttonAddRifle.Click += new System.EventHandler(this.buttonAddRifle_Click);
            // 
            // buttonAddKnife
            // 
            this.buttonAddKnife.BackColor = System.Drawing.Color.Aquamarine;
            this.buttonAddKnife.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddKnife.Location = new System.Drawing.Point(520, 512);
            this.buttonAddKnife.Name = "buttonAddKnife";
            this.buttonAddKnife.Size = new System.Drawing.Size(100, 45);
            this.buttonAddKnife.TabIndex = 73;
            this.buttonAddKnife.Text = "Dodaj";
            this.buttonAddKnife.UseVisualStyleBackColor = false;
            this.buttonAddKnife.Visible = false;
            this.buttonAddKnife.Click += new System.EventHandler(this.buttonAddKnife_Click);
            // 
            // pictureBoxRifle
            // 
            this.pictureBoxRifle.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconRifle;
            this.pictureBoxRifle.Location = new System.Drawing.Point(796, 406);
            this.pictureBoxRifle.Name = "pictureBoxRifle";
            this.pictureBoxRifle.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxRifle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxRifle.TabIndex = 72;
            this.pictureBoxRifle.TabStop = false;
            this.pictureBoxRifle.Visible = false;
            // 
            // pictureBoxBow
            // 
            this.pictureBoxBow.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconBow;
            this.pictureBoxBow.Location = new System.Drawing.Point(659, 406);
            this.pictureBoxBow.Name = "pictureBoxBow";
            this.pictureBoxBow.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxBow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxBow.TabIndex = 71;
            this.pictureBoxBow.TabStop = false;
            this.pictureBoxBow.Visible = false;
            // 
            // pictureBoxKnife
            // 
            this.pictureBoxKnife.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconKnife;
            this.pictureBoxKnife.Location = new System.Drawing.Point(520, 406);
            this.pictureBoxKnife.Name = "pictureBoxKnife";
            this.pictureBoxKnife.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxKnife.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxKnife.TabIndex = 70;
            this.pictureBoxKnife.TabStop = false;
            this.pictureBoxKnife.Visible = false;
            // 
            // pictureBoxHunter
            // 
            this.pictureBoxHunter.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconHunter;
            this.pictureBoxHunter.Location = new System.Drawing.Point(332, 406);
            this.pictureBoxHunter.Name = "pictureBoxHunter";
            this.pictureBoxHunter.Size = new System.Drawing.Size(147, 170);
            this.pictureBoxHunter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxHunter.TabIndex = 69;
            this.pictureBoxHunter.TabStop = false;
            this.pictureBoxHunter.Visible = false;
            // 
            // buttonAddFarmer
            // 
            this.buttonAddFarmer.BackColor = System.Drawing.Color.Aquamarine;
            this.buttonAddFarmer.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddFarmer.Location = new System.Drawing.Point(945, 168);
            this.buttonAddFarmer.Name = "buttonAddFarmer";
            this.buttonAddFarmer.Size = new System.Drawing.Size(96, 45);
            this.buttonAddFarmer.TabIndex = 68;
            this.buttonAddFarmer.Text = "Dodaj";
            this.buttonAddFarmer.UseVisualStyleBackColor = false;
            this.buttonAddFarmer.Visible = false;
            this.buttonAddFarmer.Click += new System.EventHandler(this.buttonAddFarmer_Click);
            // 
            // buttonAddSmallDog
            // 
            this.buttonAddSmallDog.BackColor = System.Drawing.Color.Aquamarine;
            this.buttonAddSmallDog.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddSmallDog.Location = new System.Drawing.Point(945, 50);
            this.buttonAddSmallDog.Name = "buttonAddSmallDog";
            this.buttonAddSmallDog.Size = new System.Drawing.Size(96, 45);
            this.buttonAddSmallDog.TabIndex = 67;
            this.buttonAddSmallDog.Text = "Dodaj";
            this.buttonAddSmallDog.UseVisualStyleBackColor = false;
            this.buttonAddSmallDog.Visible = false;
            this.buttonAddSmallDog.Click += new System.EventHandler(this.buttonAddSmallDog_Click);
            // 
            // buttonAddBigDog
            // 
            this.buttonAddBigDog.BackColor = System.Drawing.Color.Aquamarine;
            this.buttonAddBigDog.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddBigDog.Location = new System.Drawing.Point(945, 286);
            this.buttonAddBigDog.Name = "buttonAddBigDog";
            this.buttonAddBigDog.Size = new System.Drawing.Size(96, 45);
            this.buttonAddBigDog.TabIndex = 66;
            this.buttonAddBigDog.Text = "Dodaj";
            this.buttonAddBigDog.UseVisualStyleBackColor = false;
            this.buttonAddBigDog.Visible = false;
            this.buttonAddBigDog.Click += new System.EventHandler(this.buttonAddBigDog_Click);
            // 
            // labelBigDogCost
            // 
            this.labelBigDogCost.AutoSize = true;
            this.labelBigDogCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelBigDogCost.Location = new System.Drawing.Point(970, 340);
            this.labelBigDogCost.Name = "labelBigDogCost";
            this.labelBigDogCost.Size = new System.Drawing.Size(44, 18);
            this.labelBigDogCost.TabIndex = 65;
            this.labelBigDogCost.Text = "1500";
            this.labelBigDogCost.Visible = false;
            // 
            // labelFarmerCost
            // 
            this.labelFarmerCost.AutoSize = true;
            this.labelFarmerCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFarmerCost.Location = new System.Drawing.Point(970, 216);
            this.labelFarmerCost.Name = "labelFarmerCost";
            this.labelFarmerCost.Size = new System.Drawing.Size(44, 18);
            this.labelFarmerCost.TabIndex = 64;
            this.labelFarmerCost.Text = "1000";
            this.labelFarmerCost.Visible = false;
            // 
            // labelSmallDogCost
            // 
            this.labelSmallDogCost.AutoSize = true;
            this.labelSmallDogCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSmallDogCost.Location = new System.Drawing.Point(979, 98);
            this.labelSmallDogCost.Name = "labelSmallDogCost";
            this.labelSmallDogCost.Size = new System.Drawing.Size(35, 18);
            this.labelSmallDogCost.TabIndex = 63;
            this.labelSmallDogCost.Text = "300";
            this.labelSmallDogCost.Visible = false;
            // 
            // labelProtection
            // 
            this.labelProtection.AutoSize = true;
            this.labelProtection.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelProtection.Location = new System.Drawing.Point(734, 12);
            this.labelProtection.Name = "labelProtection";
            this.labelProtection.Size = new System.Drawing.Size(292, 18);
            this.labelProtection.TabIndex = 62;
            this.labelProtection.Text = "OCHRONA STADA (koszt: żywność)";
            this.labelProtection.Visible = false;
            // 
            // pictureBoxFarmer
            // 
            this.pictureBoxFarmer.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconFarmer;
            this.pictureBoxFarmer.Location = new System.Drawing.Point(823, 125);
            this.pictureBoxFarmer.Name = "pictureBoxFarmer";
            this.pictureBoxFarmer.Size = new System.Drawing.Size(121, 150);
            this.pictureBoxFarmer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxFarmer.TabIndex = 61;
            this.pictureBoxFarmer.TabStop = false;
            this.pictureBoxFarmer.Visible = false;
            // 
            // pictureBoxBigDog
            // 
            this.pictureBoxBigDog.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconBigDog;
            this.pictureBoxBigDog.Location = new System.Drawing.Point(757, 229);
            this.pictureBoxBigDog.Name = "pictureBoxBigDog";
            this.pictureBoxBigDog.Size = new System.Drawing.Size(150, 129);
            this.pictureBoxBigDog.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxBigDog.TabIndex = 60;
            this.pictureBoxBigDog.TabStop = false;
            this.pictureBoxBigDog.Visible = false;
            // 
            // pictureBoxSmallDog
            // 
            this.pictureBoxSmallDog.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconSmallDog;
            this.pictureBoxSmallDog.Location = new System.Drawing.Point(807, 33);
            this.pictureBoxSmallDog.Name = "pictureBoxSmallDog";
            this.pictureBoxSmallDog.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxSmallDog.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSmallDog.TabIndex = 58;
            this.pictureBoxSmallDog.TabStop = false;
            this.pictureBoxSmallDog.Visible = false;
            // 
            // labelAnimalCost
            // 
            this.labelAnimalCost.AutoSize = true;
            this.labelAnimalCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAnimalCost.Location = new System.Drawing.Point(638, 133);
            this.labelAnimalCost.Name = "labelAnimalCost";
            this.labelAnimalCost.Size = new System.Drawing.Size(35, 18);
            this.labelAnimalCost.TabIndex = 57;
            this.labelAnimalCost.Text = "200";
            // 
            // labelAdditionalCostAnimal
            // 
            this.labelAdditionalCostAnimal.AutoSize = true;
            this.labelAdditionalCostAnimal.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAdditionalCostAnimal.Location = new System.Drawing.Point(614, 115);
            this.labelAdditionalCostAnimal.Name = "labelAdditionalCostAnimal";
            this.labelAdditionalCostAnimal.Size = new System.Drawing.Size(80, 18);
            this.labelAdditionalCostAnimal.TabIndex = 56;
            this.labelAdditionalCostAnimal.Text = "(żywność)";
            // 
            // buttonBuyAnimal
            // 
            this.buttonBuyAnimal.BackColor = System.Drawing.Color.Khaki;
            this.buttonBuyAnimal.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBuyAnimal.Location = new System.Drawing.Point(607, 61);
            this.buttonBuyAnimal.Name = "buttonBuyAnimal";
            this.buttonBuyAnimal.Size = new System.Drawing.Size(97, 51);
            this.buttonBuyAnimal.TabIndex = 55;
            this.buttonBuyAnimal.Text = "Kup zwierzę";
            this.buttonBuyAnimal.UseVisualStyleBackColor = false;
            this.buttonBuyAnimal.Click += new System.EventHandler(this.buttonBuyAnimal_Click);
            // 
            // labelWallCost
            // 
            this.labelWallCost.AutoSize = true;
            this.labelWallCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWallCost.Location = new System.Drawing.Point(141, 558);
            this.labelWallCost.Name = "labelWallCost";
            this.labelWallCost.Size = new System.Drawing.Size(35, 18);
            this.labelWallCost.TabIndex = 54;
            this.labelWallCost.Text = "500";
            // 
            // labelHospitalCost
            // 
            this.labelHospitalCost.AutoSize = true;
            this.labelHospitalCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHospitalCost.Location = new System.Drawing.Point(30, 558);
            this.labelHospitalCost.Name = "labelHospitalCost";
            this.labelHospitalCost.Size = new System.Drawing.Size(35, 18);
            this.labelHospitalCost.TabIndex = 53;
            this.labelHospitalCost.Text = "500";
            // 
            // labelAnimalsCapacity
            // 
            this.labelAnimalsCapacity.AutoSize = true;
            this.labelAnimalsCapacity.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAnimalsCapacity.Location = new System.Drawing.Point(524, 198);
            this.labelAnimalsCapacity.Name = "labelAnimalsCapacity";
            this.labelAnimalsCapacity.Size = new System.Drawing.Size(57, 19);
            this.labelAnimalsCapacity.TabIndex = 51;
            this.labelAnimalsCapacity.Text = "max 2";
            // 
            // labelAnimals
            // 
            this.labelAnimals.AutoSize = true;
            this.labelAnimals.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAnimals.Location = new System.Drawing.Point(540, 168);
            this.labelAnimals.Name = "labelAnimals";
            this.labelAnimals.Size = new System.Drawing.Size(19, 19);
            this.labelAnimals.TabIndex = 50;
            this.labelAnimals.Text = "0";
            // 
            // labelAdditionalCost
            // 
            this.labelAdditionalCost.AutoSize = true;
            this.labelAdditionalCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAdditionalCost.Location = new System.Drawing.Point(481, 277);
            this.labelAdditionalCost.Name = "labelAdditionalCost";
            this.labelAdditionalCost.Size = new System.Drawing.Size(139, 18);
            this.labelAdditionalCost.TabIndex = 49;
            this.labelAdditionalCost.Text = "(również żywność)";
            // 
            // labelFarmCost
            // 
            this.labelFarmCost.AutoSize = true;
            this.labelFarmCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFarmCost.Location = new System.Drawing.Point(522, 295);
            this.labelFarmCost.Name = "labelFarmCost";
            this.labelFarmCost.Size = new System.Drawing.Size(35, 18);
            this.labelFarmCost.TabIndex = 48;
            this.labelFarmCost.Text = "150";
            // 
            // buttonBuildFarm
            // 
            this.buttonBuildFarm.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBuildFarm.Location = new System.Drawing.Point(505, 229);
            this.buttonBuildFarm.Name = "buttonBuildFarm";
            this.buttonBuildFarm.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildFarm.TabIndex = 47;
            this.buttonBuildFarm.Text = "Buduj";
            this.buttonBuildFarm.UseVisualStyleBackColor = true;
            this.buttonBuildFarm.Click += new System.EventHandler(this.buttonBuildFarm_Click);
            // 
            // labelFoodCost
            // 
            this.labelFoodCost.AutoSize = true;
            this.labelFoodCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFoodCost.Location = new System.Drawing.Point(405, 295);
            this.labelFoodCost.Name = "labelFoodCost";
            this.labelFoodCost.Size = new System.Drawing.Size(35, 18);
            this.labelFoodCost.TabIndex = 46;
            this.labelFoodCost.Text = "150";
            // 
            // labelMineCost
            // 
            this.labelMineCost.AutoSize = true;
            this.labelMineCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMineCost.Location = new System.Drawing.Point(245, 295);
            this.labelMineCost.Name = "labelMineCost";
            this.labelMineCost.Size = new System.Drawing.Size(35, 18);
            this.labelMineCost.TabIndex = 45;
            this.labelMineCost.Text = "150";
            // 
            // labelStonePitCost
            // 
            this.labelStonePitCost.AutoSize = true;
            this.labelStonePitCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStonePitCost.Location = new System.Drawing.Point(141, 295);
            this.labelStonePitCost.Name = "labelStonePitCost";
            this.labelStonePitCost.Size = new System.Drawing.Size(35, 18);
            this.labelStonePitCost.TabIndex = 44;
            this.labelStonePitCost.Text = "150";
            // 
            // labelSawmillCost
            // 
            this.labelSawmillCost.AutoSize = true;
            this.labelSawmillCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSawmillCost.Location = new System.Drawing.Point(35, 295);
            this.labelSawmillCost.Name = "labelSawmillCost";
            this.labelSawmillCost.Size = new System.Drawing.Size(35, 18);
            this.labelSawmillCost.TabIndex = 43;
            this.labelSawmillCost.Text = "150";
            // 
            // labelCost
            // 
            this.labelCost.AutoSize = true;
            this.labelCost.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCost.Location = new System.Drawing.Point(48, 277);
            this.labelCost.Name = "labelCost";
            this.labelCost.Size = new System.Drawing.Size(219, 18);
            this.labelCost.TabIndex = 42;
            this.labelCost.Text = "Koszt (każdego z surowców):";
            // 
            // buttonBuildWall
            // 
            this.buttonBuildWall.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBuildWall.Location = new System.Drawing.Point(122, 510);
            this.buttonBuildWall.Name = "buttonBuildWall";
            this.buttonBuildWall.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildWall.TabIndex = 41;
            this.buttonBuildWall.Text = "Zbuduj";
            this.buttonBuildWall.UseVisualStyleBackColor = true;
            this.buttonBuildWall.Click += new System.EventHandler(this.buttonBuildWall_Click);
            // 
            // buttonBuildHospital
            // 
            this.buttonBuildHospital.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBuildHospital.Location = new System.Drawing.Point(15, 510);
            this.buttonBuildHospital.Name = "buttonBuildHospital";
            this.buttonBuildHospital.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildHospital.TabIndex = 40;
            this.buttonBuildHospital.Text = "Zbuduj";
            this.buttonBuildHospital.UseVisualStyleBackColor = true;
            this.buttonBuildHospital.Click += new System.EventHandler(this.buttonBuildHospital_Click);
            // 
            // labelWall
            // 
            this.labelWall.AutoSize = true;
            this.labelWall.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWall.Location = new System.Drawing.Point(143, 353);
            this.labelWall.Name = "labelWall";
            this.labelWall.Size = new System.Drawing.Size(42, 18);
            this.labelWall.TabIndex = 35;
            this.labelWall.Text = "MUR";
            // 
            // labelHospital
            // 
            this.labelHospital.AutoSize = true;
            this.labelHospital.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHospital.Location = new System.Drawing.Point(23, 353);
            this.labelHospital.Name = "labelHospital";
            this.labelHospital.Size = new System.Drawing.Size(75, 18);
            this.labelHospital.TabIndex = 34;
            this.labelHospital.Text = "SZPITAL";
            // 
            // labelWallLevel
            // 
            this.labelWallLevel.AutoSize = true;
            this.labelWallLevel.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWallLevel.Location = new System.Drawing.Point(138, 374);
            this.labelWallLevel.Name = "labelWallLevel";
            this.labelWallLevel.Size = new System.Drawing.Size(40, 18);
            this.labelWallLevel.TabIndex = 33;
            this.labelWallLevel.Text = "Brak";
            // 
            // labelHospitalLevel
            // 
            this.labelHospitalLevel.AutoSize = true;
            this.labelHospitalLevel.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHospitalLevel.Location = new System.Drawing.Point(30, 374);
            this.labelHospitalLevel.Name = "labelHospitalLevel";
            this.labelHospitalLevel.Size = new System.Drawing.Size(40, 18);
            this.labelHospitalLevel.TabIndex = 32;
            this.labelHospitalLevel.Text = "Brak";
            // 
            // pictureBoxWall
            // 
            this.pictureBoxWall.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconWall;
            this.pictureBoxWall.Location = new System.Drawing.Point(113, 395);
            this.pictureBoxWall.Name = "pictureBoxWall";
            this.pictureBoxWall.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxWall.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWall.TabIndex = 31;
            this.pictureBoxWall.TabStop = false;
            // 
            // pictureBoxHospital
            // 
            this.pictureBoxHospital.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconHospital;
            this.pictureBoxHospital.Location = new System.Drawing.Point(6, 395);
            this.pictureBoxHospital.Name = "pictureBoxHospital";
            this.pictureBoxHospital.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxHospital.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxHospital.TabIndex = 30;
            this.pictureBoxHospital.TabStop = false;
            // 
            // buttonBuildFood
            // 
            this.buttonBuildFood.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBuildFood.Location = new System.Drawing.Point(387, 229);
            this.buttonBuildFood.Name = "buttonBuildFood";
            this.buttonBuildFood.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildFood.TabIndex = 29;
            this.buttonBuildFood.Text = "Buduj";
            this.buttonBuildFood.UseVisualStyleBackColor = true;
            this.buttonBuildFood.Click += new System.EventHandler(this.buttonBuildFood_Click);
            // 
            // labelFoodProduction
            // 
            this.labelFoodProduction.AutoSize = true;
            this.labelFoodProduction.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFoodProduction.Location = new System.Drawing.Point(407, 198);
            this.labelFoodProduction.Name = "labelFoodProduction";
            this.labelFoodProduction.Size = new System.Drawing.Size(54, 19);
            this.labelFoodProduction.TabIndex = 28;
            this.labelFoodProduction.Text = "10 / s";
            // 
            // labelFood
            // 
            this.labelFood.AutoSize = true;
            this.labelFood.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFood.Location = new System.Drawing.Point(420, 168);
            this.labelFood.Name = "labelFood";
            this.labelFood.Size = new System.Drawing.Size(19, 19);
            this.labelFood.TabIndex = 27;
            this.labelFood.Text = "0";
            // 
            // labelFoodLevel
            // 
            this.labelFoodLevel.AutoSize = true;
            this.labelFoodLevel.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFoodLevel.Location = new System.Drawing.Point(396, 29);
            this.labelFoodLevel.Name = "labelFoodLevel";
            this.labelFoodLevel.Size = new System.Drawing.Size(72, 18);
            this.labelFoodLevel.TabIndex = 26;
            this.labelFoodLevel.Text = "Poziom 1";
            // 
            // labelFoodField
            // 
            this.labelFoodField.AutoSize = true;
            this.labelFoodField.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFoodField.Location = new System.Drawing.Point(384, 13);
            this.labelFoodField.Name = "labelFoodField";
            this.labelFoodField.Size = new System.Drawing.Size(90, 18);
            this.labelFoodField.TabIndex = 25;
            this.labelFoodField.Text = "ŻYWNOŚĆ";
            // 
            // pictureBoxFood
            // 
            this.pictureBoxFood.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconFood;
            this.pictureBoxFood.Location = new System.Drawing.Point(378, 61);
            this.pictureBoxFood.Name = "pictureBoxFood";
            this.pictureBoxFood.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxFood.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxFood.TabIndex = 24;
            this.pictureBoxFood.TabStop = false;
            // 
            // labelFarmLevel
            // 
            this.labelFarmLevel.AutoSize = true;
            this.labelFarmLevel.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFarmLevel.Location = new System.Drawing.Point(502, 29);
            this.labelFarmLevel.Name = "labelFarmLevel";
            this.labelFarmLevel.Size = new System.Drawing.Size(72, 18);
            this.labelFarmLevel.TabIndex = 23;
            this.labelFarmLevel.Text = "Poziom 1";
            // 
            // labelFarm
            // 
            this.labelFarm.AutoSize = true;
            this.labelFarm.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFarm.Location = new System.Drawing.Point(502, 13);
            this.labelFarm.Name = "labelFarm";
            this.labelFarm.Size = new System.Drawing.Size(88, 18);
            this.labelFarm.TabIndex = 22;
            this.labelFarm.Text = "ZAGRODA";
            // 
            // pictureBoxAnimal
            // 
            this.pictureBoxAnimal.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconAnimal;
            this.pictureBoxAnimal.Location = new System.Drawing.Point(491, 61);
            this.pictureBoxAnimal.Name = "pictureBoxAnimal";
            this.pictureBoxAnimal.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxAnimal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxAnimal.TabIndex = 17;
            this.pictureBoxAnimal.TabStop = false;
            // 
            // buttonBuildMine
            // 
            this.buttonBuildMine.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBuildMine.Location = new System.Drawing.Point(229, 229);
            this.buttonBuildMine.Name = "buttonBuildMine";
            this.buttonBuildMine.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildMine.TabIndex = 16;
            this.buttonBuildMine.Text = "Buduj";
            this.buttonBuildMine.UseVisualStyleBackColor = true;
            this.buttonBuildMine.Click += new System.EventHandler(this.buttonBuildMine_Click);
            // 
            // labelMineLevel
            // 
            this.labelMineLevel.AutoSize = true;
            this.labelMineLevel.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMineLevel.Location = new System.Drawing.Point(237, 29);
            this.labelMineLevel.Name = "labelMineLevel";
            this.labelMineLevel.Size = new System.Drawing.Size(72, 18);
            this.labelMineLevel.TabIndex = 15;
            this.labelMineLevel.Text = "Poziom 1";
            // 
            // buttonBuildStonePit
            // 
            this.buttonBuildStonePit.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBuildStonePit.Location = new System.Drawing.Point(122, 229);
            this.buttonBuildStonePit.Name = "buttonBuildStonePit";
            this.buttonBuildStonePit.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildStonePit.TabIndex = 14;
            this.buttonBuildStonePit.Text = "Buduj";
            this.buttonBuildStonePit.UseVisualStyleBackColor = true;
            this.buttonBuildStonePit.Click += new System.EventHandler(this.buttonBuildStonePit_Click);
            // 
            // buttonBuildSawmill
            // 
            this.buttonBuildSawmill.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBuildSawmill.Location = new System.Drawing.Point(15, 229);
            this.buttonBuildSawmill.Name = "buttonBuildSawmill";
            this.buttonBuildSawmill.Size = new System.Drawing.Size(81, 46);
            this.buttonBuildSawmill.TabIndex = 6;
            this.buttonBuildSawmill.Text = "Buduj";
            this.buttonBuildSawmill.UseVisualStyleBackColor = true;
            this.buttonBuildSawmill.Click += new System.EventHandler(this.buttonBuildSawmill_Click);
            // 
            // labelStonePitLevel
            // 
            this.labelStonePitLevel.AutoSize = true;
            this.labelStonePitLevel.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStonePitLevel.Location = new System.Drawing.Point(130, 29);
            this.labelStonePitLevel.Name = "labelStonePitLevel";
            this.labelStonePitLevel.Size = new System.Drawing.Size(72, 18);
            this.labelStonePitLevel.TabIndex = 13;
            this.labelStonePitLevel.Text = "Poziom 1";
            // 
            // labelSawmillLevel
            // 
            this.labelSawmillLevel.AutoSize = true;
            this.labelSawmillLevel.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSawmillLevel.Location = new System.Drawing.Point(14, 29);
            this.labelSawmillLevel.Name = "labelSawmillLevel";
            this.labelSawmillLevel.Size = new System.Drawing.Size(72, 18);
            this.labelSawmillLevel.TabIndex = 12;
            this.labelSawmillLevel.Text = "Poziom 1";
            // 
            // pictureBoxMine
            // 
            this.pictureBoxMine.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconMine;
            this.pictureBoxMine.Location = new System.Drawing.Point(220, 61);
            this.pictureBoxMine.Name = "pictureBoxMine";
            this.pictureBoxMine.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxMine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxMine.TabIndex = 11;
            this.pictureBoxMine.TabStop = false;
            // 
            // pictureBoxStonePit
            // 
            this.pictureBoxStonePit.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconStonePit;
            this.pictureBoxStonePit.Location = new System.Drawing.Point(113, 61);
            this.pictureBoxStonePit.Name = "pictureBoxStonePit";
            this.pictureBoxStonePit.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxStonePit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxStonePit.TabIndex = 10;
            this.pictureBoxStonePit.TabStop = false;
            // 
            // pictureBoxSawmill
            // 
            this.pictureBoxSawmill.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconSawmill;
            this.pictureBoxSawmill.Location = new System.Drawing.Point(6, 61);
            this.pictureBoxSawmill.Name = "pictureBoxSawmill";
            this.pictureBoxSawmill.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxSawmill.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSawmill.TabIndex = 9;
            this.pictureBoxSawmill.TabStop = false;
            // 
            // labelSteel
            // 
            this.labelSteel.AutoSize = true;
            this.labelSteel.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSteel.Location = new System.Drawing.Point(263, 168);
            this.labelSteel.Name = "labelSteel";
            this.labelSteel.Size = new System.Drawing.Size(19, 19);
            this.labelSteel.TabIndex = 8;
            this.labelSteel.Text = "0";
            // 
            // labelSteelProduction
            // 
            this.labelSteelProduction.AutoSize = true;
            this.labelSteelProduction.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSteelProduction.Location = new System.Drawing.Point(244, 198);
            this.labelSteelProduction.Name = "labelSteelProduction";
            this.labelSteelProduction.Size = new System.Drawing.Size(54, 19);
            this.labelSteelProduction.TabIndex = 7;
            this.labelSteelProduction.Text = "10 / s";
            // 
            // labelStoneProduction
            // 
            this.labelStoneProduction.AutoSize = true;
            this.labelStoneProduction.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStoneProduction.Location = new System.Drawing.Point(137, 198);
            this.labelStoneProduction.Name = "labelStoneProduction";
            this.labelStoneProduction.Size = new System.Drawing.Size(54, 19);
            this.labelStoneProduction.TabIndex = 6;
            this.labelStoneProduction.Text = "10 / s";
            // 
            // labelStone
            // 
            this.labelStone.AutoSize = true;
            this.labelStone.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStone.Location = new System.Drawing.Point(155, 168);
            this.labelStone.Name = "labelStone";
            this.labelStone.Size = new System.Drawing.Size(19, 19);
            this.labelStone.TabIndex = 5;
            this.labelStone.Text = "0";
            // 
            // labelWoodProduction
            // 
            this.labelWoodProduction.AutoSize = true;
            this.labelWoodProduction.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWoodProduction.Location = new System.Drawing.Point(31, 198);
            this.labelWoodProduction.Name = "labelWoodProduction";
            this.labelWoodProduction.Size = new System.Drawing.Size(54, 19);
            this.labelWoodProduction.TabIndex = 4;
            this.labelWoodProduction.Text = "10 / s";
            // 
            // labelWood
            // 
            this.labelWood.AutoSize = true;
            this.labelWood.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWood.Location = new System.Drawing.Point(48, 168);
            this.labelWood.Name = "labelWood";
            this.labelWood.Size = new System.Drawing.Size(19, 19);
            this.labelWood.TabIndex = 3;
            this.labelWood.Text = "0";
            // 
            // labelMine
            // 
            this.labelMine.AutoSize = true;
            this.labelMine.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMine.Location = new System.Drawing.Point(233, 13);
            this.labelMine.Name = "labelMine";
            this.labelMine.Size = new System.Drawing.Size(88, 18);
            this.labelMine.TabIndex = 2;
            this.labelMine.Text = "KOPALNIA";
            // 
            // labelStonePit
            // 
            this.labelStonePit.AutoSize = true;
            this.labelStonePit.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStonePit.Location = new System.Drawing.Point(99, 13);
            this.labelStonePit.Name = "labelStonePit";
            this.labelStonePit.Size = new System.Drawing.Size(120, 18);
            this.labelStonePit.TabIndex = 1;
            this.labelStonePit.Text = "KAMIENIOŁOM";
            // 
            // labelSawmill
            // 
            this.labelSawmill.AutoSize = true;
            this.labelSawmill.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSawmill.Location = new System.Drawing.Point(15, 13);
            this.labelSawmill.Name = "labelSawmill";
            this.labelSawmill.Size = new System.Drawing.Size(72, 18);
            this.labelSawmill.TabIndex = 0;
            this.labelSawmill.Text = "TARTAK";
            // 
            // tabDetailsResources
            // 
            this.tabDetailsResources.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabDetailsResources.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabDetailsResources.Controls.Add(this.labelDetailsResources);
            this.tabDetailsResources.Location = new System.Drawing.Point(4, 34);
            this.tabDetailsResources.Name = "tabDetailsResources";
            this.tabDetailsResources.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetailsResources.Size = new System.Drawing.Size(1065, 603);
            this.tabDetailsResources.TabIndex = 1;
            this.tabDetailsResources.Text = "Zasoby";
            // 
            // labelDetailsResources
            // 
            this.labelDetailsResources.AutoSize = true;
            this.labelDetailsResources.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDetailsResources.Location = new System.Drawing.Point(6, 12);
            this.labelDetailsResources.Name = "labelDetailsResources";
            this.labelDetailsResources.Size = new System.Drawing.Size(733, 522);
            this.labelDetailsResources.TabIndex = 0;
            this.labelDetailsResources.Text = resources.GetString("labelDetailsResources.Text");
            // 
            // tabDescriptionFight
            // 
            this.tabDescriptionFight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabDescriptionFight.Controls.Add(this.labelDescriptionFight);
            this.tabDescriptionFight.Location = new System.Drawing.Point(4, 34);
            this.tabDescriptionFight.Name = "tabDescriptionFight";
            this.tabDescriptionFight.Padding = new System.Windows.Forms.Padding(3);
            this.tabDescriptionFight.Size = new System.Drawing.Size(1065, 603);
            this.tabDescriptionFight.TabIndex = 2;
            this.tabDescriptionFight.Text = "Walka";
            this.tabDescriptionFight.UseVisualStyleBackColor = true;
            // 
            // labelDangers
            // 
            this.labelDangers.AutoSize = true;
            this.labelDangers.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDangers.Location = new System.Drawing.Point(81, 667);
            this.labelDangers.Name = "labelDangers";
            this.labelDangers.Size = new System.Drawing.Size(114, 18);
            this.labelDangers.TabIndex = 52;
            this.labelDangers.Text = "ZAGROŻENIA";
            // 
            // labelThief
            // 
            this.labelThief.AutoSize = true;
            this.labelThief.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelThief.Location = new System.Drawing.Point(165, 685);
            this.labelThief.Name = "labelThief";
            this.labelThief.Size = new System.Drawing.Size(84, 18);
            this.labelThief.TabIndex = 39;
            this.labelThief.Text = "RABUNEK";
            // 
            // labelVirus
            // 
            this.labelVirus.AutoSize = true;
            this.labelVirus.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVirus.Location = new System.Drawing.Point(41, 685);
            this.labelVirus.Name = "labelVirus";
            this.labelVirus.Size = new System.Drawing.Size(74, 18);
            this.labelVirus.TabIndex = 38;
            this.labelVirus.Text = "ZARAZA";
            // 
            // labelThiefProtection
            // 
            this.labelThiefProtection.AutoSize = true;
            this.labelThiefProtection.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelThiefProtection.Location = new System.Drawing.Point(155, 864);
            this.labelThiefProtection.Name = "labelThiefProtection";
            this.labelThiefProtection.Size = new System.Drawing.Size(106, 18);
            this.labelThiefProtection.TabIndex = 37;
            this.labelThiefProtection.Text = "Ochrona: mur";
            // 
            // labelVirusProtection
            // 
            this.labelVirusProtection.AutoSize = true;
            this.labelVirusProtection.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVirusProtection.Location = new System.Drawing.Point(20, 864);
            this.labelVirusProtection.Name = "labelVirusProtection";
            this.labelVirusProtection.Size = new System.Drawing.Size(123, 18);
            this.labelVirusProtection.TabIndex = 36;
            this.labelVirusProtection.Text = "Ochrona: szpital";
            // 
            // labelThiefChance
            // 
            this.labelThiefChance.AutoSize = true;
            this.labelThiefChance.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelThiefChance.Location = new System.Drawing.Point(160, 832);
            this.labelThiefChance.Name = "labelThiefChance";
            this.labelThiefChance.Size = new System.Drawing.Size(89, 18);
            this.labelThiefChance.TabIndex = 21;
            this.labelThiefChance.Text = "Znika 50 %";
            // 
            // labelVirusChance
            // 
            this.labelVirusChance.AutoSize = true;
            this.labelVirusChance.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelVirusChance.Location = new System.Drawing.Point(21, 832);
            this.labelVirusChance.Name = "labelVirusChance";
            this.labelVirusChance.Size = new System.Drawing.Size(114, 18);
            this.labelVirusChance.TabIndex = 20;
            this.labelVirusChance.Text = "Uśmierca 35 %";
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.RosyBrown;
            this.buttonClose.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonClose.Location = new System.Drawing.Point(1226, 588);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(116, 65);
            this.buttonClose.TabIndex = 3;
            this.buttonClose.Text = "Zamknij";
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelTime
            // 
            this.labelTime.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTime.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelTime.Location = new System.Drawing.Point(1123, 142);
            this.labelTime.Name = "labelTime";
            this.labelTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelTime.Size = new System.Drawing.Size(220, 26);
            this.labelTime.TabIndex = 4;
            this.labelTime.Text = "Wciśnij start, aby rozpocząć";
            this.labelTime.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelGoal
            // 
            this.labelGoal.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGoal.Location = new System.Drawing.Point(1127, 186);
            this.labelGoal.Name = "labelGoal";
            this.labelGoal.Size = new System.Drawing.Size(216, 23);
            this.labelGoal.TabIndex = 5;
            this.labelGoal.Text = "Cel: 100 zwierząt";
            this.labelGoal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // timerCounter
            // 
            this.timerCounter.Interval = 1000;
            this.timerCounter.Tick += new System.EventHandler(this.timerCounter_Tick);
            // 
            // labelChangeGoal
            // 
            this.labelChangeGoal.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelChangeGoal.Location = new System.Drawing.Point(1127, 325);
            this.labelChangeGoal.Name = "labelChangeGoal";
            this.labelChangeGoal.Size = new System.Drawing.Size(216, 17);
            this.labelChangeGoal.TabIndex = 6;
            this.labelChangeGoal.Text = "Docelowa liczba zwierząt:";
            this.labelChangeGoal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxGoal
            // 
            this.textBoxGoal.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxGoal.Location = new System.Drawing.Point(1227, 352);
            this.textBoxGoal.Name = "textBoxGoal";
            this.textBoxGoal.Size = new System.Drawing.Size(116, 26);
            this.textBoxGoal.TabIndex = 7;
            this.textBoxGoal.Text = "100";
            this.textBoxGoal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonChangeGoal
            // 
            this.buttonChangeGoal.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonChangeGoal.Location = new System.Drawing.Point(1227, 385);
            this.buttonChangeGoal.Name = "buttonChangeGoal";
            this.buttonChangeGoal.Size = new System.Drawing.Size(116, 61);
            this.buttonChangeGoal.TabIndex = 8;
            this.buttonChangeGoal.Text = "Zmień cel";
            this.buttonChangeGoal.UseVisualStyleBackColor = true;
            this.buttonChangeGoal.Click += new System.EventHandler(this.buttonChangeGoal_Click);
            // 
            // progressBarAnimals
            // 
            this.progressBarAnimals.Location = new System.Drawing.Point(1127, 225);
            this.progressBarAnimals.Name = "progressBarAnimals";
            this.progressBarAnimals.Size = new System.Drawing.Size(215, 23);
            this.progressBarAnimals.Step = 1;
            this.progressBarAnimals.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarAnimals.TabIndex = 9;
            // 
            // timerWolves
            // 
            this.timerWolves.Interval = 125000;
            this.timerWolves.Tick += new System.EventHandler(this.timerWolves_Tick);
            // 
            // labelWolvesGroup
            // 
            this.labelWolvesGroup.AutoSize = true;
            this.labelWolvesGroup.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWolvesGroup.Location = new System.Drawing.Point(673, 667);
            this.labelWolvesGroup.Name = "labelWolvesGroup";
            this.labelWolvesGroup.Size = new System.Drawing.Size(140, 18);
            this.labelWolvesGroup.TabIndex = 56;
            this.labelWolvesGroup.Text = "STADO WILKÓW";
            this.labelWolvesGroup.Visible = false;
            // 
            // progressBarWolves
            // 
            this.progressBarWolves.ForeColor = System.Drawing.Color.Crimson;
            this.progressBarWolves.Location = new System.Drawing.Point(676, 688);
            this.progressBarWolves.Maximum = 10;
            this.progressBarWolves.Name = "progressBarWolves";
            this.progressBarWolves.Size = new System.Drawing.Size(142, 23);
            this.progressBarWolves.Step = 1;
            this.progressBarWolves.TabIndex = 57;
            this.progressBarWolves.Visible = false;
            // 
            // pictureBoxWolf3
            // 
            this.pictureBoxWolf3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxWolf3.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconWolf3;
            this.pictureBoxWolf3.Location = new System.Drawing.Point(583, 773);
            this.pictureBoxWolf3.Name = "pictureBoxWolf3";
            this.pictureBoxWolf3.Size = new System.Drawing.Size(250, 250);
            this.pictureBoxWolf3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWolf3.TabIndex = 55;
            this.pictureBoxWolf3.TabStop = false;
            this.pictureBoxWolf3.Visible = false;
            // 
            // pictureBoxWolf2
            // 
            this.pictureBoxWolf2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxWolf2.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconWolf2;
            this.pictureBoxWolf2.Location = new System.Drawing.Point(368, 685);
            this.pictureBoxWolf2.Name = "pictureBoxWolf2";
            this.pictureBoxWolf2.Size = new System.Drawing.Size(200, 200);
            this.pictureBoxWolf2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWolf2.TabIndex = 54;
            this.pictureBoxWolf2.TabStop = false;
            this.pictureBoxWolf2.Visible = false;
            // 
            // pictureBoxWolf1
            // 
            this.pictureBoxWolf1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxWolf1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxWolf1.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconWolf1;
            this.pictureBoxWolf1.Location = new System.Drawing.Point(851, 736);
            this.pictureBoxWolf1.Name = "pictureBoxWolf1";
            this.pictureBoxWolf1.Size = new System.Drawing.Size(230, 211);
            this.pictureBoxWolf1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWolf1.TabIndex = 53;
            this.pictureBoxWolf1.TabStop = false;
            this.pictureBoxWolf1.Visible = false;
            // 
            // pictureBoxVirus
            // 
            this.pictureBoxVirus.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconVirus;
            this.pictureBoxVirus.Location = new System.Drawing.Point(23, 714);
            this.pictureBoxVirus.Name = "pictureBoxVirus";
            this.pictureBoxVirus.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxVirus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxVirus.TabIndex = 19;
            this.pictureBoxVirus.TabStop = false;
            // 
            // pictureBoxThief
            // 
            this.pictureBoxThief.Image = global::AnnaGudelajtisZadanieDomoweLab1.Properties.Resources.iconThief;
            this.pictureBoxThief.Location = new System.Drawing.Point(158, 714);
            this.pictureBoxThief.Name = "pictureBoxThief";
            this.pictureBoxThief.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxThief.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxThief.TabIndex = 18;
            this.pictureBoxThief.TabStop = false;
            // 
            // labelDescriptionFight
            // 
            this.labelDescriptionFight.AutoSize = true;
            this.labelDescriptionFight.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDescriptionFight.Location = new System.Drawing.Point(6, 12);
            this.labelDescriptionFight.Name = "labelDescriptionFight";
            this.labelDescriptionFight.Size = new System.Drawing.Size(927, 324);
            this.labelDescriptionFight.TabIndex = 1;
            this.labelDescriptionFight.Text = resources.GetString("labelDescriptionFight.Text");
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1385, 1055);
            this.Controls.Add(this.progressBarWolves);
            this.Controls.Add(this.labelWolvesGroup);
            this.Controls.Add(this.pictureBoxWolf3);
            this.Controls.Add(this.pictureBoxWolf2);
            this.Controls.Add(this.pictureBoxWolf1);
            this.Controls.Add(this.progressBarAnimals);
            this.Controls.Add(this.buttonChangeGoal);
            this.Controls.Add(this.textBoxGoal);
            this.Controls.Add(this.labelChangeGoal);
            this.Controls.Add(this.labelGoal);
            this.Controls.Add(this.labelDangers);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.pictureBoxVirus);
            this.Controls.Add(this.pictureBoxThief);
            this.Controls.Add(this.labelVirusChance);
            this.Controls.Add(this.labelThiefChance);
            this.Controls.Add(this.labelVirusProtection);
            this.Controls.Add(this.labelThiefProtection);
            this.Controls.Add(this.labelVirus);
            this.Controls.Add(this.labelThief);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tabControl.ResumeLayout(false);
            this.tabMainView.ResumeLayout(false);
            this.tabMainView.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRifle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKnife)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHunter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFarmer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBigDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSmallDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHospital)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAnimal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStonePit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSawmill)).EndInit();
            this.tabDetailsResources.ResumeLayout(false);
            this.tabDetailsResources.PerformLayout();
            this.tabDescriptionFight.ResumeLayout(false);
            this.tabDescriptionFight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWolf3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWolf2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWolf1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVirus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxThief)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabMainView;
        private System.Windows.Forms.TabPage tabDetailsResources;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelGoal;
        private System.Windows.Forms.Timer timerCounter;
        private System.Windows.Forms.Label labelWoodProduction;
        private System.Windows.Forms.Label labelWood;
        private System.Windows.Forms.Label labelMine;
        private System.Windows.Forms.Label labelStonePit;
        private System.Windows.Forms.Label labelSawmill;
        private System.Windows.Forms.Label labelSteel;
        private System.Windows.Forms.Label labelSteelProduction;
        private System.Windows.Forms.Label labelStoneProduction;
        private System.Windows.Forms.Label labelStone;
        private System.Windows.Forms.PictureBox pictureBoxSawmill;
        private System.Windows.Forms.PictureBox pictureBoxStonePit;
        private System.Windows.Forms.PictureBox pictureBoxMine;
        private System.Windows.Forms.PictureBox pictureBoxAnimal;
        private System.Windows.Forms.Button buttonBuildMine;
        private System.Windows.Forms.Label labelMineLevel;
        private System.Windows.Forms.Button buttonBuildStonePit;
        private System.Windows.Forms.Button buttonBuildSawmill;
        private System.Windows.Forms.Label labelStonePitLevel;
        private System.Windows.Forms.Label labelSawmillLevel;
        private System.Windows.Forms.PictureBox pictureBoxThief;
        private System.Windows.Forms.PictureBox pictureBoxVirus;
        private System.Windows.Forms.PictureBox pictureBoxFood;
        private System.Windows.Forms.Label labelFarmLevel;
        private System.Windows.Forms.Label labelFarm;
        private System.Windows.Forms.Label labelThiefChance;
        private System.Windows.Forms.Label labelVirusChance;
        private System.Windows.Forms.Button buttonBuildFood;
        private System.Windows.Forms.Label labelFoodProduction;
        private System.Windows.Forms.Label labelFood;
        private System.Windows.Forms.Label labelFoodLevel;
        private System.Windows.Forms.Label labelFoodField;
        private System.Windows.Forms.PictureBox pictureBoxHospital;
        private System.Windows.Forms.PictureBox pictureBoxWall;
        private System.Windows.Forms.Button buttonBuildWall;
        private System.Windows.Forms.Button buttonBuildHospital;
        private System.Windows.Forms.Label labelThief;
        private System.Windows.Forms.Label labelVirus;
        private System.Windows.Forms.Label labelThiefProtection;
        private System.Windows.Forms.Label labelVirusProtection;
        private System.Windows.Forms.Label labelWall;
        private System.Windows.Forms.Label labelHospital;
        private System.Windows.Forms.Label labelWallLevel;
        private System.Windows.Forms.Label labelHospitalLevel;
        private System.Windows.Forms.Label labelMineCost;
        private System.Windows.Forms.Label labelStonePitCost;
        private System.Windows.Forms.Label labelSawmillCost;
        private System.Windows.Forms.Label labelCost;
        private System.Windows.Forms.Label labelFoodCost;
        private System.Windows.Forms.Label labelFarmCost;
        private System.Windows.Forms.Button buttonBuildFarm;
        private System.Windows.Forms.Button buttonBuyAnimal;
        private System.Windows.Forms.Label labelWallCost;
        private System.Windows.Forms.Label labelHospitalCost;
        private System.Windows.Forms.Label labelDangers;
        private System.Windows.Forms.Label labelAnimalsCapacity;
        private System.Windows.Forms.Label labelAnimals;
        private System.Windows.Forms.Label labelAdditionalCost;
        private System.Windows.Forms.Label labelAdditionalCostAnimal;
        private System.Windows.Forms.Label labelAnimalCost;
        private System.Windows.Forms.Label labelChangeGoal;
        private System.Windows.Forms.TextBox textBoxGoal;
        private System.Windows.Forms.Button buttonChangeGoal;
        private System.Windows.Forms.Label labelDetailsResources;
        private System.Windows.Forms.ProgressBar progressBarAnimals;
        private System.Windows.Forms.Timer timerWolves;
        private System.Windows.Forms.TabPage tabDescriptionFight;
        private System.Windows.Forms.PictureBox pictureBoxWolf1;
        private System.Windows.Forms.PictureBox pictureBoxWolf2;
        private System.Windows.Forms.PictureBox pictureBoxWolf3;
        private System.Windows.Forms.PictureBox pictureBoxSmallDog;
        private System.Windows.Forms.Label labelWolvesGroup;
        private System.Windows.Forms.ProgressBar progressBarWolves;
        private System.Windows.Forms.PictureBox pictureBoxBigDog;
        private System.Windows.Forms.PictureBox pictureBoxFarmer;
        private System.Windows.Forms.Label labelProtection;
        private System.Windows.Forms.Label labelBigDogCost;
        private System.Windows.Forms.Label labelFarmerCost;
        private System.Windows.Forms.Label labelSmallDogCost;
        private System.Windows.Forms.Button buttonAddFarmer;
        private System.Windows.Forms.Button buttonAddSmallDog;
        private System.Windows.Forms.Button buttonAddBigDog;
        private System.Windows.Forms.PictureBox pictureBoxRifle;
        private System.Windows.Forms.PictureBox pictureBoxBow;
        private System.Windows.Forms.PictureBox pictureBoxKnife;
        private System.Windows.Forms.PictureBox pictureBoxHunter;
        private System.Windows.Forms.Button buttonAddBow;
        private System.Windows.Forms.Button buttonAddRifle;
        private System.Windows.Forms.Button buttonAddKnife;
        private System.Windows.Forms.Label labelOfense;
        private System.Windows.Forms.Button buttonHunterAttack;
        private System.Windows.Forms.Label labelBowCost;
        private System.Windows.Forms.Label labelRifleCost;
        private System.Windows.Forms.Label labelHunterAttactCost;
        private System.Windows.Forms.Label labelKnifeCost;
        private System.Windows.Forms.Label labelDescriptionFight;
    }
}

