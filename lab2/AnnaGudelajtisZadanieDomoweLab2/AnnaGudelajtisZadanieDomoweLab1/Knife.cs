﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnaGudelajtisZadanieDomoweLab1
{
    /// <summary>
    /// Publiczna klasa, dziedziczy po broni, jednostka ofensywna
    /// </summary>
    public class Knife : Weapon
    {
        //konstruktor
        public Knife()
        {
            Name = "Nóż";
            Strength = 2;
            Cost = 300;
        }
    }
}
