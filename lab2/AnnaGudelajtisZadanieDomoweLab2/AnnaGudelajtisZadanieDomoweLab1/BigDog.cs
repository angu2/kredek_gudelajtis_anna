﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnaGudelajtisZadanieDomoweLab1
{
    /// <summary>
    /// Publiczna klasa, dziedziczy po psie, jednostka defensywna
    /// </summary>
    public class BigDog : Dog
    {
        //konstruktor
        public BigDog()
        {
            Name = "DużyPies";
            Strength = 7;
        }
    }
}
