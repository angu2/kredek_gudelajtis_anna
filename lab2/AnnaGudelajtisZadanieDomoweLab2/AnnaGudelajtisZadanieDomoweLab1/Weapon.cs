﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnaGudelajtisZadanieDomoweLab1
{
    /// <summary>
    /// Publiczna klasa broń, dziedziczy po Myśliwym
    /// </summary>
    public class Weapon : Hunter
    {
        //dodatkowa właściwość klasy - koszt
        public int Cost { get; set; }

        //konstruktor
        public Weapon()
        {
        }
    }
}
