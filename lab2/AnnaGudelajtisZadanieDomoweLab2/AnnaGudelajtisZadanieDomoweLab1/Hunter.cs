﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnaGudelajtisZadanieDomoweLab1
{
    /// <summary>
    /// Publiczna klasa - jednostka ofensywna podstawowa
    /// </summary>
    public class Hunter
    {
        //właściwości klasy - nazwa, siła
        public string Name { get; set; }
        public int Strength { get; set; }

        //konstruktor
        public Hunter()
        {
            Name = "Myśliwy";
            Strength = 5;
        }
    }
}
