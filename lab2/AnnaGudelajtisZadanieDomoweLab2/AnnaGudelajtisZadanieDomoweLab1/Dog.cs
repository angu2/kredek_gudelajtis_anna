﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnaGudelajtisZadanieDomoweLab1
{
    /// <summary>
    /// Publiczna Klasa Pies, dziedziczy po wilku (wałaściwość siła), jednostka defensywna
    /// </summary>
    public class Dog : Wolf
    {
        // właściwość psa 
        public string Name { get; set; }
        //konstruktor, nadaje nazwe i siłę
        public Dog()
        {
            Name = "MałyPies";
            Strength = 3;
        }
        //konstruktor o parametrach
        public Dog (string name, int strength)
        {
            this.Name = name;
            this.Strength = strength;
        }
    }
}
