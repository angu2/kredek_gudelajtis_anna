﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnaGudelajtisZadanieDomoweLab1
{
    /// <summary>
    /// Publiczna klasa, dziedziczy po broni, jednostka ofensywna
    /// </summary>
    public class Rifle : Weapon
    {
        //konstruktor
        public Rifle()
        {
            Name = "Strzelba";
            Strength = 5;
            Cost = 1500;
        }
    }
}
