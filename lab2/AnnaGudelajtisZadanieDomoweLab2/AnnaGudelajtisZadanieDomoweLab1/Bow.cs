﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnaGudelajtisZadanieDomoweLab1
{
    /// <summary>
    /// Publiczna klasa, dziedziczy po broni, jednostka ofensywna
    /// </summary>
    public class Bow : Weapon
    {
        //konstruktor
        public Bow()
        {
            Name = "Łuk";
            Strength = 3;
            Cost = 1000;
        }
    }
}
