﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnaGudelajtisZadanieDomoweLab1
{
    public partial class MainForm : Form
    {
        // właściwości lista wilkow i listy jednostek - definicje
        public List<Wolf> WolvesList { get; set; }
        public List<Dog> DogsList { get; set; }
        public List<Hunter> OffensiveUnitsList { get; set; }

        int timeCounter = 0;        //liczy czas
        int animals = 0;            //liczy zwierzeta
        int animalsCapacity = 2;    //pojemnosc na zwierzeta w zagrodzie
        int goal = 100;             //cel gry
        int wood = 0;               //posiadana ilosc drewna
        int stone = 0;              //posiadana ilosc kamienia
        int steel = 0;              //posiadana ilosc zelaza
        int food = 0;              //posiadana ilosc zywnosci
        int woodProduction = 10;    //produkcja drewna
        int stoneProduction = 10;   //produkcja kamienia
        int steelProduction = 10;   //produkcja zelaza
        int foodProduction = 10;   //produkcja zywności

        int sawmillLevel = 1;       //poziom tartaku
        int stonePitLevel = 1;       //poziom kamieniołomu
        int mineLevel = 1;       //poziom kopalni
        int foodLevel = 1;       //poziom zywnosci
        int farmLevel = 1;          //poziom zagrody
        int hospitalLevel = 0;          //poziom szpitala
        int wallLevel = 0;          //poziom muru

        int wolves = 10;        //ilość wilków

        public MainForm()
        {
            InitializeComponent();
            //tworzenie zdefiniowanych list, inicjalizacja
            WolvesList = new List<Wolf>();
            DogsList = new List<Dog>();
            OffensiveUnitsList = new List<Hunter>();
        }

        /// <summary>
        /// Funkcja dodaje zasoby według ich aktualnej produkcji, wywoływana przy każdym interwale czasu
        /// </summary>
        private void AddResources()
        {
            //Dodaje drewno wg produkcji i wyświetla jego nową wartość
            wood += woodProduction;
            labelWood.Text = wood.ToString();
            //Dodaje kamień wg produkcji i wyświetla jego nową wartość
            stone += stoneProduction;
            labelStone.Text = stone.ToString();
            //Dodaje żelazo wg produkcji i wyświetla jego nową wartość
            steel += steelProduction;
            labelSteel.Text = steel.ToString();
            //Dodaje żywność wg produkcji i wyświetla jej nową wartość
            food += foodProduction;
            labelFood.Text = food.ToString();
        }
        
        /// <summary>
        /// Funkcja rozpoczyna symulacje, aktywuje zegar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStart_Click(object sender, EventArgs e)
        {
            //Zmiana koloru przycisku i jego dezaktywacja
            buttonStart.BackColor = Color.Transparent;
            buttonStart.Enabled = false;
            //Zmiana tekstu etykiet na czas i liczbę ludności
            labelTime.Text = "Czas: " + timeCounter.ToString();
            labelGoal.Text = "Zwierzęta:  " + animals.ToString() + "  /  " + goal;
            //Ustawia ProgressBar stada
            progressBarAnimals.Maximum = goal;
            progressBarAnimals.Value = animals;
            //Ustawia ProgressBar wilków
            progressBarWolves.Value = wolves;
            //progressBarAnimals.Step = 1;
            // Uruchamia zegar
            timerCounter.Start();
        }

        /// <summary>
        /// Funkcja zamykająca okno
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            //Zamyka okno
            Close();
        }

        /// <summary>
        /// Funkcja uruchamiana po każdym interwale zegara - przybywanie surowców, katastrofy uzależnione od czasu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerCounter_Tick(object sender, EventArgs e)
        {
            //Liczy czas i wyświetla go 
            timeCounter++;
            labelTime.Text = "Czas: " + timeCounter.ToString();
            //Dodaje zasoby
            AddResources();
            //KATASTROFY
            #region ZARAZA co 100s
            if (timeCounter % 100 == 0)
            {
                //liczy strate w zaleznosci od poziomu szpitala
                int lost = animals * (35 - 15 * hospitalLevel) / 100;
                //usuwa odpowiednio zwierzeta i wyswietla nowa wartosc
                animals -= lost;
                labelAnimals.Text = animals.ToString();
                labelGoal.Text = "Zwierzęta:  " + animals.ToString() + "  /  " + goal;
                //Aktualizuje progressBar
                progressBarAnimals.Value = progressBarAnimals.Value - lost;
                //wyswietla komunikat o smierci zwierzat
                MessageBox.Show("Panuje ZARAZA !! Giną zwierzęta: " + lost);
            }
            #endregion
            #region RABUNEK co 100s z przesunięciem o 50s
            if (timeCounter % 100 == 50)
            {
                //liczy straty poszczegolnych zasobow w zaleznosci od poziomu muru
                int woodLost = wood * (50 - 20 * wallLevel) / 100;
                int stoneLost = stone * (50 - 20 * wallLevel) / 100;
                int steelLost = steel * (50 - 20 * wallLevel) / 100;
                int foodLost = food * (50 - 20 * wallLevel) / 100;
                //usuwa odpowiednio zasoby i wyswietla ich nowe wartosci
                wood -= woodLost;
                stone -= stoneLost;
                steel -= steelLost;
                food -= foodLost;
                labelWood.Text = wood.ToString();
                labelStone.Text = stone.ToString();
                labelSteel.Text = steel.ToString();
                labelFood.Text = food.ToString();
                //wyswietla komunikat o kradzieży zasobów
                MessageBox.Show("Wkradł się ZŁODZIEJ !! Zrabowano zasoby! drewno:  " + woodLost + " kamień: " + stoneLost + " żelazo: " + steelLost + " żywność: " + foodLost);
            }
            #endregion
        }

        /// <summary>
        /// Funkcja rozbudowa tartaku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildSawmill_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na robudowe
            if (wood >= 150 * sawmillLevel & stone >= 150 * sawmillLevel & steel >= 150 * sawmillLevel)
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 150 * sawmillLevel;
                stone -= 150 * sawmillLevel;
                steel -= 150 * sawmillLevel;
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                sawmillLevel++;
                labelSawmillLevel.Text = "Poziom " + sawmillLevel;
                //Zwiększa produkcje zasobu i wyswietla nową w etykietce
                woodProduction = sawmillLevel * 15;
                labelWoodProduction.Text = woodProduction.ToString() + " / s";
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelSawmillCost.Text = (150 * sawmillLevel).ToString();
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 150 * sawmillLevel + " wszystkiego");
            
        }

        /// <summary>
        /// Funkcja rozbudowa kamieniołomu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildStonePit_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na robudowe
            if (wood >= 150 * stonePitLevel & stone >= 150 * stonePitLevel & steel >= 150 * stonePitLevel)
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 150 * stonePitLevel;
                stone -= 150 * stonePitLevel;
                steel -= 150 * stonePitLevel;
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                stonePitLevel++;
                labelStonePitLevel.Text = "Poziom " + stonePitLevel;
                //Zwiększa produkcje zasobu i wyswietla nową w etykietce
                stoneProduction = stonePitLevel * 15;
                labelStoneProduction.Text = stoneProduction.ToString() + " / s";
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelStonePitCost.Text = (150 * stonePitLevel).ToString();
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 150 * stonePitLevel + " wszystkiego");
        }

        /// <summary>
        /// Funkcja rozbudowa kopalni
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildMine_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na robudowe
            if (wood >= 150 * mineLevel & stone >= 150 * mineLevel & steel >= 150 * mineLevel)
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 150 * mineLevel;
                stone -= 150 * mineLevel;
                steel -= 150 * mineLevel;
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                mineLevel++;
                labelMineLevel.Text = "Poziom " + mineLevel;
                //Zwiększa produkcje zasobu i wyswietla nową w etykietce
                steelProduction = mineLevel * 15;
                labelSteelProduction.Text = steelProduction.ToString() + " / s";
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelMineCost.Text = (150 * mineLevel).ToString();
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 150 * mineLevel + " wszystkiego");
        }

        /// <summary>
        /// Funkcja rozbudowa zywnosci
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildFood_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na robudowe
            if (wood >= 150 * foodLevel & stone >= 150 * foodLevel & steel >= 150 * foodLevel)
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 150 * foodLevel;
                stone -= 150 * foodLevel;
                steel -= 150 * foodLevel;
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                foodLevel++;
                labelFoodLevel.Text = "Poziom " + foodLevel;
                //Zwiększa produkcje zasobu i wyswietla nową w etykietce
                foodProduction = foodLevel * 10;
                labelFoodProduction.Text = foodProduction.ToString() + " / s";
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelFoodCost.Text = (150 * foodLevel).ToString();
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 150 * foodLevel + " wszystkiego");
        }

        /// <summary>
        /// Funkcja rozbudowa zagrody
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildFarm_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na robudowe
            if (wood >= 150 * farmLevel & stone >= 150 * farmLevel & steel >= 150 * farmLevel & food >= 150* farmLevel)
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 150 * farmLevel;
                stone -= 150 * farmLevel;
                steel -= 150 * farmLevel;
                food -= 150 * farmLevel;
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                farmLevel++;
                labelFarmLevel.Text = "Poziom " + farmLevel;
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelFarmCost.Text = (150 * farmLevel).ToString();
                //Zwieksza pojemnosc zagrody i wyswietla nową w etykietce
                animalsCapacity = farmLevel * 10;
                labelAnimalsCapacity.Text = "max " + animalsCapacity;
                //Rozbudowa na poziom 2 skutkuje pojawieniem się stada wilków
                if (farmLevel == 2)
                {
                    //Pojawienie się stada wilków
                    WolvesGroupAppear();
                }
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 150 * farmLevel + " wszystkich surowców i żywności");
        }

        /// <summary>
        /// Funkcja kupowania zwierzęcia
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuyAnimal_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zywnosci na kupno i miejsca w zagrodzie
            if (food >= 200 & animalsCapacity > animals)
            {
                //Zmniejsza odpowiednio ilość zywnosci
                food -= 200;
                //Zwieksza ilosci zwierzat i wyswietla nową w etykietkach
                animals++;
                labelAnimals.Text = animals.ToString();
                labelGoal.Text = "Zwierzęta:  " + animals.ToString() + "  /  " + goal;
                //Aktualizuje progressBar
                //progressBarAnimals.PerformStep();     //alternatywny sposób
                progressBarAnimals.Increment(1);
                //Sprawdza czy cel zostal osiągniety
                if (animals==goal )
                {
                    //Zatrzymuje zegar
                    timerCounter.Stop();
                    //Wyswietla wiadomosc o wygranej i czasie rozgrywki
                    MessageBox.Show("Wygrałeś!! Cel został osiągnięty w czasie: " + timeCounter + " s.");
                }
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu i potrzebach przed kupnem
                MessageBox.Show("Nie wystarczająco żywnosci lub miejsca w zagrodzie. Potrzeba min 200 żywności i miejsce.");
        }

        /// <summary>
        /// Funkcja budowa szpitala, zmniejsza skutki ZARAZY
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildHospital_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na budowe
            if (wood >= 500 * (hospitalLevel+1) & stone >= 500 * (hospitalLevel + 1) & steel >= 500 * (hospitalLevel + 1))
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 500 * (hospitalLevel + 1);
                stone -= 500 * (hospitalLevel + 1);
                steel -= 500 * (hospitalLevel + 1);
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                hospitalLevel++;
                labelHospitalLevel.Text = "Poziom " + hospitalLevel;
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelHospitalCost.Text = (500 * (hospitalLevel + 1)).ToString();
                //Zmniejsza skutki ZARAZY - wyswietla wartos w etykkietce
                labelVirusChance.Text = "Uśmierca " + (35 - 15 * hospitalLevel) + "%";
                // Zmienia guzik na 'zbuduj' na 'ulepsz'
                buttonBuildHospital.Text = "Ulepsz";
                // Uniemożliwia dalsza rozbudowe po uzyskaniu poziomu 2
                if (hospitalLevel == 2)
                {
                    //dezaktywuje przycisk
                    buttonBuildHospital.Enabled = false;
                    //usuwa widocznosc kosztu nastepnej budowy
                    labelHospitalCost.Visible = false;
                }
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 500 * (hospitalLevel + 1) + " wszystkich surowców.");
        }

        /// <summary>
        /// Funkcja budowa muru, zmniejsza skutki rabunku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuildWall_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco zasobow na budowe
            if (wood >= 500 * (wallLevel + 1) & stone >= 500 * (wallLevel + 1) & steel >= 500 * (wallLevel + 1))
            {
                //Zmniejsza odpowiednio ilość zasobow wykorzystanych na rozbudowe
                wood -= 500 * (wallLevel + 1);
                stone -= 500 * (wallLevel + 1);
                steel -= 500 * (wallLevel + 1);
                //Zwieksza poziom budynku i wyswietla nowy w etykietce
                wallLevel++;
                labelWallLevel.Text = "Poziom " + wallLevel;
                //wyswietla zwiekszony koszt kolejnej rozbudowy
                labelWallCost.Text = (500 * (wallLevel + 1)).ToString();
                //Zmniejsza skutki RABUNKU - wyswietla wartosc w etykietce
                labelThiefChance.Text = "Znika " + (50 - 20 * wallLevel) + "%";
                // Zmienia guzik na 'zbuduj' na 'ulepsz'
                buttonBuildWall.Text = "Ulepsz";
                // Uniemożliwia dalsza rozbudowe po uzyskaniu poziomu 2
                if (wallLevel == 2)
                {
                    //dezaktywuje przycisk
                    buttonBuildWall.Enabled = false;
                    //usuwa widocznosc kosztu nastepnej budowy
                    labelWallCost.Visible = false;
                }
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak surowcow
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: " + 500 * (wallLevel + 1) + " wszystkich surowców.");
        }

        /// <summary>
        /// Zmienia cel gry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChangeGoal_Click(object sender, EventArgs e)
        {
            //Sprawdza czy nowy cel jest odpowiedni
            if (Int32.Parse(textBoxGoal.Text) > animals)
            {
                //Ustawia nowy cel
                goal = Int32.Parse(textBoxGoal.Text);
                //Wyswietla nowy cel w etykietce w zależności od tego czy juz rozpoczęto gre
                if (buttonStart.Enabled == true)
                    labelGoal.Text = "Cel:  " + goal + " zwierząt";
                else
                    labelGoal.Text = "Zwierzęta:  " + animals.ToString() + "  /  " + goal;
            }
            else
                MessageBox.Show("Cel jest za niski, już został osiągnięty! Nie można zmienić.");
        }

        /// <summary>
        /// Funkcja wywołująca pojawienie się stada wilków oraz jednostek defensywnych i ofensywnych w grze
        /// </summary>
        private void WolvesGroupAppear()
        {
            //pojawienie się na ekranie wszystkich komponentów związanych z obroną, atakiem i stadem wilków
            #region wyswietlenie komponentow
            //jednostki obronne
            labelProtection.Visible = true;
            pictureBoxSmallDog.Visible = true;
            pictureBoxFarmer.Visible = true;
            pictureBoxBigDog.Visible = true;
            labelSmallDogCost.Visible = true;
            labelFarmerCost.Visible = true;
            labelBigDogCost.Visible = true;
            buttonAddSmallDog.Visible = true;
            buttonAddFarmer.Visible = true;
            buttonAddBigDog.Visible = true;
            //wilki
            labelWolvesGroup.Visible = true;
            progressBarWolves.Visible = true;
            pictureBoxWolf1.Visible = true;
            pictureBoxWolf2.Visible = true;
            pictureBoxWolf3.Visible = true;
            //jednostki atakujące
            labelOfense.Visible = true;
            pictureBoxHunter.Visible = true;
            pictureBoxKnife.Visible = true;
            pictureBoxBow.Visible = true;
            pictureBoxRifle.Visible = true;
            labelKnifeCost.Visible = true;
            labelBowCost.Visible = true;
            labelRifleCost.Visible = true;
            labelHunterAttactCost.Visible = true;
            buttonAddKnife.Visible = true;
            buttonAddBow.Visible = true;
            buttonAddRifle.Visible = true;
            buttonHunterAttack.Visible = true;
            #endregion

            //tworzy obiekt klasy Random do losowania liczb
            Random rnd = new Random();

            //tworzy w pętli 10 wilków i dodaje je do listy wilkow
            for (int i = 0; i < 10; i++)
            {
                //tworzy nowy obiekt wilk
                Wolf newWolf = new Wolf();
                //przypisuje losową siłę do właściwości wilka
                int randomStrength = rnd.Next(1, 16); // losowa liczba z zakresu <1,15>
                newWolf.Strength = randomStrength;
                //dodaje obiekt do listy wilków
                WolvesList.Add(newWolf);
            }

            //wyświetla wiadomość o pojawieniu sie wilków
            MessageBox.Show("W okolicy pojawia się stado 10 wilków! Twoje zwierzęta są w niebezpieczeństwie! Broń stado i poluj na wilki!");

            //tworzy myśliwego do ataku na wilki i dodaje go do listy jednostek ofensywnych
            Hunter newHunter = new Hunter();
            OffensiveUnitsList.Add(newHunter);
            //wyświetla wiadomość o myśliwym i dostępnej broni
            MessageBox.Show("Na farmę przybył myśliwy! Możesz teraz polować na wilki. Siłę myśliwego(5) możesz zwiększać dodając mu różną broń(2,3,5)");

            //startuje zegar wilków (do ataków na farmę)
            timerWolves.Start();
        }

        /// <summary>
        /// Zegar wilków, interwał = 125000 - 125*podstawowy, wywołuje atak wilkow na farme
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerWolves_Tick(object sender, EventArgs e)
        {
            //zmienne lokalne 
            int lost = 0;           //dla ilości zabitych zwierzat
            int wolvesPower = 0;    //dla siły ataku wilków
            int defended = 0;       //dla ilości obronionych zwierząt

            #region liczy statystyki ataku wilków
            //sumuje wartości sił wszystkich wilków
            foreach (Wolf wolf in WolvesList)
            {
                wolvesPower += wolf.Strength;
            }

            //przypisuje sile wilkow jako maksymalną strate w zwierzetach
            lost = wolvesPower;

            //sprawdza ilosc zwierzat i ustawia ja jako max strate gdy ilosc jest mniejsza
            if (animals < lost)
                lost = animals;

            //posiadane jednostki obronne zmniejszają strate
            foreach (var defence in DogsList)
            {
                lost -= defence.Strength;
                defended += defence.Strength;
            }

            //sprawdza czy obronionych nie jest wiecej niz posiadanych
            if (animals < defended)
                defended = animals;

            //sprawdza czy strata nie jest <0, wtedy ginie 0
            if (lost < 0)
                lost = 0;
            #endregion

            //giną zwierzeta - zmiejsza się ich ilosc i aktualizują info w etykietkach
            animals -= lost;
            labelAnimals.Text = animals.ToString();
            labelGoal.Text = "Zwierzęta:  " + animals.ToString() + "  /  " + goal;
            //Aktualizuje progressBar
            progressBarAnimals.Value = animals;

            //wyświetla wiadomość o ataku wilków
            MessageBox.Show("Wilki zaatakowały twoją farmę z siłą " + wolvesPower + "! Obroniono: " + defended + " zwierząt. Giną zwierzęta: " + lost);
        }

        #region dodat. śmierć psa
        /// <summary>
        /// Mały pies umiera po ataku wilkow
        /// </summary>
        /*
        private void SmallDogDies()
        {
            //zmiana guzika i umowliwienie kupna nowego psa
            buttonAddSmallDog.Text = "Dodaj";
            buttonAddSmallDog.Enabled = true;
            //usuwa psa z listy
        }
        */
        #endregion

        /// <summary>
        /// Tworzy małego psa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddSmallDog_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco żywności na zakup 
            if (food >= 300 )
            {
                //Zmniejsza odpowiednio ilość wykorzystanej żywności
                food -= 300;
                // Zmienia guzik z 'dodaj' na 'posiadasz' i uniemożliwia dodanie więcej niz jednego
                buttonAddSmallDog.Text = "Posiadasz";
                buttonAddSmallDog.Enabled = false;

                //tworzy obiekt i dodaje go do listy jednostek obronnych(DogsList)
                Dog newDog = new Dog();
                DogsList.Add(newDog);
                //Console.WriteLine(DogsList);
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak zasobów
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: 300 żywności.");
        }

        /// <summary>
        /// Tworzy Pastucha
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddFarmer_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco żywności na zakup 
            if (food >= 1000)
            {
                //Zmniejsza odpowiednio ilość wykorzystanej żywności
                food -= 1000;
                // Zmienia guzik z 'dodaj' na 'posiadasz' i uniemożliwia dodanie więcej niz jednego
                buttonAddFarmer.Text = "Posiadasz";
                buttonAddFarmer.Enabled = false;

                //tworzy obiekt i dodaje go do listy jednostek obronnych(DogsList)
                Farmer newFarmer = new Farmer();
                DogsList.Add(newFarmer);
                //Console.WriteLine(DogsList);
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak zasobów
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: 1000 żywności.");
        }

        /// <summary>
        /// Tworzy Dużego Psa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddBigDog_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco żywności na zakup 
            if (food >= 1500)
            {
                //Zmniejsza odpowiednio ilość wykorzystanej żywności
                food -= 1500;
                // Zmienia guzik z 'dodaj' na 'posiadasz' i uniemożliwia dodanie więcej niz jednego
                buttonAddBigDog.Text = "Posiadasz";
                buttonAddBigDog.Enabled = false;

                //tworzy obiekt i dodaje go do listy jednostek obronnych(DogsList)
                BigDog newDog = new BigDog();
                DogsList.Add(newDog);
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak zasobów
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: 1500 żywności.");
        }

        /// <summary>
        /// Tworzy jednostke ofensywna Nóż
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddKnife_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco surowców na zakup 
            if (wood >= 300 & stone >= 300 & steel >= 300)
            {
                //Zmniejsza odpowiednio ilość wykorzystanych surowców
                wood -= 300;
                stone -= 300;
                steel -= 300;
                // Zmienia guzik z 'dodaj' na 'posiadasz' i uniemożliwia dodanie więcej niz jednego
                buttonAddKnife.Text = "Posiadasz";
                buttonAddKnife.Enabled = false;

                //tworzy obiekt i dodaje go do listy jednostek ofensywnych(OffensiveUnitsList)
                Knife newKnife = new Knife();
                OffensiveUnitsList.Add(newKnife);
                //Console.WriteLine(OffensiveUnitsList);  //debug
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak zasobów
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: 300 każdego z surowców.");
        }

        /// <summary>
        /// Tworzy jednostkę ofensywną Łuk
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddBow_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco surowców na zakup 
            if (wood >= 1000 & stone >= 1000 & steel >= 1000)
            {
                //Zmniejsza odpowiednio ilość wykorzystanych surowców
                wood -= 1000;
                stone -= 1000;
                steel -= 1000;
                // Zmienia guzik z 'dodaj' na 'posiadasz' i uniemożliwia dodanie więcej niz jednego
                buttonAddBow.Text = "Posiadasz";
                buttonAddBow.Enabled = false;

                //tworzy obiekt i dodaje go do listy jednostek ofensywnych(OffensiveUnitsList)
                Bow newBow = new Bow();
                OffensiveUnitsList.Add(newBow);
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak zasobów
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: 1000 każdego z surowców.");
        }

        /// <summary>
        /// Tworzy jednostkę ofensywną Strzelba
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddRifle_Click(object sender, EventArgs e)
        {
            //Sprawdza czy mamy wystarczająco surowców na zakup 
            if (wood >= 1500 & stone >= 1500 & steel >= 1500)
            {
                //Zmniejsza odpowiednio ilość wykorzystanych surowców
                wood -= 1500;
                stone -= 1500;
                steel -= 1500;
                // Zmienia guzik z 'dodaj' na 'posiadasz' i uniemożliwia dodanie więcej niz jednego
                buttonAddRifle.Text = "Posiadasz";
                buttonAddRifle.Enabled = false;

                //tworzy obiekt i dodaje go do listy jednostek ofensywnych(OffensiveUnitsList)
                Rifle newRifle = new Rifle();
                OffensiveUnitsList.Add(newRifle);
            }
            else
                //Wyswietla wiadomosc o niepowodzeniu ze wzgledu na brak zasobów
                MessageBox.Show("Nie wystarczająco zasobów. Potrzeba min: 1500 każdego z surowców.");
        }

        /// <summary>
        /// Funkcja ataku na stado wilków
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonHunterAttack_Click(object sender, EventArgs e)
        {
            //zmienne lokalne
            int attackPower = 0; //siła całkowita ataku

            //sumuje siłe ataku z posiadanych jednostek ofensywnych
            foreach (var offense in OffensiveUnitsList)
            {
                attackPower += offense.Strength;
            }

            //losuje przeciwnika (jednego z wilków)
            Random rnd = new Random();              //tworzy obiekt klasy Random do losowania liczb
            int wolfIndex = rnd.Next(0, wolves);   // losowa liczba z zakresu <0, ilosc wilkow -1> - indeks wylosowanego wilka na liście wilków
            //zaatakowany wilk
            Wolf attackedWolf = new Wolf();
            attackedWolf = WolvesList[wolfIndex];

            //Pojedynek myśliwego z wilkiem (na siłę)
            if (attackPower >= attackedWolf.Strength )
            {
                //MYŚLIWY WYGRAŁ, wilk ginie
                //zmniejsza ilość wilków i aktualizuje progressBar wilkow
                wolves--;
                progressBarWolves.Value = wolves;
                //usuwa pokonanego wilka z listy wilków
                WolvesList.RemoveAt(wolfIndex);

                // wyswietla wiadomosc o wygranym pojedynku
                MessageBox.Show("POJEDYNEK Z WILKIEM WYGRANY! Wilk ginie! Siła myśliwego: " + attackPower + ", siła wilka: " + attackedWolf.Strength);

                //sprawdza czy wszystkie wilki zostały pokonane
                if (wolves == 0)
                {
                    //uniemożliwia dalszy atak na wilki - dezaktywacja guzika
                    buttonHunterAttack.Enabled = false;
                    //zatrzymuje zegar liczący czas do ataku wilków na farme
                    timerWolves.Stop();
                    //wyswietla wiadomosc o pokonaniu stada wilkow
                    MessageBox.Show("Stado wilków zostało całkowicie pokonane!");
                }
            }
            else //MYŚLIWY PRZEGRAŁ
            {
                // wyswietla wiadomosc o przegranym pojedynku
                MessageBox.Show("POJEDYNEK Z WILKIEM PRZEGRANY! Siła myśliwego: " + attackPower + ", siła wilka: " + attackedWolf.Strength);
            }
        }
    }
}
