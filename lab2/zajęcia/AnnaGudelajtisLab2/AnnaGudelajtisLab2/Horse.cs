﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnaGudelajtisLab2
{
    /// <summary>
    /// Klasa publiczna
    /// </summary>
    public class Horse
    {
        //pola klasy Horse
        // Właściwość w klasie Horse (konwencja: właściwości - wielka litera); Imię
        public string Name { get; set; }
        // Druga właściwość; Ulubiona liczba
        public int FavouriteNumber { get; set; }


    }
}
