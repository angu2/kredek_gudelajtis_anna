﻿namespace AnnaGudelajtisLab2
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelUserName = new System.Windows.Forms.Label();
            this.buttonAddHorse = new System.Windows.Forms.Button();
            this.textBoxNumber = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelNumber = new System.Windows.Forms.Label();
            this.labelHornColor = new System.Windows.Forms.Label();
            this.textBoxHornColor = new System.Windows.Forms.TextBox();
            this.buttonAddUnicorn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelUserName.Location = new System.Drawing.Point(12, 9);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(425, 63);
            this.labelUserName.TabIndex = 0;
            this.labelUserName.Text = "Anna Gudełajtis";
            // 
            // buttonAddHorse
            // 
            this.buttonAddHorse.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddHorse.Location = new System.Drawing.Point(46, 228);
            this.buttonAddHorse.Name = "buttonAddHorse";
            this.buttonAddHorse.Size = new System.Drawing.Size(210, 50);
            this.buttonAddHorse.TabIndex = 1;
            this.buttonAddHorse.Text = "Dodaj konia";
            this.buttonAddHorse.UseVisualStyleBackColor = true;
            this.buttonAddHorse.Click += new System.EventHandler(this.buttonAddHorse_Click);
            // 
            // textBoxNumber
            // 
            this.textBoxNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxNumber.Location = new System.Drawing.Point(127, 175);
            this.textBoxNumber.Name = "textBoxNumber";
            this.textBoxNumber.Size = new System.Drawing.Size(129, 34);
            this.textBoxNumber.TabIndex = 2;
            // 
            // textBoxName
            // 
            this.textBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxName.Location = new System.Drawing.Point(127, 134);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(129, 34);
            this.textBoxName.TabIndex = 3;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(41, 134);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(59, 29);
            this.labelName.TabIndex = 4;
            this.labelName.Text = "Imię";
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelNumber.Location = new System.Drawing.Point(41, 180);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(82, 29);
            this.labelNumber.TabIndex = 5;
            this.labelNumber.Text = "Liczba";
            // 
            // labelHornColor
            // 
            this.labelHornColor.AutoSize = true;
            this.labelHornColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHornColor.Location = new System.Drawing.Point(302, 175);
            this.labelHornColor.Name = "labelHornColor";
            this.labelHornColor.Size = new System.Drawing.Size(126, 29);
            this.labelHornColor.TabIndex = 7;
            this.labelHornColor.Text = "Kolor rogu";
            // 
            // textBoxHornColor
            // 
            this.textBoxHornColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxHornColor.Location = new System.Drawing.Point(439, 172);
            this.textBoxHornColor.Name = "textBoxHornColor";
            this.textBoxHornColor.Size = new System.Drawing.Size(129, 34);
            this.textBoxHornColor.TabIndex = 8;
            // 
            // buttonAddUnicorn
            // 
            this.buttonAddUnicorn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddUnicorn.Location = new System.Drawing.Point(307, 228);
            this.buttonAddUnicorn.Name = "buttonAddUnicorn";
            this.buttonAddUnicorn.Size = new System.Drawing.Size(261, 50);
            this.buttonAddUnicorn.TabIndex = 13;
            this.buttonAddUnicorn.Text = "Dodaj jednorożca";
            this.buttonAddUnicorn.UseVisualStyleBackColor = true;
            this.buttonAddUnicorn.Click += new System.EventHandler(this.buttonAddUnicorn_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonAddUnicorn);
            this.Controls.Add(this.textBoxHornColor);
            this.Controls.Add(this.labelHornColor);
            this.Controls.Add(this.labelNumber);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.textBoxNumber);
            this.Controls.Add(this.buttonAddHorse);
            this.Controls.Add(this.labelUserName);
            this.Name = "FormMain";
            this.Text = "FormMain";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.Button buttonAddHorse;
        private System.Windows.Forms.TextBox textBoxNumber;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelNumber;
        private System.Windows.Forms.Label labelHornColor;
        private System.Windows.Forms.TextBox textBoxHornColor;
        private System.Windows.Forms.Button buttonAddUnicorn;
    }
}

