﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnaGudelajtisLab2
{
    public partial class FormMain : Form
    {
        // właściwość lista koni- definicja
        public List<Horse> HorseList { get; set; }

        public FormMain()
        {
            InitializeComponent();
            //tworzenie zdefiniowanej listy, inicjalizacja
            HorseList = new List<Horse>();
        }

        /// <summary>
        /// Funkcja dodaje konia
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddHorse_Click(object sender, EventArgs e)
        {
            //tworzenie obiektu kon na podstawie danych, ktore wprowadzamy do textboxow
            //obiekt zostanie stworzony w srodku funkcji GetHorseData i zwrocony do zmiennej lokalnej newHorse
            Horse newHorse = this.GetHorseData();

            //dodanie konia do listy
            HorseList.Add(newHorse);
        }

        /// <summary>
        /// Tworzenie jednorożca
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddUnicorn_Click(object sender, EventArgs e)
        {
            //Tworzenie kon analogicznie jak przy AddHorse
            Horse horse = GetHorseData();
            //tworzenie obiektu na podstawie przekazanego parametru (kon)
            //przypisze wartości z obiektu rodzica (konia) do obiektu dziecka(jednorozca) wg konstruktora
            Unicorn unicorn = new Unicorn(horse);

            //dodanie wlasciwosci koloru rogu z textBoxa do pola obiektu
            unicorn.HornColor = textBoxHornColor.Text;

            //dodanie jednorozca do listy
            HorseList.Add(unicorn);
        }

        /// <summary>
        /// prywatna metoda, zwraca obiekt typu horse
        /// Pobieranie danych z textBoxów
        /// </summary>
        /// <returns></returns>
        private Horse GetHorseData()
        {
            //tworzy region
            #region tworzenie konia
            //tworzy nowy obiekt klasy Horse (zmienna lokalna)
            Horse newHorse = new Horse();
            //Przypisuje wartości do właściwości konia
            //newHorse.FavouriteNumber = 42;
            newHorse.Name = textBoxName.Text;
            newHorse.FavouriteNumber = Int32.Parse(textBoxNumber.Text);
            #endregion

            //zwrocenie konia
            return newHorse;
        }
    }
}


///ZADANIE DOMOWE
///program z zajęc
///zad1 (instr na maila)
///do zad dom lab 1 dodac obiekty
///jednostki wytwarzac z  surowcow - 3 defensywne (zabezpiecza przed atakami z zewnatrz),   (+dziedziczenie/uogolnienie)
///                                - 3 ofensywne (odzialuja na rzeczywistosc na zewnatrz)
/// 6 rodzajow klas
/// kontrolki progressBar
/// pomyslowosc, multimedia
/// moze byc nowy program
/// klasy StreamReader i StreamWriter  -> przycisk save game / load Data
