﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnaGudelajtisLab2
{
    public class Unicorn : Horse
    {
        // wlasciwosc - kolor rogu
        public string HornColor { get; set; }

        //konstruktor bezparametrowy
        public Unicorn()
        {
        }

        //konstruktor parametrowy ktory przyjmuje obiekt Horse i przypisuje wartosci z tego obiektu do DANEGO obiektu
        public Unicorn(Horse horse)
        {
            //przypisanie wartosci (Name i FavouriteNumber) z obiektu horse do wartosci z DANEGO obiektu
            this.Name = horse.Name;
            this.FavouriteNumber = horse.FavouriteNumber;
        }
    }
}
