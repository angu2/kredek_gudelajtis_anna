﻿using AnnaGudelajtisLab6ZadanieDomowe.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AnnaGudelajtisLab6ZadanieDomowe.Controllers
{
    public class HomeController : Controller
    {
        //połączenie z bazą danych
        AnnaGudelajtisLab6ZadanieDomoweEntities database = new AnnaGudelajtisLab6ZadanieDomoweEntities();

        /// <summary>
        /// strona domowa
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            //zwraca widok główny
            return View();
        }

        /// <summary>
        /// widok ze wszystkimi filmami
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAllFilms()
        {
            //lista do ktorej bedą zapisywane filmy z bazy danych; film-model(cała tabela)
            List<Film> allFilms = database.Film.ToList();

            // Lista obiektów do wyświetlenia; FilmVM - widok modelu(wybrane elementy modelu)
            List<FilmVM> viewFilms = new List<FilmVM>();
            foreach (var film in allFilms)
            {
                FilmVM viewFilm = new FilmVM(film.Name, Convert.ToInt32(film.YearOfProduction), film.Category, film.Director, film.Photo);
                viewFilms.Add(viewFilm);
            }
            // Przekazanie listy filmow do widoku
            return View(viewFilms);
        }

        /// <summary>
        /// widok ze wszystkimi aktorami
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAllActors()
        {
            //lista do ktorej bedą zapisywani aktorzy z bazy danych; aktor-model(cała tabela)
            List<Actor> allActors = database.Actor.ToList();

            // Lista obiektów do wyświetlenia; ActorVM - widok modelu(wybrane elementy modelu)
            List<ActorVM> viewActors = new List<ActorVM>();
            foreach (var actor in allActors)
            {
                ActorVM viewActor = new ActorVM(actor.FirstName, actor.LastName, actor.Nationality, actor.Photo, actor.Film);
                viewActors.Add(viewActor);
            }
            // Przekazanie listy aktorów do widoku
            return View(viewActors);
        }

        /// <summary>
        /// wyświetla liste gatunkow filmow
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetListOfCategories()
        {
            //pobiera filmy z bazy
            List<Film> allFilms = database.Film.ToList();

            List<string> allCategories = new List<string>();

            //pobiera gatunki filmów
            foreach (var film in allFilms)
            {
                if(!allCategories.Contains(film.Category))
                    allCategories.Add(film.Category);
            }
            // Przekazanie listy gatunkow do widoku
            return View(allCategories);
        }

        /// <summary>
        /// wyswietla filmy z danego gatunku
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetFilmByCategory(string[] category)
        {
            List<Film> allFilms = database.Film.ToList();
            //filtracja listy, zapytanie o filmy z danego gatunku
            var query = allFilms.Where(a => a.Category.ToLower() == category[0].ToLower()).ToList();            //ToLower - do malych liter dla porownania   var query = allCars.Select(n=>n).Where(a => a.Model.ToLower() == model.ToLower()).FirstOrDefault();    

            List<FilmVM> viewFilms = new List<FilmVM>();

            foreach(var film in query)
            {
                FilmVM viewFilm = new FilmVM(film.Name, Convert.ToInt32(film.YearOfProduction), film.Category, film.Category, film.Photo);
                viewFilms.Add(viewFilm);
            }
            
            // Przekazanie modelu do widoku
            return View(viewFilms);
        }

        /// <summary>
        /// widok z opisem
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        //widok z kontaktem
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}