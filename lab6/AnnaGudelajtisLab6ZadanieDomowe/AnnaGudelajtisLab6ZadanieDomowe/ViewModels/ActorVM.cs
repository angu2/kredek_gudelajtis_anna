﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnnaGudelajtisLab6ZadanieDomowe.ViewModels
{
    public class ActorVM
    {
        //własności klasy (ActorVM) widok dla modelu aktor
        //imie
        public string FirstName { get; set; }
        //nazwisko
        public string LastName { get; set; }
        //kraj
        public string Nationality { get; set; }
        //ścieżka do zdjęcia
        public string Photo { get; set; }

        //film
        public virtual Film Film { get; set; }

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="lastName"></param>
        /// <param name="nationality"></param>
        /// <param name="photo"></param>
        public ActorVM(string name, string lastName, string nationality, string photo, Film film)
        {
            FirstName = name;
            LastName = lastName;
            Nationality = nationality;
            Photo = photo;
            Film = film;
        }
    }
}