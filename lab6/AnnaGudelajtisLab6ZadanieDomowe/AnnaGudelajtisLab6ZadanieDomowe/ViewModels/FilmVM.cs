﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnnaGudelajtisLab6ZadanieDomowe.ViewModels
{
    public class FilmVM
    {
        //własności klasy(FilmVM) widok dla modelu film
        //nazwa
        public string Name { get; set; }
        //rok produkcji
        public Nullable<int> YearOfProduction { get; set; }
        //gatunek
        public string Category { get; set; }
        //rezyser
        public string Director { get; set; }
        //ściezka do zdjęcia
        public string Photo { get; set; }

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="year"></param>
        /// <param name="category"></param>
        /// <param name="director"></param>
        /// <param name="photo"></param>
        public FilmVM(string name, int year, string category, string director, string photo)
        {
            Name = name;
            YearOfProduction = year;
            Category = category;
            Director = director;
            Photo = photo;
        }
    }
}