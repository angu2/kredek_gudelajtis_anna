﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnnaGudelajtisLab6.ViewModels
{
    public class CarVM
    {
        //model samochodu
        public string Model { get; set; }
        //marka samochodu
        public string Manufacturer { get; set; }
        //cena samochodu
        public decimal Price { get; set; }
        //sciezka do zdjecia
        public string Photo { get; set; }

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="model"></param>
        /// <param name="manufacturer"></param>
        /// <param name="price"></param>
        /// <param name="photo"></param>
        public CarVM(string model, string manufacturer, int price, string photo)
        {
            Model = model;
            Manufacturer = manufacturer;
            Price = price;
            Photo = photo;
        }

    }
}