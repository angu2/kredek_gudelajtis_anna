﻿using AnnaGudelajtisLab6.Models;
using AnnaGudelajtisLab6.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AnnaGudelajtisLab6.Controllers
{
    //logika biznesowa aplikacji

    public class HomeController : Controller
    {
        //polączenie z baza danych
        CarsAppDbEntities database = new CarsAppDbEntities();

        /// <summary>
        /// zwraca widok (actionresult) - strone internetowa
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        //widok z linkami
        [HttpGet]
        public ActionResult InterestingLinks()
        {
            return View();
        }

        /// <summary>
        /// wyświetlanie wszystkich samochodów
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAllCars()
        {
            //lista do ktorej bedą zapisywane samochody z bazy danych; car-model(cała tabela)
            List<Car> allCars = database.Car.ToList();

            // Lista obiektów do wyświetlenia; carVM - widok modelu(wybrane elementy modelu)
            List<CarVM> viewCars = new List<CarVM>();
            foreach(var car in allCars)
            {
                CarVM viewCar = new CarVM(car.Model, car.Manufacturer, Convert.ToInt32(car.Price), car.Photo);
                viewCars.Add(viewCar);
            }
            // Przekazanie listy samochodów do widoku
            return View(viewCars);
        }

        /// <summary>
        /// wyswietlanie listy modeli samochodow
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetListOfModels()
        {
            List<Car> allCars = database.Car.ToList();
            List<string> allModels = new List<string>();

            foreach(var car in allCars)
            {
                allModels.Add(car.Model);
            }
            // Przekazanie listy modeli samochodów do widoku
            return View(allModels);
        }


        /// <summary>
        /// wyswietlanie samochodu po modelu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetCarByModel(string model)
        {
            List<Car> allCars = database.Car.ToList();
            //filtracja listy
            var query = allCars.Where(a => a.Model.ToLower() == model.ToLower()).FirstOrDefault();            //ToLower - do malych liter dla porownania   var query = allCars.Select(n=>n).Where(a => a.Model.ToLower() == model.ToLower()).FirstOrDefault();    
            CarVM viewCar = new CarVM(query.Model, query.Manufacturer, Convert.ToInt32(query.Price), query.Photo);

            // Przekazanie modelu do widoku
            return View(viewCar);
        }

        /// <summary>
        /// formularz kontaktowy
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ContactForm()
        {
            return View();
        }

        /// <summary>
        /// przechwycenie danych z formularza kontaktowego
        /// </summary>
        /// <param name="userData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ContactForm(ContactForm userData)
        {
            string fullName = userData.Name + " " + userData.Surname;
            ViewBag.UserName = fullName;

            return View("ContactFromGreetings");
        }
        
    }
}