﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnnaGudelajtisLab6.Models
{

    public class ContactForm
    {
        //właściwość Imie
        public string Name { get; set; }
        //nazwisko
        public string Surname { get; set; }
        //opis
        public string Description { get; set; }
        
    }
}