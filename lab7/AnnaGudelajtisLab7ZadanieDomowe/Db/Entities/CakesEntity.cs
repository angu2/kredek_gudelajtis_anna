﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AnnaGudelajtisLab7ZadanieDomowe.Db.Entities
{
    /// <summary>
    /// reprezentuje tabele ciasta
    /// </summary>
    public class CakesEntity
    {
        //poszczegolne atrybuty tabeli ciasta
        [Key]
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string PreparationTime { get; set; }
        public virtual string Difficulty { get; set; }
        public virtual string Temperature { get; set; }
        public virtual string Description { get; set; }
    }
}
