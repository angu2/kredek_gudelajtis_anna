﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AnnaGudelajtisLab7ZadanieDomowe.Db.Entities
{
    /// <summary>
    /// reprezentuje tabele potraw
    /// </summary>
    public class RecipesEntity
    {
        //poszczegolne atrybuty tabeli potraw
        [Key]
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string PreparationTime { get; set; }
        public virtual string Difficulty { get; set; }
        public virtual string Quantity { get; set; }
    }
}
