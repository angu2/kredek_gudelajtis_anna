﻿using AnnaGudelajtisLab7ZadanieDomowe.Db.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnnaGudelajtisLab7ZadanieDomowe.Db
{
    // klasa kontekstu bazy danych 
    public class DataContext : DbContext
    {
        //wlaściwiosci reprezentujące tabele w bazie
        public virtual DbSet<CakesEntity> CakesEntities { get; set; }
        public virtual DbSet<RecipesEntity> RecipesEntities { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //ustala serwer bazy danych
            optionsBuilder.UseSqlServer(@"Data Source=ANIA-PC\ANIAGUDSQL;Initial Catalog=AnnaGudelajtisLab7ZadanieDomowe;Integrated Security=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
