﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnnaGudelajtisLab7ZadanieDomowe.ViewModels
{
    public class ShowRecipeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PreparationTime { get; set; }
        public string Difficulty { get; set; }
        public string Quantity { get; set; }
    }
}
