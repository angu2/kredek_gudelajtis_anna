﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnnaGudelajtisLab7ZadanieDomowe.ViewModels
{
    public class SaveCakeViewModel
    {
        public string Name { get; set; }
        public string PreparationTime { get; set; }
        public string Difficulty { get; set; }
        public string Temperature { get; set; }
        public string Description { get; set; }
    }
}
