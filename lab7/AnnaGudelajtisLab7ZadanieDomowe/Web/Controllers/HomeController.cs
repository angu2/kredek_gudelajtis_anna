﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AnnaGudelajtisLab7ZadanieDomowe.Service.CakeService;
using AnnaGudelajtisLab7ZadanieDomowe.Service.RecipeSevice;
using AnnaGudelajtisLab7ZadanieDomowe.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        //zmienne z dostepem do serwisow obsługujących bazę
        private ICakeService _cakeService;
        private IRecipeService _recipeService;

        /// <summary>
        /// konstruktor home kontrolera, przypisuje serwisy obslugujące baze
        /// </summary>
        /// <param name="cakeService"></param>
        /// <param name="recipeService"></param>
        public HomeController(ICakeService cakeService, IRecipeService recipeService)
        {
            _cakeService = cakeService;
            _recipeService = recipeService;
        }

        /// <summary>
        /// widok glowny z ciastami
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            //pobiera wszystkie ciasta z bazy
            List<ShowCakeViewModel> model = _cakeService.GetCakes();
            return View(model);
        }
        
        /// <summary>
        /// formularz dla dodawania ciasta
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Cake(SaveCakeViewModel model)
        {
            //zapisuje ciasto do bazy
            _cakeService.SaveCake(model);
            //przekierowuje do widoku glownego
            return RedirectToAction("Index");
        }

        /// <summary>
        /// widok z potrawami
        /// </summary>
        /// <returns></returns>
        public IActionResult Recipes()
        {
            //pobiera z bazy wszystkie potrawy
            List<ShowRecipeViewModel> model = _recipeService.GetRecipes();
            return View(model);
        }

        /// <summary>
        /// formularz do dodawania potrawy
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult RecipePost(SaveRecipeViewModel model)
        {
            //zapisuje potrawe do bazy
            _recipeService.SaveRecipe(model);
            //przekierowuje do widoku 'recipes'
            return RedirectToAction("Recipes");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
