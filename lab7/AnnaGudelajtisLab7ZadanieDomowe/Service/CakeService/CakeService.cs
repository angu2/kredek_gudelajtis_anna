﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AnnaGudelajtisLab7ZadanieDomowe.Db;
using AnnaGudelajtisLab7ZadanieDomowe.Db.Entities;
using AnnaGudelajtisLab7ZadanieDomowe.ViewModels;

namespace AnnaGudelajtisLab7ZadanieDomowe.Service.CakeService
{
    public class CakeService : ICakeService

    {
        public List<ShowCakeViewModel> GetCakes()
        {
            var db = new DataContext();
            var list = new List<ShowCakeViewModel>();
            var model = db.Set<CakesEntity>().Where(x => true);

            foreach (var item in model)
            {
                //dodawanie poszczegolnych rekordow do listy 
                list.Add(new ShowCakeViewModel()
                {
                    //przypisuje atrybuty z rekordu do widoku modelu
                    Id = item.Id,
                    Name = item.Name,
                    PreparationTime = item.PreparationTime,
                    Difficulty = item.Difficulty,
                    Temperature = item.Temperature,
                    Description = item.Description
                });
            }

            return list;
        }

        public void SaveCake(SaveCakeViewModel model)
        {
            var db = new DataContext();
            //tworzy rekord ciasta
            var cake = new CakesEntity()
            {
                //przypisuje atrybuty z modelu (np. pobrane z formularza)
                Name = model.Name,
                PreparationTime = model.PreparationTime,
                Difficulty = model.Difficulty,
                Temperature = model.Temperature,
                Description = model.Description,
            };
            //dodaje do bazy
            db.Add<CakesEntity>(cake);
            db.SaveChanges();
        }
    }
}
