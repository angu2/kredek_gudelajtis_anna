﻿using System;
using System.Collections.Generic;
using System.Text;
using AnnaGudelajtisLab7ZadanieDomowe.ViewModels;

namespace AnnaGudelajtisLab7ZadanieDomowe.Service.CakeService
{
    //interfejs serwisu do obsługi tabeli ciast w bazie
    public interface ICakeService
    {
        //zapisuje ciasto do bazy
        void SaveCake(SaveCakeViewModel model);
        //pobiera wszystkie ciasta z bazy
        List<ShowCakeViewModel> GetCakes();
    }
}
