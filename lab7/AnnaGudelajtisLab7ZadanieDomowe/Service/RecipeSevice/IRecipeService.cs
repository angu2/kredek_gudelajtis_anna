﻿using System;
using System.Collections.Generic;
using System.Text;
using AnnaGudelajtisLab7ZadanieDomowe.ViewModels;

namespace AnnaGudelajtisLab7ZadanieDomowe.Service.RecipeSevice
{
    //interfejs serwisu do obslugi tabeli potraw
    public interface IRecipeService
    {
        //zapisuje rekord do bazy
        void SaveRecipe(SaveRecipeViewModel model);
        //pobiera wszystkie rekordy z bazy
        List<ShowRecipeViewModel> GetRecipes();
    }
}
