﻿using AnnaGudelajtisLab7ZadanieDomowe.Db;
using AnnaGudelajtisLab7ZadanieDomowe.Db.Entities;
using AnnaGudelajtisLab7ZadanieDomowe.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AnnaGudelajtisLab7ZadanieDomowe.Service.RecipeSevice
{
    public class RecipeService : IRecipeService
    {
        public List<ShowRecipeViewModel> GetRecipes()
        {
            var db = new DataContext();
            var list = new List<ShowRecipeViewModel>();
            var model = db.Set<RecipesEntity>().Where(x => true);

            foreach (var item in model)
            {
                //dodawanie poszczegolnych rekordow do listy 
                list.Add(new ShowRecipeViewModel()
                {
                    //przypisuje atrybuty z rekordu do widoku modelu
                    Id = item.Id,
                    Name = item.Name,
                    PreparationTime = item.PreparationTime,
                    Difficulty = item.Difficulty,
                    Quantity = item.Quantity
                });
            }

            return list;
        }

        public void SaveRecipe(SaveRecipeViewModel model)
        {
            var db = new DataContext();
            //tworzy rekord
            var recipe = new RecipesEntity()
            {
                //przypisuje atrybuty z modelu (np. pobrane z formularza)//przypisuje atrybuty z modelu (np. pobrane z formularza)
                Name = model.Name,
                PreparationTime = model.PreparationTime,
                Difficulty = model.Difficulty,
                Quantity = model.Quantity
            };
            //dodaje do bazy
            db.Add<RecipesEntity>(recipe);
            db.SaveChanges();
        }
    }
}
