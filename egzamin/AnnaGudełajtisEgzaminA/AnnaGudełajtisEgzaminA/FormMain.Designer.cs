﻿namespace AnnaGudełajtisEgzaminA
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxA1Login = new System.Windows.Forms.TextBox();
            this.textBoxA1Password = new System.Windows.Forms.TextBox();
            this.buttonA1LogIn = new System.Windows.Forms.Button();
            this.labelA1Login = new System.Windows.Forms.Label();
            this.labelA1Password = new System.Windows.Forms.Label();
            this.labelA1LoggedIn = new System.Windows.Forms.Label();
            this.buttonA3Start = new System.Windows.Forms.Button();
            this.timerA3 = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewData = new System.Windows.Forms.DataGridView();
            this.buttonA4Employees = new System.Windows.Forms.Button();
            this.buttonA5WithoutBoss = new System.Windows.Forms.Button();
            this.buttonA6roducts = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxA1Login
            // 
            this.textBoxA1Login.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxA1Login.Location = new System.Drawing.Point(109, 51);
            this.textBoxA1Login.Name = "textBoxA1Login";
            this.textBoxA1Login.Size = new System.Drawing.Size(211, 26);
            this.textBoxA1Login.TabIndex = 2;
            // 
            // textBoxA1Password
            // 
            this.textBoxA1Password.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxA1Password.Location = new System.Drawing.Point(109, 103);
            this.textBoxA1Password.Name = "textBoxA1Password";
            this.textBoxA1Password.Size = new System.Drawing.Size(211, 26);
            this.textBoxA1Password.TabIndex = 3;
            // 
            // buttonA1LogIn
            // 
            this.buttonA1LogIn.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonA1LogIn.Location = new System.Drawing.Point(109, 166);
            this.buttonA1LogIn.Name = "buttonA1LogIn";
            this.buttonA1LogIn.Size = new System.Drawing.Size(210, 33);
            this.buttonA1LogIn.TabIndex = 4;
            this.buttonA1LogIn.Text = "A1 Zaloguj";
            this.buttonA1LogIn.UseVisualStyleBackColor = true;
            this.buttonA1LogIn.Click += new System.EventHandler(this.buttonA1LogIn_Click);
            // 
            // labelA1Login
            // 
            this.labelA1Login.AutoSize = true;
            this.labelA1Login.Location = new System.Drawing.Point(49, 51);
            this.labelA1Login.Name = "labelA1Login";
            this.labelA1Login.Size = new System.Drawing.Size(42, 17);
            this.labelA1Login.TabIndex = 5;
            this.labelA1Login.Text = "login:";
            // 
            // labelA1Password
            // 
            this.labelA1Password.AutoSize = true;
            this.labelA1Password.Location = new System.Drawing.Point(49, 103);
            this.labelA1Password.Name = "labelA1Password";
            this.labelA1Password.Size = new System.Drawing.Size(46, 17);
            this.labelA1Password.TabIndex = 6;
            this.labelA1Password.Text = "hasło:";
            // 
            // labelA1LoggedIn
            // 
            this.labelA1LoggedIn.AutoSize = true;
            this.labelA1LoggedIn.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelA1LoggedIn.Location = new System.Drawing.Point(106, 234);
            this.labelA1LoggedIn.Name = "labelA1LoggedIn";
            this.labelA1LoggedIn.Size = new System.Drawing.Size(0, 19);
            this.labelA1LoggedIn.TabIndex = 7;
            // 
            // buttonA3Start
            // 
            this.buttonA3Start.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonA3Start.Location = new System.Drawing.Point(109, 515);
            this.buttonA3Start.Name = "buttonA3Start";
            this.buttonA3Start.Size = new System.Drawing.Size(210, 33);
            this.buttonA3Start.TabIndex = 8;
            this.buttonA3Start.Text = "A3 Start";
            this.buttonA3Start.UseVisualStyleBackColor = true;
            this.buttonA3Start.Click += new System.EventHandler(this.buttonA3Start_Click);
            // 
            // timerA3
            // 
            this.timerA3.Tick += new System.EventHandler(this.timerA3_Tick);
            // 
            // dataGridViewData
            // 
            this.dataGridViewData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewData.Location = new System.Drawing.Point(516, 51);
            this.dataGridViewData.Name = "dataGridViewData";
            this.dataGridViewData.RowTemplate.Height = 24;
            this.dataGridViewData.Size = new System.Drawing.Size(523, 378);
            this.dataGridViewData.TabIndex = 9;
            // 
            // buttonA4Employees
            // 
            this.buttonA4Employees.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonA4Employees.Location = new System.Drawing.Point(110, 266);
            this.buttonA4Employees.Name = "buttonA4Employees";
            this.buttonA4Employees.Size = new System.Drawing.Size(210, 33);
            this.buttonA4Employees.TabIndex = 10;
            this.buttonA4Employees.Text = "A4 Pracownicy";
            this.buttonA4Employees.UseVisualStyleBackColor = true;
            this.buttonA4Employees.Click += new System.EventHandler(this.buttonA4Employees_Click);
            // 
            // buttonA5WithoutBoss
            // 
            this.buttonA5WithoutBoss.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonA5WithoutBoss.Location = new System.Drawing.Point(110, 328);
            this.buttonA5WithoutBoss.Name = "buttonA5WithoutBoss";
            this.buttonA5WithoutBoss.Size = new System.Drawing.Size(210, 33);
            this.buttonA5WithoutBoss.TabIndex = 11;
            this.buttonA5WithoutBoss.Text = "A5 Bez Przelozonych";
            this.buttonA5WithoutBoss.UseVisualStyleBackColor = true;
            this.buttonA5WithoutBoss.Click += new System.EventHandler(this.buttonA5WithoutBoss_Click);
            // 
            // buttonA6roducts
            // 
            this.buttonA6roducts.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonA6roducts.Location = new System.Drawing.Point(110, 396);
            this.buttonA6roducts.Name = "buttonA6roducts";
            this.buttonA6roducts.Size = new System.Drawing.Size(210, 33);
            this.buttonA6roducts.TabIndex = 12;
            this.buttonA6roducts.Text = "A6 Produkty";
            this.buttonA6roducts.UseVisualStyleBackColor = true;
            this.buttonA6roducts.Click += new System.EventHandler(this.buttonA6roducts_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1105, 574);
            this.Controls.Add(this.buttonA6roducts);
            this.Controls.Add(this.buttonA5WithoutBoss);
            this.Controls.Add(this.buttonA4Employees);
            this.Controls.Add(this.dataGridViewData);
            this.Controls.Add(this.buttonA3Start);
            this.Controls.Add(this.labelA1LoggedIn);
            this.Controls.Add(this.labelA1Password);
            this.Controls.Add(this.labelA1Login);
            this.Controls.Add(this.buttonA1LogIn);
            this.Controls.Add(this.textBoxA1Password);
            this.Controls.Add(this.textBoxA1Login);
            this.Name = "FormMain";
            this.Text = "FormMain";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxA1Login;
        private System.Windows.Forms.TextBox textBoxA1Password;
        private System.Windows.Forms.Button buttonA1LogIn;
        private System.Windows.Forms.Label labelA1Login;
        private System.Windows.Forms.Label labelA1Password;
        private System.Windows.Forms.Label labelA1LoggedIn;
        private System.Windows.Forms.Button buttonA3Start;
        private System.Windows.Forms.Timer timerA3;
        private System.Windows.Forms.DataGridView dataGridViewData;
        private System.Windows.Forms.Button buttonA4Employees;
        private System.Windows.Forms.Button buttonA5WithoutBoss;
        private System.Windows.Forms.Button buttonA6roducts;
    }
}

