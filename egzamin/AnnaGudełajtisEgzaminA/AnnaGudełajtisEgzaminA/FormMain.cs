﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnaGudełajtisEgzaminA
{
    public partial class FormMain : Form
    {
        //A1 zmienna zliczająca ilosc blędow logowania
        int counter = 0;

        public FormMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// rozwiazanie A1 - kontroluje logowanie 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonA1LogIn_Click(object sender, EventArgs e)
        {
            //sprawdza poprawnosc logn i hasla
            if (textBoxA1Login.Text == "admin" & textBoxA1Password.Text == "test")
            {
                //wyswietla komunikat o zalogowaniu
                labelA1LoggedIn.Text = "zalogowano";
            }
            else
            {
                //dodaje blad
                counter++;
                //sprawdza czy to 3. blad
                if (counter == 3)
                {
                    //textboxy i reszta znika
                    textBoxA1Login.Visible = false;
                    textBoxA1Password.Visible = false;
                    buttonA1LogIn.Visible = false;
                    labelA1Login.Visible = false;
                    labelA1Password.Visible = false;
                    //wyswietla kominukat
                    labelA1LoggedIn.Text = "3 błedne próby";
                }

            }
            //czysci textboxy
            textBoxA1Login.Text = "";
            textBoxA1Password.Text = "";
        }

        /// <summary>
        /// A3 - funkcja przesuwająca guzik zgodnie z timerem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonA3Start_Click(object sender, EventArgs e)
        {
            //ustawia intewal na 3s
            timerA3.Interval = 3000;
            //startuje zgar
            timerA3.Start();
        }

        /// <summary>
        /// A3 timer - fnkcja wykonująca sie za kazdym interwałem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerA3_Tick(object sender, EventArgs e)
        {
            //zwieksza dystans guzika od lewej (od kontenera) o 10
            buttonA3Start.Left += 10;
        }

        /// <summary>
        /// A5 - wyswietla pracownikow bez przelozonych
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonA5WithoutBoss_Click(object sender, EventArgs e)
        {
            //tworzy kontekst do bazy danych
            NorthwindEntities context = new NorthwindEntities();
            //zapytanie do bazy danych
            var emloyees = context.Employees.Where(x => x.ReportsTo == null).ToList();
            //wyswietla dane w grid view
            dataGridViewData.DataSource = emloyees;

        }

        /// <summary>
        /// A6 - wyswietla produkty
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonA6roducts_Click(object sender, EventArgs e)
        {
            //tworzy kontekst do bazy danych
            NorthwindEntities context = new NorthwindEntities();
            //zapytanie do bazy danych
            var products = context.Products.Where(x => x.UnitPrice>5 & x.UnitPrice<20).ToList();
            //wyswietla dane w grid view
            dataGridViewData.DataSource = products;
        }

        /// <summary>
        /// A4 - acncy zawierajcy a
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonA4Employees_Click(object sender, EventArgs e)
        {
            //tworzy kontekst do bazy danych
            NorthwindEntities context = new NorthwindEntities();
            //zapytanie do bazy danych
            var emloyees = context.Employees.Where(x => x.FirstName.Contains("a") ).ToList();
            //wyswietla dane w grid view
            dataGridViewData.DataSource = emloyees;
        }
    }
}
